<?php

namespace App\DataFixtures;

use App\Entity\CoursePromotion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CoursePromotionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; $i++) {
            $coursePromotion = new CoursePromotion();
            //$coursePromotion->setCode($faker->unique()->randomLetters($nbLetters = 32));
            $coursePromotion->setCode($faker->unique()->hexColor . $faker->unique()->hexColor);
            $coursePromotion->setStartDateValidity($faker->dateTimeBetween('-15 days', '+25 days'));
            $coursePromotion->setEndDateValidity($faker->dateTimeBetween($coursePromotion->getStartDateValidity(), '+30 days'));
            $coursePromotion->setPercentOnPrice($faker->numberBetween(10, 30));
            $manager->persist($coursePromotion);
        }

        $manager->flush();
    }
}

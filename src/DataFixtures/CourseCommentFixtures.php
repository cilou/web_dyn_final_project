<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\CourseComment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CourseCommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $userRepository = $manager->getRepository(User::class);
        //$users = $userRepository->findAll();
        //$users = $userRepository->findAllByRoleName('ROLE_MEMBER');
        $users = $userRepository->findAllNotWithRoleName('ROLE_ADMIN');
        $courseRepository = $manager->getRepository(Course::class);
        $courses = $courseRepository->findAll();
        $courseCommentRepository = $manager->getRepository(CourseComment::class);

        for($i = 0; $i < 40; $i++) {
            $courseComment = new CourseComment();
            $courseComment->setUser($users[$faker->numberBetween(0, count($users) - 1)]);
            $courseComment->setCourse($courses[$faker->numberBetween(0, count($courses) - 1)]);
            $courseComment->setCreatedAt($faker->dateTimeBetween($courseComment->getCourse()->getCreatedAt(), 'now'));
            $courseComment->setUpdatedAt($faker->dateTimeBetween($courseComment->getCreatedAt(), 'now'));
            $courseComment->setMessage($faker->text(50));
            $courseComment->setIsDeleted($faker->boolean(10));
            $courseComment->setIsPublished(!$courseComment->getIsDeleted() && $faker->boolean(80));
            /*
            if($courseComment->getIsDeleted()) {
                $courseComment->setIsPublished(0);
            } else {
                $courseComment->setIsPublished($faker->boolean(80));
            }
            */

            $courseComments = $courseCommentRepository->findAll();
            if(count($courseComments) > 5) {
                if($faker->boolean(70)) {
                    $courseComment->setParent($courseComments[$faker->numberBetween(0, count($courseComments) - 1)]);
                    $courseComment->setCourse($courseComment->getParent()->getCourse());
                }
            }
            $manager->persist($courseComment);
            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}

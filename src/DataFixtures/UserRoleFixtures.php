<?php

namespace App\DataFixtures;

use App\Entity\UserRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserRoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $userRole = new UserRole();
        $userRole->setName('ROLE_GUEST');
        $manager->persist($userRole);

        $userRole = new UserRole();
        $userRole->setName('ROLE_MEMBER');
        $manager->persist($userRole);

        $userRole = new UserRole();
        $userRole->setName('ROLE_MODERATOR');
        $manager->persist($userRole);

        $userRole = new UserRole();
        $userRole->setName('ROLE_COURSE_MANAGER');
        $manager->persist($userRole);

        $userRole = new UserRole();
        $userRole->setName('ROLE_ADMIN');
        $manager->persist($userRole);

        $manager->flush();
    }
}

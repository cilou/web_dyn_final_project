<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Course;
use App\Entity\CoursePromotion;
use App\Entity\Language;
use App\Entity\Tag;
use App\Entity\TaxRate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use EmanueleMinotto;

class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new EmanueleMinotto\Faker\PlaceholdItProvider($faker));
        $categoryRepository = $manager->getRepository(Category::class);
        $categories = $categoryRepository->findAll();
        $taxRateRepository = $manager->getRepository(TaxRate::class);
        $taxRates = $taxRateRepository->findAll();
        $coursePromotionRepository = $manager->getRepository(CoursePromotion::class);
        $promotions = $coursePromotionRepository->findAll();
        $tagRepository = $manager->getRepository(Tag::class);
        $tags = $tagRepository->findAll();

        $languageRepository = $manager->getRepository(Language::class);
        $languages = $languageRepository->findAll();

        for($i = 0; $i < 20; $i++) {

            $course = new Course();

            // ManyToMany Category
            $faker->unique(true); //reset unicity so the next call can provide the same category if needed
            $course->addCategory($categories[$faker->unique->numberBetween(0, count($categories) - 1)]);
            if($faker->boolean()) {
                $course->addCategory($categories[$faker->unique->numberBetween(0, count($categories) - 1)]);
            }if($faker->boolean(20)) {
                $course->addCategory($categories[$faker->unique->numberBetween(0, count($categories) - 1)]);
            }

            // ManyToMany Tag
            $faker->unique(true);
            $course->addTag($tags[$faker->unique->numberBetween(0, count($tags) - 1)]);
            if($faker->boolean()) {
                $course->addTag($tags[$faker->unique->numberBetween(0, count($tags) - 1)]);
            }
            if($faker->boolean(20)) {
                $course->addTag($tags[$faker->unique->numberBetween(0, count($tags) - 1)]);
            }

            // ManyToOne fields (foreign keys)
            $course->setTaxRate($taxRates[$faker->numberBetween(0, count($taxRates) - 1)]);
            $course->setCoursePromotion($promotions[$faker->numberBetween(0, count($promotions) - 1)]);
            $course->setLanguage($languages[$faker->numberBetween(0, count($languages) - 1)]);

            // Stand alone fields
            $course->setTitle($faker->text(40));
            //$course->setImage($faker->imageUrl(50, 'jpg'));
            $course->setImage($i+1 . '.jpeg');
            //$course->setImage($faker->numberBetween(1, 20) . '.jpeg');
            $course->setSmallDescription($faker->text(175));
            $course->setFullDescription($faker->text(1700));
            $course->setDuration($faker->numberBetween(10, 80));
            $course->setPrice($faker->numberBetween(50, 250));
            $course->setIsPublished($faker->boolean(80));
            $course->setCreatedAt($faker->dateTimeBetween('-3 months', 'now'));
            $course->setUpdatedAt($faker->dateTimeBetween($course->getCreatedAt(), 'now'));

            $manager->persist($course);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        // loaded in order of use, not based on array order
        return [
            TaxRateFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class,
            CoursePromotionFixtures::class,
            LanguageFixtures::class // loaded even if commented !!??
        ];
    }
}

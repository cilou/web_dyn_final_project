<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use EmanueleMinotto;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new EmanueleMinotto\Faker\PlaceholdItProvider($faker));
        $userRoleRepository = $manager->getRepository(UserRole::class);
        $userRoles = $userRoleRepository->findAll();

        for($i = 0; $i < 60; $i++) {

            $user = new User();
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->lastName);
            $user->setUsername($user->getFirstname() . $user->getLastname());
            $user->setEmail($user->getFirstname() . $user->getLastname() . '@gmail.com');
            $user->setIsEmailConfirmed($faker->boolean(50));
            $user->setImage($faker->imageUrl(50, 'png'));
            $user->setPassword('password');
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
            $user->setCreatedAt($faker->dateTimeBetween('-6 months', 'now'));
            $user->setUpdatedAt($faker->dateTimeBetween('now'));
            $user->setLastLogin($faker->dateTimeBetween('now'));
            $user->setIsDisabled($faker->boolean(20));
            $user->setIsDeleted($faker->boolean(10));
            //$userRoleRepository = $manager->getRepository(UserRole::class);
            //$userRoles = $userRoleRepository->findAll();
            $user->setUserRole($userRoles[$faker->numberBetween(1, count($userRoles) - 1)]);

            $manager->persist($user);
        }

        $user = new User();
        $user->setFirstname('John');
        $user->setLastname('Doe');
        $user->setUsername('jdoe');
        $user->setEmail($user->getFirstname() . $user->getLastname() . '@gmail.com');
        $user->setIsEmailConfirmed(true);
        $user->setImage('userDefault.png');
        $user->setPassword('password');
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $user->setCreatedAt($faker->dateTimeBetween('now'));
        $user->setUpdatedAt($user->getCreatedAt());
        $user->setIsDisabled(false);
        $user->setIsDeleted(false);
        //$userRoleRepository = $manager->getRepository(UserRole::class);
        $user->setUserRole($userRoleRepository->findOneBy(['name' => 'ROLE_MEMBER']));

        $manager->persist($user);

        $user = new User();
        $user->setFirstname('Admin');
        $user->setLastname('Admin');
        $user->setUsername('admin');
        $user->setEmail($user->getFirstname() . $user->getLastname() . '@gmail.com');
        $user->setIsEmailConfirmed(true);
        $user->setImage('userDefault.png');
        $user->setPassword('password');
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $user->setCreatedAt($faker->dateTimeBetween('now'));
        $user->setUpdatedAt($user->getCreatedAt());
        $user->setIsDisabled(false);
        $user->setIsDeleted(false);
        //$userRoleRepository = $manager->getRepository(UserRole::class);
        $user->setUserRole($userRoleRepository->findOneBy(['name' => 'ROLE_ADMIN']));

        $manager->persist($user);


        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserRoleFixtures::class
        ];
    }
}

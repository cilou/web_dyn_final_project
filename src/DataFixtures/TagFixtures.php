<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tag = new Tag();
        $tag->setName('Symfony');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('JQuery');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('Laravel');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('Doctrine');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('Promises');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('Bootstrap');
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName('MVC');
        $manager->persist($tag);


        $manager->flush();
    }
}

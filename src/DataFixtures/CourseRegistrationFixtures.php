<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\CourseRegistration;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CourseRegistrationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $courseRepository = $manager->getRepository(Course::class);
        $courses = $courseRepository->findAll();
        $userRepository = $manager->getRepository(User::class);
        //$users = $userRepository->findAllByRoleName('ROLE_MEMBER');
        $users = $userRepository->findAllNotWithRoleName('ROLE_ADMIN');

        for($i = 0; $i < 20; $i++) {
            $courseRegistration = new CourseRegistration();
            $courseRegistration->setUser($users[$faker->numberBetween(0, count($users) - 1)]);
            $courseRegistration->setCourse($courses[$faker->numberBetween(0, count($courses) - 1)]);
            $courseRegistration->setCreatedAt($faker->dateTimeBetween($courseRegistration->getCourse()->getCreatedAt(), 'now'));
            $courseRegistration->setUpdatedAt($faker->dateTimeBetween($courseRegistration->getCreatedAt(), 'now'));
            $courseRegistration->setPaidAmount($courseRegistration->getCourse()->getPrice());
            $courseRegistration->setTaxRate($courseRegistration->getCourse()->getTaxRate()->getRate());
            $courseRegistration->setIsCancelled($faker->boolean(20));

            $manager->persist($courseRegistration);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}

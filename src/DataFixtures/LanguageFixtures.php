<?php

namespace App\DataFixtures;

use App\Entity\Language;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LanguageFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $language = new Language();
        $language->setIsoCode('en-US');
        $language->setIsoName('English (United States)');
        $manager->persist($language);

        // if same object is used a second time, data is replaced in DB
        $language = new Language();
        $language->setIsoCode('fr-BE');
        $language->setIsoName('French (Belgium)');
        $manager->persist($language);

        $language = new Language();
        $language->setIsoCode('es-ES');
        $language->setIsoName('Spanish (Spain)');
        $manager->persist($language);

        $manager->flush();
    }
}

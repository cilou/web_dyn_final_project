<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\CourseReview;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CourseReviewFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $userRepository = $manager->getRepository(User::class);
        //$users = $userRepository->findAll();
        //$users = $userRepository->findAllByRoleName('ROLE_MEMBER');
        $users = $userRepository->findAllNotWithRoleName('ROLE_ADMIN');
        $courseRepository = $manager->getRepository(Course::class);
        $courses = $courseRepository->findAll();

        $idUserCourseUsed = [];

        for ($i = 0; $i < 30; $i++) {
            $courseReview = new CourseReview();
            do {
                $userId = $faker->numberBetween(0, count($users) - 1);
                $courseId = $faker->numberBetween(0, count($courses) - 1);
            } while (key_exists($userId, $idUserCourseUsed) && $idUserCourseUsed[$userId] == $courseId);
            $idUserCourseUsed[$userId] = $courseId;

            $courseReview->setUser($users[$userId]);
            $courseReview->setCourse($courses[$courseId]);
            $courseReview->setRating($faker->numberBetween(1, 5));
            $courseReview->setReview($faker->text(120));
            $courseReview->setCreatedAt($faker->dateTimeBetween($courseReview->getCourse()->getCreatedAt(), 'now'));
            $courseReview->setUpdatedAt($faker->dateTimeBetween($courseReview->getCreatedAt(), 'now'));
            $courseReview->setIsDeleted($faker->boolean(10));
            $courseReview->setIsPublished(!$courseReview->getIsDeleted() && $faker->boolean(80));

            $manager->persist($courseReview);
        }

        $manager->flush();
    }
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}

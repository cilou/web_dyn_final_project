<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName("PHP");
        $manager->persist($category);

        $category = new Category();
        $category->setName("Javascript");
        $manager->persist($category);

        $category = new Category();
        $category->setName("CSS");
        $manager->persist($category);

        $category = new Category();
        $category->setName("HTML");
        $manager->persist($category);

        $category = new Category();
        $category->setName("Database");
        $manager->persist($category);

        $category = new Category();
        $category->setName("Python");
        $manager->persist($category);

        $manager->flush();
    }
}

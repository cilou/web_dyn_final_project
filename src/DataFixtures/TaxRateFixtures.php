<?php

namespace App\DataFixtures;

use App\Entity\TaxRate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TaxRateFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //$taxRate = new TaxRate();
        //$taxRate->setRate(0);
        //$manager->persist($taxRate);

        $taxRate = new TaxRate();
        $taxRate->setRate(6);
        $manager->persist($taxRate);

        $taxRate = new TaxRate();
        $taxRate->setRate(12);
        $manager->persist($taxRate);

        $taxRate = new TaxRate();
        $taxRate->setRate(21);
        $manager->persist($taxRate);

        $manager->flush();
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190104084831 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, tax_rate_id INT NOT NULL, course_promotion_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, small_description VARCHAR(255) NOT NULL, full_description LONGTEXT NOT NULL, duration INT NOT NULL, price DOUBLE PRECISION NOT NULL, is_published TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_169E6FB982F1BAF4 (language_id), INDEX IDX_169E6FB9FDD13F95 (tax_rate_id), INDEX IDX_169E6FB992578969 (course_promotion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_category (course_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_AFF87497591CC992 (course_id), INDEX IDX_AFF8749712469DE2 (category_id), PRIMARY KEY(course_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_tag (course_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_760531B1591CC992 (course_id), INDEX IDX_760531B1BAD26311 (tag_id), PRIMARY KEY(course_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, course_id INT NOT NULL, parent_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, message LONGTEXT NOT NULL, is_published TINYINT(1) NOT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_9CB75780A76ED395 (user_id), INDEX IDX_9CB75780591CC992 (course_id), INDEX IDX_9CB75780727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_promotion (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(32) NOT NULL, start_date_validity DATETIME NOT NULL, end_date_validity DATETIME NOT NULL, percent_on_price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_registration (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, course_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, paid_amount DOUBLE PRECISION NOT NULL, tax_rate INT NOT NULL, is_cancelled TINYINT(1) NOT NULL, INDEX IDX_E362DF5AA76ED395 (user_id), INDEX IDX_E362DF5A591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_review (course_id INT NOT NULL, user_id INT NOT NULL, rating SMALLINT NOT NULL, review LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, is_published TINYINT(1) NOT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_D77B408B591CC992 (course_id), INDEX IDX_D77B408BA76ED395 (user_id), PRIMARY KEY(course_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, iso_code VARCHAR(5) NOT NULL, iso_name VARCHAR(80) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE one_time_token (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, uuid VARCHAR(36) NOT NULL, type VARCHAR(32) NOT NULL, iat INT NOT NULL, exp INT NOT NULL, is_valid TINYINT(1) NOT NULL, INDEX IDX_5D9F8E3DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax_rate (id INT AUTO_INCREMENT NOT NULL, rate INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, user_role_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, last_login DATETIME DEFAULT NULL, last_successful_login DATETIME DEFAULT NULL, last_failed_login DATETIME DEFAULT NULL, is_disabled TINYINT(1) DEFAULT \'0\' NOT NULL, is_deleted TINYINT(1) DEFAULT \'0\' NOT NULL, is_email_confirmed TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_8D93D6498E0E3CA6 (user_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_registration (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB982F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB9FDD13F95 FOREIGN KEY (tax_rate_id) REFERENCES tax_rate (id)');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB992578969 FOREIGN KEY (course_promotion_id) REFERENCES course_promotion (id)');
        $this->addSql('ALTER TABLE course_category ADD CONSTRAINT FK_AFF87497591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_category ADD CONSTRAINT FK_AFF8749712469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_tag ADD CONSTRAINT FK_760531B1591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_tag ADD CONSTRAINT FK_760531B1BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_comment ADD CONSTRAINT FK_9CB75780A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE course_comment ADD CONSTRAINT FK_9CB75780591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE course_comment ADD CONSTRAINT FK_9CB75780727ACA70 FOREIGN KEY (parent_id) REFERENCES course_comment (id)');
        $this->addSql('ALTER TABLE course_registration ADD CONSTRAINT FK_E362DF5AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE course_registration ADD CONSTRAINT FK_E362DF5A591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE course_review ADD CONSTRAINT FK_D77B408B591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE course_review ADD CONSTRAINT FK_D77B408BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE one_time_token ADD CONSTRAINT FK_5D9F8E3DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6498E0E3CA6 FOREIGN KEY (user_role_id) REFERENCES user_role (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_category DROP FOREIGN KEY FK_AFF8749712469DE2');
        $this->addSql('ALTER TABLE course_category DROP FOREIGN KEY FK_AFF87497591CC992');
        $this->addSql('ALTER TABLE course_tag DROP FOREIGN KEY FK_760531B1591CC992');
        $this->addSql('ALTER TABLE course_comment DROP FOREIGN KEY FK_9CB75780591CC992');
        $this->addSql('ALTER TABLE course_registration DROP FOREIGN KEY FK_E362DF5A591CC992');
        $this->addSql('ALTER TABLE course_review DROP FOREIGN KEY FK_D77B408B591CC992');
        $this->addSql('ALTER TABLE course_comment DROP FOREIGN KEY FK_9CB75780727ACA70');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB992578969');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB982F1BAF4');
        $this->addSql('ALTER TABLE course_tag DROP FOREIGN KEY FK_760531B1BAD26311');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB9FDD13F95');
        $this->addSql('ALTER TABLE course_comment DROP FOREIGN KEY FK_9CB75780A76ED395');
        $this->addSql('ALTER TABLE course_registration DROP FOREIGN KEY FK_E362DF5AA76ED395');
        $this->addSql('ALTER TABLE course_review DROP FOREIGN KEY FK_D77B408BA76ED395');
        $this->addSql('ALTER TABLE one_time_token DROP FOREIGN KEY FK_5D9F8E3DA76ED395');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6498E0E3CA6');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE course_category');
        $this->addSql('DROP TABLE course_tag');
        $this->addSql('DROP TABLE course_comment');
        $this->addSql('DROP TABLE course_promotion');
        $this->addSql('DROP TABLE course_registration');
        $this->addSql('DROP TABLE course_review');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE one_time_token');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tax_rate');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_registration');
        $this->addSql('DROP TABLE user_role');
    }
}

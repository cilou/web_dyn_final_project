<?php
/*
    We cannot use User because there is a captcha code requested on the form.
    Better to leave User free of Captcha and create separate entities for
    captcha checks since we do not need to be stored in the database. The
    necessary data will be copied to a User object if necessary then persisted.
 */

namespace App\Entity;

use Captcha\Bundle\CaptchaBundle\Validator\Constraints as CaptchaAssert;

class ResetPasswordRequestUser
{
    /**
     * @var string
     */
    private $email;

    /**
     * @CaptchaAssert\ValidCaptcha(
     *     message = "CAPTCHA validation failed, try again."
     * )
     */
    private $captchaCode;


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCaptchaCode()
    {
        return $this->captchaCode;
    }

    public function setCaptchaCode($captchaCode)
    {
        $this->captchaCode = $captchaCode;
    }
}

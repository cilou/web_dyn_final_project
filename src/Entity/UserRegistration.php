<?php
/*
 * So we can use User.php (entity) without it to be linked with a captcha code.
 * We cannot use User because there is a captcha code requested on the form.
    Better to leave User free of Captcha and create separate entities for
    captcha checks since we do not need to be stored in the database. The
    necessary data will be copied to a User object if necessary then persisted.
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints as CaptchaAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message = "This email is already used")
 * @UniqueEntity(fields={"username"}, message = "This username is already used")
 */
class UserRegistration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var string
     */
    private $image;

    /**
     * @Assert\Length(
     *     min = 8,
     *     minMessage = "The password must contain at least {{ limit }} characters"
     *    )
     */
    private $password;

    /**
     * @Assert\EqualTo(
     *     propertyPath = "password",
     *     message = "The passwords must match"
     *  )
     */
    private $confirmPassword;


    /**
     * @var boolean
     * Used by registration form to require acceptance before creating the account
     */
    private $tos;

    /**
     * @CaptchaAssert\ValidCaptcha(
     *     message = "CAPTCHA validation failed, try again."
     * )
     */
    private $captchaCode;

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    public function setConfirmPassword($confirmPassword): void
    {
        $this->confirmPassword = $confirmPassword;
    }

    /**
     * @return bool
     */
    public function isTos(): bool
    {
        if($this->tos == null) {
            return false;
        }
        return $this->tos;
    }

    /**
     * @param bool $tos
     */
    public function setTos(bool $tos): void
    {
        $this->tos = $tos;
    }

    public function getCaptchaCode()
    {
        return $this->captchaCode;
    }

    public function setCaptchaCode($captchaCode)
    {
        $this->captchaCode = $captchaCode;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $smallDescription;

    /**
     * @ORM\Column(type="text")
     */
    private $fullDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TaxRate")
     * @ORM\JoinColumn(nullable=false)
     */
    private $taxRate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CoursePromotion", inversedBy="courses")
     */
    private $coursePromotion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="courses")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="courses")
     */
    private $tag;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseReview", mappedBy="course", orphanRemoval=true)
     */
    private $courseReviews;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseRegistration", mappedBy="course", orphanRemoval=true)
     */
    private $courseRegistrations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseComment", mappedBy="course", orphanRemoval=true)
     */
    private $courseComments;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->tag = new ArrayCollection();
        $this->courseReviews = new ArrayCollection();
        $this->courseRegistrations = new ArrayCollection();
        $this->courseComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSmallDescription(): ?string
    {
        return $this->smallDescription;
    }

    public function setSmallDescription(string $smallDescription): self
    {
        $this->smallDescription = $smallDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTaxRate(): ?TaxRate
    {
        return $this->taxRate;
    }

    public function setTaxRate(?TaxRate $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function getCoursePromotion(): ?CoursePromotion
    {
        return $this->coursePromotion;
    }

    public function setCoursePromotion(?CoursePromotion $coursePromotion): self
    {
        $this->coursePromotion = $coursePromotion;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTag(): Collection
    {
        return $this->tag;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tag->contains($tag)) {
            $this->tag[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tag->contains($tag)) {
            $this->tag->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return Collection|CourseReview[]
     */
    public function getCourseReviews(): Collection
    {
        return $this->courseReviews;
    }

    public function addCourseReview(CourseReview $courseReview): self
    {
        if (!$this->courseReviews->contains($courseReview)) {
            $this->courseReviews[] = $courseReview;
            $courseReview->setCourse($this);
        }

        return $this;
    }

    public function removeCourseReview(CourseReview $courseReview): self
    {
        if ($this->courseReviews->contains($courseReview)) {
            $this->courseReviews->removeElement($courseReview);
            // set the owning side to null (unless already changed)
            if ($courseReview->getCourse() === $this) {
                $courseReview->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseRegistration[]
     */
    public function getCourseRegistrations(): Collection
    {
        return $this->courseRegistrations;
    }

    public function addCourseRegistration(CourseRegistration $courseRegistration): self
    {
        if (!$this->courseRegistrations->contains($courseRegistration)) {
            $this->courseRegistrations[] = $courseRegistration;
            $courseRegistration->setCourse($this);
        }

        return $this;
    }

    public function removeCourseRegistration(CourseRegistration $courseRegistration): self
    {
        if ($this->courseRegistrations->contains($courseRegistration)) {
            $this->courseRegistrations->removeElement($courseRegistration);
            // set the owning side to null (unless already changed)
            if ($courseRegistration->getCourse() === $this) {
                $courseRegistration->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseComment[]
     */
    public function getCourseComments(): Collection
    {
        return $this->courseComments;
    }

    public function addCourseComment(CourseComment $courseComment): self
    {
        if (!$this->courseComments->contains($courseComment)) {
            $this->courseComments[] = $courseComment;
            $courseComment->setCourse($this);
        }

        return $this;
    }

    public function removeCourseComment(CourseComment $courseComment): self
    {
        if ($this->courseComments->contains($courseComment)) {
            $this->courseComments->removeElement($courseComment);
            // set the owning side to null (unless already changed)
            if ($courseComment->getCourse() === $this) {
                $courseComment->setCourse(null);
            }
        }

        return $this;
    }
}

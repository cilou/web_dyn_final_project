<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursePromotionRepository")
 */
class CoursePromotion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThanOrEqual("today")
     * )
     */
    private $startDateValidity;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThan(
     *     propertyPath = "startDateValidity",
     *      message = "The end date must be greater than the start date"
     * )
     */
    private $endDateValidity;

    /**
     * @ORM\Column(type="float")
     */
    private $percentOnPrice;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Course", mappedBy="coursePromotion")
     */
    private $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStartDateValidity(): ?\DateTimeInterface
    {
        return $this->startDateValidity;
    }

    public function setStartDateValidity(\DateTimeInterface $startDateValidity): self
    {
        $this->startDateValidity = $startDateValidity;

        return $this;
    }

    public function getEndDateValidity(): ?\DateTimeInterface
    {
        return $this->endDateValidity;
    }

    public function setEndDateValidity(\DateTimeInterface $endDateValidity): self
    {
        $this->endDateValidity = $endDateValidity;

        return $this;
    }

    public function getPercentOnPrice(): ?float
    {
        return $this->percentOnPrice;
    }

    public function setPercentOnPrice(float $percentOnPrice): self
    {
        $this->percentOnPrice = $percentOnPrice;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setCoursePromotion($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getCoursePromotion() === $this) {
                $course->setCoursePromotion(null);
            }
        }

        return $this;
    }
}

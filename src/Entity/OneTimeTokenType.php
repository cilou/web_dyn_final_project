<?php

namespace App\Entity;


class OneTimeTokenType
{
    public const RST_PASSWD_TYPE = 'RST_PASSWD';
    public const NEW_ACCT_TYPE = 'NEW_ACCT';

    private function __construct()
    {
    }
}

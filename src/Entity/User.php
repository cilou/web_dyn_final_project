<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message = "This email is already used")
 * @UniqueEntity(fields={"username"}, message = "This username is already used")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min = 8,
     *     minMessage = "The password must contain at least {{ limit }} characters"
     *    )
     */
    private $password;

    /**
     * @Assert\EqualTo(
     *     propertyPath = "password",
     *     message = "The passwords must match"
     *  )
     */
    private $confirmPassword;

    /**
     *
     */
    private $currentPassword;

    /**
     * @var boolean
     * Used by registration form to require acceptance before creating the account
     */
    private $tos;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSuccessfulLogin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastFailedLogin;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isDisabled;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isEmailConfirmed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRole", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $userRole;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseReview", mappedBy="user", orphanRemoval=true)
     */
    private $courseReviews;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseRegistration", mappedBy="user", orphanRemoval=true)
     */
    private $courseRegistrations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseComment", mappedBy="user", orphanRemoval=true)
     */
    private $courseComments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OneTimeToken", mappedBy="user", orphanRemoval=true)
     */
    private $oneTimeTokens;

    public function __construct()
    {
        $this->courseReviews = new ArrayCollection();
        $this->courseRegistrations = new ArrayCollection();
        $this->courseComments = new ArrayCollection();
        $this->oneTimeTokens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    public function setConfirmPassword($confirmPassword): void
    {
        $this->confirmPassword = $confirmPassword;
    }

    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    public function setCurrentPassword($currentPassword): void
    {
        $this->currentPassword = $currentPassword;
    }

    /**
     * @return bool
     */
    public function isTos(): bool
    {
        if($this->tos == null) {
            return false;
        }
        return $this->tos;
    }

    /**
     * @param bool $tos
     */
    public function setTos(bool $tos): void
    {
        $this->tos = $tos;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getLastSuccessfulLogin(): ?\DateTimeInterface
    {
        return $this->lastSuccessfulLogin;
    }

    public function setLastSuccessfulLogin(?\DateTimeInterface $lastSuccessfulLogin): self
    {
        $this->lastSuccessfulLogin = $lastSuccessfulLogin;

        return $this;
    }

    public function getLastFailedLogin(): ?\DateTimeInterface
    {
        return $this->lastFailedLogin;
    }

    public function setLastFailedLogin(?\DateTimeInterface $lastFailedLogin): self
    {
        $this->lastFailedLogin = $lastFailedLogin;

        return $this;
    }

    public function getIsDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    public function setIsDisabled(bool $isDisabled): self
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsEmailConfirmed() : bool
    {
        return $this->isEmailConfirmed;
    }

    /**
     * @param bool $isEmailConfirmed
     */
    public function setIsEmailConfirmed(bool $isEmailConfirmed): void
    {
        $this->isEmailConfirmed = $isEmailConfirmed;
    }

    public function getUserRole(): ?UserRole
    {
        return $this->userRole;
    }

    public function setUserRole(?UserRole $userRole): self
    {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * @return Collection|CourseReview[]
     */
    public function getCourseReviews(): Collection
    {
        return $this->courseReviews;
    }

    public function addCourseReview(CourseReview $courseReview): self
    {
        if (!$this->courseReviews->contains($courseReview)) {
            $this->courseReviews[] = $courseReview;
            $courseReview->setUser($this);
        }

        return $this;
    }

    public function removeCourseReview(CourseReview $courseReview): self
    {
        if ($this->courseReviews->contains($courseReview)) {
            $this->courseReviews->removeElement($courseReview);
            // set the owning side to null (unless already changed)
            if ($courseReview->getUser() === $this) {
                $courseReview->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseRegistration[]
     */
    public function getCourseRegistrations(): Collection
    {
        return $this->courseRegistrations;
    }

    public function addCourseRegistration(CourseRegistration $courseRegistration): self
    {
        if (!$this->courseRegistrations->contains($courseRegistration)) {
            $this->courseRegistrations[] = $courseRegistration;
            $courseRegistration->setUser($this);
        }

        return $this;
    }

    public function removeCourseRegistration(CourseRegistration $courseRegistration): self
    {
        if ($this->courseRegistrations->contains($courseRegistration)) {
            $this->courseRegistrations->removeElement($courseRegistration);
            // set the owning side to null (unless already changed)
            if ($courseRegistration->getUser() === $this) {
                $courseRegistration->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseComment[]
     */
    public function getCourseComments(): Collection
    {
        return $this->courseComments;
    }

    public function addCourseComment(CourseComment $courseComment): self
    {
        if (!$this->courseComments->contains($courseComment)) {
            $this->courseComments[] = $courseComment;
            $courseComment->setUser($this);
        }

        return $this;
    }

    public function removeCourseComment(CourseComment $courseComment): self
    {
        if ($this->courseComments->contains($courseComment)) {
            $this->courseComments->removeElement($courseComment);
            // set the owning side to null (unless already changed)
            if ($courseComment->getUser() === $this) {
                $courseComment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        $currentRole = $this->getUserRole()->getName();
        return $currentRole ? [$currentRole] : 'ROLE_GUEST';
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|OneTimeToken[]
     */
    public function getOneTimeTokens(): Collection
    {
        return $this->oneTimeTokens;
    }

    public function addOneTimeToken(OneTimeToken $oneTimeToken): self
    {
        if (!$this->oneTimeTokens->contains($oneTimeToken)) {
            $this->oneTimeTokens[] = $oneTimeToken;
            $oneTimeToken->setUser($this);
        }

        return $this;
    }

    public function removeOneTimeToken(OneTimeToken $oneTimeToken): self
    {
        if ($this->oneTimeTokens->contains($oneTimeToken)) {
            $this->oneTimeTokens->removeElement($oneTimeToken);
            // set the owning side to null (unless already changed)
            if ($oneTimeToken->getUser() === $this) {
                $oneTimeToken->setUser(null);
            }
        }

        return $this;
    }
}

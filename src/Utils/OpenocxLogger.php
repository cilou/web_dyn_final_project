<?php
/**
 * Handles customized loggers for the application
 *
 * @author C Leyman
 * @updated_at 2019-01-04
 */
namespace App\Utils;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Handles customized loggers for the application
 */
class OpenocxLogger implements LoggerAwareInterface
{
    private $logger;

    /**
     * OpenocxLogger constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger) {
        $this->setLogger($logger);
    }

    /**
     * @param string $msg The log
     * @param string $logLevel The severity
     */
    public function log(string $msg, string $logLevel): void {
        $this->logger->$logLevel($msg);
    }

    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface $logger
     *
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void {
        $this->logger = $logger;
    }
}
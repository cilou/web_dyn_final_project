<?php
/**
 * Handles the customized logger related to admin actions
 *
 * @author C Leyman
 * @updated_at 2019-01-04
 */
namespace App\Utils;

use Psr\Log\LoggerInterface;

/**
 * Handles the customized logger related to admin actions
 */
class OpenocxAdminLogger extends OpenocxLogger
{
    /**
     * OpenocxAdminLogger constructor.
     * @param LoggerInterface $adminLogger
     */
    public function __construct(LoggerInterface $adminLogger) {
        parent::__construct($adminLogger);
    }
}
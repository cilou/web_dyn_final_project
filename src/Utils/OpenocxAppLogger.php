<?php
/**
 * Handles the customized logger related to the application actions
 *
 * @author C Leyman
 * @updated_at 2019-01-04
 */
namespace App\Utils;

use Psr\Log\LoggerInterface;

/**
 * Handles the customized logger related to the application actions
 */
class OpenocxAppLogger extends OpenocxLogger
{
    /**
     * OpenocxAppLogger constructor.
     * @param LoggerInterface $appLogger
     */
    public function __construct(LoggerInterface $appLogger) {
        parent::__construct($appLogger);
    }
}
<?php
/**
 * Application Helpers
 *
 * @author C Leyman
 * @updated_at 2019-01-04
 */
namespace App\Utils;

use App\Entity\OneTimeToken;
use App\Entity\OneTimeTokenType;
use App\Entity\User;
use App\Repository\OneTimeTokenRepository;
use Jose\Component\Checker\AlgorithmChecker;
use Jose\Component\Checker\ClaimCheckerManager;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\HS256;
use Jose\Component\Signature\Algorithm\HS512;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSTokenSupport;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Checker;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Application Helpers, cannot be instantiated.
 */
class Utils {

    /**
     * Utils constructor. Private, unused.
     */
    private function __construct() {}

    /**
     * Retrieve php.ini upload_max_filesize settings and convert it in bytes
     * @param string $size
     * @return int
     */
    private static function getSizeInBytes(string $size) : int {
        $sizeInt = substr($size, 0, strlen($size) - 1);
        $sizeType = strtolower($size[strlen($size)-1]);

        if(!is_numeric($sizeInt)) {
            return 0;
        }

        $sizeInt = intval($sizeInt);
        //dump($sizeInt);
        //dump($sizeType);

        switch($sizeType) {
            case 'g':
                $sizeInt *= 1024 * 1024 * 1024;
                break;
            case 'm':
                $sizeInt *= 1024 * 1024;
                break;
            case 'k':
                $sizeInt *= 1024;
                break;
            default:
                return 0;
        }

        return $sizeInt;
    }

    /**
     * Return the upload_max_file_size settings in php.ini (in bytes)
     * @return int
     */
    public static function getIniMaxFileSizeInBytes() : int {
        return self::getSizeInBytes(ini_get('upload_max_filesize'));
    }

    /**
     * Return the upload_max_file_size settings in php.ini (in string)
     * @return string
     */
    public static function getIniMaxFileSizeInString() : string {
        return ini_get('upload_max_filesize');
    }

    /**
     * Filter string for specific HTML tags
     * @param string $json A JSON string
     * @return string A filtered and HTML formatted string
     */
    public static function sanitizeQuillEditorInput(string $json) : string {
        $jsonData = json_decode($json, true)["ops"];
        if(empty($jsonData)) {
            throw new Exception("String is not a JSON string");
        }
        $htmlString = "";

        foreach($jsonData as $data) {
            $currentString = "";
            if(isset($data["attributes"])) {
                $attrArray = [];
                foreach($data["attributes"] as $attribute => $value) {
                    if ($value) $attrArray[] = $attribute;
                }

                if(in_array('header', $attrArray)) $currentString .= '<h2>';
                if(in_array('bold', $attrArray)) $currentString .= '<strong>';
                if(in_array('italic', $attrArray)) $currentString .= '<em>';
                if(in_array('underline', $attrArray)) $currentString .= '<u>';

                $currentString .= self::filterHtmlTags($data["insert"], ['<br>', '<h2>', '<strong>', '<em>', '<u>']);

                if(in_array('underline', $attrArray)) $currentString .= '</u>';
                if(in_array('italic', $attrArray)) $currentString .= '</em>';
                if(in_array('bold', $attrArray)) $currentString .= '</strong>';
                if(in_array('header', $attrArray)) $currentString .= '</h2>';

            } else {
                $currentString .= self::filterHtmlTags($data["insert"], ['<br>', '<h2>', '<strong>', '<em>', '<u>']);
            }

            // We remove the last '<br>' from the string (not added by the user but by Quill).
            $posLastBr = strrpos($currentString, '\n');
            if($posLastBr) {
                $currentString = substr($currentString,0, $posLastBr - 1);
            }
            // We convert all '\n' to'<br />'
            $htmlString .= nl2br($currentString);
        }

        return $htmlString;
    }

    /**
     * @param string $str The string to clean up
     * @param [] $allowedTags An array of html tags allowed
     * @return string The string cleaned up
     */
    public static function filterHtmlTags(string $str, $allowedTags = []) : string  {
        if(empty($str)) return $str;
        if(empty($allowedTags)) return strip_tags($str);
        $tagsString = '';
        foreach($allowedTags as $tag) {
            $tagsString .= $tag;
        }

        return strip_tags($str, $tagsString);
    }

    /**
     * Hangle the generation of a signed JWT
     *
     * @param User $user The user requesting the reset of his/her password
     * @param string $type The type of the token to be generated
     * @param int $maxSecondsValidity The number of seconds the token will be valid
     * or the user registering an account
     * @param string $secretKey The secret key stored in .env, the key used to sign all tokens
     * @param string $secretKeyAlgorithm The hashing algorithm used to sign all tokens.
     * Must be either HS256 or HS512.
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxUsersLogger $usersLogger
     *
     * @return null|OneTimeToken Return a OneTimeToken object if the generation was successful,
     * null otherwise
     *
     * @throws \Exception if the $secretKeyAlgorithm specified is not supported
     */
    public static function createJWTOneTimeToken(User $user, string $type ,int $maxSecondsValidity, string $secretKey, string $secretKeyAlgorithm, OpenocxAppLogger $appLogger, OpenocxUsersLogger $usersLogger) {
        // https://web-token.spomky-labs.com/components/signed-tokens-jws/jws-creation
        // The algorithm manager with the HS256 algorithm.
        $algorithmManager = null;
        if($secretKeyAlgorithm === 'HS256') {
            $algorithmManager = AlgorithmManager::create([
                new HS256()
            ]);
        } else if($secretKeyAlgorithm === 'HS512') {
            $algorithmManager = AlgorithmManager::create([
                new HS512()
            ]);
        } else {
            throw new Exception('Supported JWA algorithms are HS256 and HS512');
        }

        // Our key.
        $jwk = JWK::create([
            'kty' => 'oct',
            'k' => $secretKey
        ]);

        // The JSON Converter
        $jsonConverter = new StandardConverter();

        // We instantiate our JWS Builder.
        $jwsBuilder = new JWSBuilder(
            $jsonConverter,
            $algorithmManager
        );

        // We generate the UUID type 4 (random), throws an Exception if unsuccessful
        $uuid = Uuid::uuid4();
        $nowTimestamp = time();

        // The payload we want to sign. The payload MUST be a string hence we use our JSON Converter.
        $payload = $jsonConverter->encode([
            'iat' => $nowTimestamp,
            'nbf' => $nowTimestamp,
            'exp' => $nowTimestamp + $maxSecondsValidity,
            'iss' => 'OpenOCX',
            'uuid' => $uuid,
            'type' => $type
        ]);

        $jws = $jwsBuilder
            ->create()
            ->withPayload($payload)
            ->addSignature($jwk, ['alg' => $secretKeyAlgorithm])
            ->build();

        // We want to send it to the audience. Before that, it must be serialized.
        // We will use the compact serialization mode. This is the most common mode as it is
        // URL safe and very compact. Perfect for a use in a web context!
        $serializer = new CompactSerializer($jsonConverter);

        // We serialize the signature at index 0 (we only have one signature).
        $token = null;
        $tokenToPersist = null;
        try {
            $token = $serializer->serialize($jws, 0);
            $tokenToPersist = new OneTimeToken();
            $tokenToPersist->setUuid($uuid);
            $tokenToPersist->setType($type);
            $tokenToPersist->setUser($user);
            $tokenToPersist->setIat($nowTimestamp);
            $tokenToPersist->setExp($nowTimestamp + $maxSecondsValidity);
            $tokenToPersist->setIsValid(true);
            $tokenToPersist->setTokenString($token);

            $msg = 'Utils createJWTOneTimeToken: ' .
                'Token ' . $uuid->toString() . ' generated for user ' . $user->getUsername() . ' with email ' .
                $user->getEmail() . '. iat: ' . $nowTimestamp . ', exp: ' .
                ($nowTimestamp + $maxSecondsValidity);
            $appLogger->log($msg, LogLevel::INFO);
            $usersLogger->log($msg, LogLevel::INFO);

        } catch(\Exception $ex) {
            $appLogger->log('Utils createJWTOneTimeToken: ' . $ex->getMessage(), LogLevel::ERROR);
            $usersLogger->log(
                'Utils createJWTOneTimeToken: ' .
                    'Token could not be generated for user ' . $user->getUsername() . ' with email ' . $user->getEmail(),
                LogLevel::ERROR);
        }

        return $tokenToPersist;
    }

    /**
     * Verify JWT validity
     *
     * @param string $token The token received from the user
     * @param string $secretKey The secret key stored in .env, the key used to sign all tokens
     * @param string $secretKeyAlgorithm The hashing algorithm used to sign all tokens.
     * @param OpenocxLogger $logger To log any error
     *
     * @return bool return true if token is valid, fasle otherwise
     */
    public static function isJWTTokenValid(string $token, string $secretKey, string $secretKeyAlgorithm, OpenocxLogger $logger) {
        if(empty($token)) return false;

        $jsonConverter = new StandardConverter();
        $serializer = new CompactSerializer($jsonConverter);
        $jws = null;
        try {
            // We try to load the token.
            $jws = $serializer->unserialize($token);
        } catch(\Exception $ex) {
            $logger->log('Utils isJWTTokenValid: ' . $ex->getMessage(), LogLevel::ERROR);
            return false;
        }

        // The algorithm manager with the HS256 algorithm.
        $algorithmManager = null;
        if($secretKeyAlgorithm === 'HS256') {
            $algorithmManager = AlgorithmManager::create([
                new HS256()
            ]);
        } else if($secretKeyAlgorithm === 'HS512') {
            $algorithmManager = AlgorithmManager::create([
                new HS512()
            ]);
        } else {
            throw new Exception('Supported JWA algorithms are HS256 and HS512');
        }

        $jwsVerifier = new JWSVerifier($algorithmManager);

        // Our key.
        $jwk = JWK::create([
            'kty' => 'oct',
            'k' => $secretKey
        ]);

        // Check signature validity
        $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
        if(!$isVerified) {
            $logger->log('Utils isJWTTokenValid: Invalid signature for token', LogLevel::ERROR);
            return false;
        }

        // In the following example, we want to check the alg header parameter for the
        // signed tokens (JWS) received by our application.
        // https://web-token.spomky-labs.com/components/header-checker
        $headerCheckerManager = HeaderCheckerManager::create(
            [
                new AlgorithmChecker([$secretKeyAlgorithm])
            ],
            [
                new JWSTokenSupport()
            ]
        );

        // The first parameter is the JWT to check, the second one is the index of
        // the signature/recipient, the third one is the mandatory fields the header must have
        try {
            $headerCheckerManager->check($jws, 0, ['alg']);
        } catch(\Exception $ex) {
            $logger->log('Utils isJWTTokenValid: ' . $ex->getMessage(), LogLevel::ERROR);
            return false;
        }

        // We create a manager able to check the iat (Issued At), nbf (Not Before)
        // and exp (Expiration) claims.
        // https://web-token.spomky-labs.com/components/claim-checker
        $claimCheckerManager = ClaimCheckerManager::create(
            [
                new Checker\IssuedAtChecker(),
                new Checker\NotBeforeChecker(),
                new Checker\ExpirationTimeChecker()
            ]
        );

        // When instantiated, call the method check to check the claims of a JWT object.
        // This method only accept an array. You have to retrieve this array by converting the
        // JWT payload.
        // An exception will be thrown if the iss, iat, nbf, or exp claim is missing.
        $claims = $jsonConverter->decode($jws->getPayload());
        try {
            $claimCheckerManager->check($claims, ['iss', 'iat', 'nbf', 'exp', 'uuid', 'type']);
        } catch(Checker\InvalidClaimException $ex) {
            $logger->log('Utils isJWTTokenValid: ' . $ex->getMessage(), LogLevel::ERROR);
            return false;
        } catch(Checker\MissingMandatoryClaimException $ex2) {
            $logger->log('Utils isJWTTokenValid: ' . $ex2->getMessage(), LogLevel::ERROR);
            return false;
        }

        // Token is valid
        return true;
    }

    /**
     * Read a Symfony log file
     *
     * @param string $fullFilePath The full path to the file
     *
     * @return array The logs in an associative array
     *
     * @throws \Exception If the file is not found or cannot be read
     */
    public static function readLogFile(string $fullFilePath) {
        // For portability, it is strongly recommended that you always use the 'b' flag
        // when opening files with fopen().
        // https://secure.php.net/manual/en/function.fopen.php
        $handle = fopen($fullFilePath, 'rb');
        if(!$handle) {
            throw new \Exception('Utils::readLogFile: ' . $fullFilePath . ' not found');
        }

        $logs = [];
        while(($buffer = fgets($handle, 4096)) !== false) {
            // We extract the datetime stored at the beginning of the line
            $datetime = trim(substr($buffer, 1, strpos($buffer, ']') - 1));
            // Logs are stored in UTC time. We need to convert the time to our timezone
            $datetime = new \DateTime($datetime);
            $datetime->setTimezone(new \DateTimeZone('Europe/Brussels'));
            $datetime = $datetime->format('Y-m-d H:i:s');
            // We remove the datetime from the beginning of the line and extract the type
            $buffer = substr($buffer, strpos($buffer, ']') + 1);
            $type = trim(substr($buffer, 0, strpos($buffer, ':')));
            $type = trim(substr($type, strpos($type, '.') + 1));

            // We remove the type from the neginning of the line and extract the rest (log text)
            $buffer = substr($buffer, strpos($buffer, ':') + 1);

            // We store all of the above into an array
            $logs[] = ['datetime' => $datetime, 'type' => $type, 'log' => $buffer];
        }

        if(!feof($handle)) {
            throw new \Exception('Utils::readLogFile: ' . $fullFilePath .
                ' unexpected fgets() fail (!feof)');
        }

        fclose($handle);

        return array_reverse($logs);
    }
}
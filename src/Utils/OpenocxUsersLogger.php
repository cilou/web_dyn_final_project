<?php
/**
 * Handles the customized logger related to user actions
 *
 * @author C Leyman
 * @updated_at 2019-01-04
 */
namespace App\Utils;

use Psr\Log\LoggerInterface;

/**
 * Handles the customized logger related to user actions.
 */
class OpenocxUsersLogger extends OpenocxLogger
{
    /**
     * OpenocxUsersLogger constructor.
     * @param LoggerInterface $usersLogger
     */
    public function __construct(LoggerInterface $usersLogger) {
        parent::__construct($usersLogger);
    }
}
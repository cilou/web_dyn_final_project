<?php
/**
 * Handles Users Management at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/users
 */

namespace App\Controller;

use App\Entity\OneTimeToken;
use App\Entity\User;
use App\Entity\UserRole;
use App\Form\UserType;
use App\Utils\OpenocxAdminLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of users by authenticated ROLE_ADMIN.
 *
 * List, edit, disable, delete (no creation of users).
 * Access is logged.
 */
class AdminUsersController extends AbstractController
{
    /**
     * List all users and provide filters related to their status
     *
     * @Route("/admin/dashboard/users", name="admin_dashboard_users", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardUsers(Request $request, OpenocxAdminLogger $adminLogger)
    {
//        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsers ' .
                '/admin/dashboard/users',
            LogLevel::INFO
        );

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $cookie = null;
        if ($request->cookies->has('menu')) {
            $cookie = $request->cookies->get('menu');
            $request->cookies->remove('menu');
            $delCookie = new Cookie('menu', $cookie, time() - 3600, '/', null, false, true);
            $response = new Response();
            $response->headers->setCookie($delCookie);
            $response->sendHeaders();
        }

        /*
         * Deleted Users
         */
        if ($request->isMethod('POST') && $request->get('isDeleted')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isDeleted' => true]);

            $response = $this->render('user/dashboard.html.twig', [
                'deletedUsers' => $users
            ]);

            $response->headers->setCookie(new Cookie('menu', 'deletedUsers', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;
            // If editing or enabling/disabling a deletedUser, we need to stay or come back on the deletedUsers
            // part of the page. Else, if we click on another button, we need to get there
        } else if ($cookie == 'deletedUsers' && $request->cookies->has('sf_redirect')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isDeleted' => true]);

            $response = $this->render('user/dashboard.html.twig', [
                'deletedUsers' => $users
            ]);
            return $response;

            /*
             * Disabled Users
             */
        } else if ($request->isMethod('POST') && $request->get('isDisabled')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isDeleted' => false, 'isDisabled' => true]);

            $response = $this->render('user/dashboard.html.twig', [
                'disabledUsers' => $users
            ]);

            $response->headers->setCookie(new Cookie('menu', 'disabledUsers', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;

        } else if ($cookie == 'disabledUsers' && $request->cookies->has('sf_redirect')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isDeleted' => false, 'isDisabled' => true]);

            $response = $this->render('user/dashboard.html.twig', [
                'disabledUsers' => $users
            ]);
            return $response;

            /*
             * Moderators
             */
        } else if ($request->isMethod('POST') && $request->get('role') == 'MODERATOR') {
            $role = 'ROLE_' . $request->get('role');
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);

            $response = $this->render('user/dashboard.html.twig', [
                'MODERATOR' => $users
            ]);
            $response->headers->setCookie(new Cookie('menu', 'MODERATOR', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;

        } else if ($cookie == 'MODERATOR' && $request->cookies->has('sf_redirect')) {
            $role = 'ROLE_' . $cookie;
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'MODERATOR' => $users
            ]);
            return $response;

            /*
             * Course Managers
             */
        } else if ($request->isMethod('POST') && $request->get('role') == 'COURSE_MANAGER') {
            $role = 'ROLE_' . $request->get('role');
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'COURSE_MANAGER' => $users
            ]);
            $response->headers->setCookie(new Cookie('menu', 'COURSE_MANAGER', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;

        } else if ($cookie == 'COURSE_MANAGER' && $request->cookies->has('sf_redirect')) {
            $role = 'ROLE_' . $cookie;
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'COURSE_MANAGER' => $users
            ]);
            return $response;

            /*
             * Admin
             */
        } else if ($request->isMethod('POST') && $request->get('role') == 'ADMIN') {
            $role = 'ROLE_' . $request->get('role');
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'ADMIN' => $users
            ]);
            $response->headers->setCookie(new Cookie('menu', 'ADMIN', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;

        } else if ($cookie == 'ADMIN' && $request->cookies->has('sf_redirect')) {
            $role = 'ROLE_' . $cookie;
            $userRole = $this->getDoctrine()->getRepository(UserRole::class)->findBy(['name' => $role]);
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userRole' => $userRole, 'isDeleted' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'ADMIN' => $users
            ]);
            return $response;

            /*
             * All active users
             */
        } else {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isDeleted' => false, 'isDisabled' => false]);
        }

        $response = $this->render('user/dashboard.html.twig', [
            'users' => $users
        ]);
        return $response;
    }

    /**
     * Edit a user settings.
     *
     * @Route("/admin/dashboard/users/edit-{id}", name="admin_dashboard_users_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The user ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardUsersEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersEdit ' .
            '/admin/dashboard/users/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user == null) {
            $this->addFlash(
                'error',
                'User not found'
            );
            return $this->redirectToRoute('admin_dashboard_users');
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'User updated successfully'
            );

            return $this->redirectToRoute('admin_dashboard_users');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'user_edit' => true,
        ]);
    }

    /**
     * Enable or disable a user.
     *
     * @Route("/admin/dashboard/users/enable-{id}", name="admin_dashboard_users_enable")
     *
     * @param int $id The user ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardUsersEnable(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersEnable ' .
            '/admin/dashboard/users/enable-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user == null) {
            $this->addFlash(
                'error',
                'User not found'
            );
        } else {
            $user->setIsDisabled(!$user->getIsDisabled());
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            if ($user->getIsDisabled()) {
                $this->addFlash(
                    'success',
                    'User has been disabled'
                );
            } else {
                $this->addFlash(
                    'success',
                    'User has been enabled'
                );
            }
        }

        return $this->redirectToRoute('admin_dashboard_users');
    }

    /**
     * Delete a user.
     *
     * @Route("/admin/dashboard/users/delete-{id}", name="admin_dashboard_users_delete")
     *
     * @param int $id The User ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardUsersDelete(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersDelete ' .
            '/admin/dashboard/users/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user == null) {
            $this->addFlash(
                'error',
                'User not found'
            );
        } else {
            $user->setIsDeleted(true);
            $user->setIsDisabled(true);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'User has been deleted'
            );
        }

        return $this->redirectToRoute('admin_dashboard_users');
    }

    /**
     * List all existing tokens (password resets, registration of a new account email confirmation)
     *
     * @Route("/admin/dashboard/users/tokens", name="admin_dashboard_users_tokens")
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardUsersToken(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersToken ' .
            '/admin/dashboard/users/tokens',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $tokens = $this->getDoctrine()->getRepository(OneTimeToken::class)->findAll();

        $response = $this->render('user/dashboard.html.twig', [
            'tokens' => $tokens
        ]);

        return $response;
    }

    /**
     * Allow to revoke a specific token
     *
     * @Route("/admin/dashboard/users/tokens/delete-{id}", name="admin_dashboard_users_tokens_delete")
     *
     * @param int $id The token id
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardUsersTokenRevoke(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersTokenRevoke ' .
            '/admin/dashboard/users/tokens/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $token = $this->getDoctrine()->getRepository(OneTimeToken::class)->findOneBy(['id' => $id]);
        if ($token == null) {
            $this->addFlash(
                'error',
                'Token not found'
            );
        } else {
            $this->getDoctrine()->getManager()->remove($token);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Token has been revoked'
            );
        }

        $tokens = $this->getDoctrine()->getRepository(OneTimeToken::class)->findAll();

        $response = $this->render('user/dashboard.html.twig', [
            'tokens' => $tokens
        ]);

        return $response;
    }

    /**
     * Allow to revoke all expired tokens
     *
     * @Route("/admin/dashboard/users/tokens/delete/expired", name="admin_dashboard_users_tokens_delete_expired")
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardUsersTokenRevokeExpired(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminUsersController dashboardUsersTokenRevokeExpired ' .
            '/admin/dashboard/users/tokens/delete-expired',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $expiredTokens = $this->getDoctrine()->getRepository(OneTimeToken::class)
            ->findAllWithExpLessThanCurrentTimestamp(time());

        //dd(time(), $expiredTokens);

        if(count($expiredTokens) > 0) {
            foreach($expiredTokens as $expiredToken) {
                $this->getDoctrine()->getManager()->remove($expiredToken);
                $this->getDoctrine()->getManager()->flush();
            }

            $this->addFlash(
                'success',
                'All expired tokens have been revoked'
            );
        } else {
            $this->addFlash(
                'success',
                'There are currently no expired tokens in the database'
            );
        }

        $tokens = $this->getDoctrine()->getRepository(OneTimeToken::class)->findAll();

        $response = $this->render('user/dashboard.html.twig', [
            'tokens' => $tokens
        ]);

        return $response;
    }
}
<?php
/**
 * Controller for links towards information related to the website (ToS, ...).
 *
 * @author C Leyman
 * @updated_at 2018-12-24
 * @base_url /information
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * All actions related to the access of information about the website.
 *
 * Term of Services, Social Network Links, privacy Policy, and so on.
 */
class InformationController extends AbstractController
{
    /**
     * Term of Services rendering.
     *
     * @Route("/information/tos", name="information_tos", methods="GET")
     *
     * @return Response
     */
    public function tos() : Response
    {
        return $this->render('information/tos.html.twig');
    }
}

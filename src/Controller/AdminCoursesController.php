<?php
/**
 * Handles Courses Management at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/courses
 */

namespace App\Controller;

use App\Entity\Course;
use App\Form\CourseType;
use App\Utils\OpenocxAdminLogger;
use App\Utils\Utils;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of courses by authenticated ROLE_ADMIN.
 *
 * CRUD, publish/unpublish. Access is logged.
 */
class AdminCoursesController extends AbstractController
{
    /**
     * List all courses. A cookie is used to filter them in relation to their status.
     *
     * @Route("/admin/dashboard/courses", name="admin_dashboard_courses", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardCourses(Request $request, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCoursesController dashboardCourses ' .
            '/admin/dashboard/courses',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $cookie = null;
        if ($request->cookies->has('menu')) {
            $cookie = $request->cookies->get('menu');
            $request->cookies->remove('menu');
            $delCookie = new Cookie('menu', $cookie, time() - 3600, '/', null, false, true);
            $response = new Response();
            $response->headers->setCookie($delCookie);
            $response->sendHeaders();
        }

        if ($request->isMethod('POST') && $request->get('isPublished') == 'no') {
            $courses = $this->getDoctrine()->getRepository(Course::class)->findBy(['isPublished' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'inactiveCourses' => $courses
            ]);
            $response->headers->setCookie(new Cookie('menu', 'inactiveCourses', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $response;

        } else if ($cookie == 'inactiveCourses' && $request->cookies->has('sf_redirect')) {
            $courses = $this->getDoctrine()->getRepository(Course::class)->findBy(['isPublished' => false]);
            $response = $this->render('user/dashboard.html.twig', [
                'inactiveCourses' => $courses
            ]);
            return $response;
        }

        $courses = $this->getDoctrine()->getRepository(Course::class)->findBy(['isPublished' => true]);

        return $this->render('user/dashboard.html.twig', [
            'courses' => $courses
        ]);
    }

    /**
     * Creation of a course by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/courses/new", name="admin_dashboard_courses_new", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardCoursesNew(Request $request, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCoursesController dashboardCoursesNew ' .
            '/admin/dashboard/courses/new',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Add course'
        ]);
        // https://api.symfony.com/4.1/Symfony/Component/Form/FormInterface.html#method_handleRequest
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //dump($user->getImage()); // C:\wamp64\tmp\php1827.tmp
            /** @var UploadedFile $file */
            $file = $form->get('image')->getData();
            if ($file != 'courseDefault.jpeg') {

                try {
                    if (!in_array($file->guessExtension(), ['png', 'jpg', 'jpeg'])) {
                        $this->addFlash(
                            'error',
                            'Only jpg/jpeg and png files are supported'
                        );
                        return $this->render('user/dashboard.html.twig', [
                            'form' => $form->createView(),
                            'course_edit' => true
                        ]);

                    } else if ($file->getSize() > Utils::getIniMaxFileSizeInBytes()) {
                        $this->addFlash(
                            'error',
                            'File is too big (> ' . Utils::getIniMaxFileSizeInString() . ')'
                        );
                        return $this->render('user/dashboard.html.twig', [
                            'form' => $form->createView(),
                            'course_edit' => true
                        ]);
                    }
                } catch (FileNotFoundException $ex) {
                    $this->addFlash(
                        'error',
                        'File might be too big (max size is ' . Utils::getIniMaxFileSizeInString() . ')'
                    );
                    return $this->render('user/dashboard.html.twig', [
                        'form' => $form->createView(),
                        'course_edit' => true
                    ]);
                } catch (FileException $ex2) {
                    $this->addFlash(
                        'error',
                        'An error occurred while uploading the image'
                    );
                    return $this->render('user/dashboard.html.twig', [
                        'form' => $form->createView(),
                        'course_edit' => true
                    ]);
                }

                $md5 = md5(uniqid());
                $filename = $md5 . '.' . $file->guessExtension();
                try {
                    $file->move($this->getParameter('course_main_img_dir'), $filename);

                } catch (FileException $ex) {
                    $ex->getMessage();
                    $this->addFlash(
                        'error',
                        'File could not be moved'
                    );
                    return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                }
                $course->setImage($filename);
            }

            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $course->setCreatedAt($now);
            $course->setUpdatedAt($now);
            $course->setIsPublished(false);

            $this->getDoctrine()->getManager()->persist($course);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Course created successfully'
            );

            $response = new Response();
            $response->headers->setCookie(new Cookie('menu', 'inactiveCourses', time() + 3600, '/', null, false, true));
            $response->sendHeaders();
            return $this->redirectToRoute('admin_dashboard_courses');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'course_edit' => true
        ]);
    }

    /**
     * Editing of a course by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/courses/edit-{id}", name="admin_dashboard_courses_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The course ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardCoursesEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCoursesController dashboardCoursesEdit ' .
            '/admin/dashboard/courses/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if ($course == null) {
            $this->addFlash(
                'error',
                'Course not found'
            );
            return $this->redirectToRoute('admin_dashboard_courses');
        }

        $oldFilename = $course->getImage();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $file */
            $file = $form->get('image')->getData();

            // no image update
            if (is_string($file) && $file == 'courseDefault.jpeg') {
                $course->setImage($oldFilename);
                $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
                $course->setUpdatedAt($now);
                $this->getDoctrine()->getManager()->persist($course);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Course updated successfully'
                );
                return $this->redirectToRoute('admin_dashboard_courses');

            } else {
                try {
                    if (!in_array($file->guessExtension(), ['png', 'jpg', 'jpeg'])) {
                        $this->addFlash(
                            'error',
                            'Only jpg/jpeg and png files are supported'
                        );
                        return $this->render('user/dashboard.html.twig', [
                            'form' => $form->createView(),
                            'course_edit' => true
                        ]);

                    } else if ($file->getSize() > Utils::getIniMaxFileSizeInBytes()) {
                        $this->addFlash(
                            'error',
                            'File is too big (>' . Utils::getIniMaxFileSizeInString() . ')'
                        );
                        return $this->render('user/dashboard.html.twig', [
                            'form' => $form->createView(),
                            'course_edit' => true
                        ]);
                    }
                } catch (FileNotFoundException $ex) {
                    $this->addFlash(
                        'error',
                        'File might be too big (max size is ' . Utils::getIniMaxFileSizeInString() . ')'
                    );
                    return $this->render('user/dashboard.html.twig', [
                        'form' => $form->createView(),
                        'course_edit' => true
                    ]);
                } catch (FileException $ex2) {
                    $this->addFlash(
                        'error',
                        'An error occurred while uploading the image'
                    );
                    return $this->render('user/dashboard.html.twig', [
                        'form' => $form->createView(),
                        'course_edit' => true
                    ]);
                }

                $md5 = md5(uniqid());
                $filename = $md5 . '.' . $file->guessExtension();
                try {
                    $file->move($this->getParameter('course_main_img_dir'), $filename);
                    if ($oldFilename != 'courseDefault.jpeg') {
                        unlink($this->getParameter('course_main_img_dir') . '/' . $oldFilename);
                    }

                } catch (FileException $ex) {
                    $ex->getMessage();
                    $this->addFlash(
                        'error',
                        'File could not be moved'
                    );
                    return $this->render('registration/index.html.twig', [
                        'form' => $form->createView()
                    ]);
                } catch (\ErrorException $ex2) {
                }

                $course->setImage($filename);
            }

            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $course->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($course);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Course updated successfully'
            );

            return $this->redirectToRoute('admin_dashboard_courses');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'course_edit' => true
        ]);
    }

    /**
     * Publishing or unpublishing a course by authenticated ROLE_ADMIN.
     * Only courses with no enrollments will be allowed to be unpublished.
     *
     * @Route("/admin/dashboard/courses/publish-{id}", name="admin_dashboard_courses_publish")
     *
     * @param int $id The course ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardCoursesPublish(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCoursesController dashboardCoursesPublish ' .
            '/admin/dashboard/courses/publish-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if ($course == null) {
            $this->addFlash(
                'error',
                'Course not found'
            );
        } else if (count($course->getCourseRegistrations()) > 0) {
            $this->addFlash(
                'error',
                'Course cannot be unpublished since it has active registrations'
            );
        } else {
            $course->setIsPublished(!$course->getIsPublished());
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $course->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($course);
            $this->getDoctrine()->getManager()->flush();

            if ($course->getIsPublished()) {
                $this->addFlash(
                    'success',
                    'Course published'
                );
            } else {
                $this->addFlash(
                    'success',
                    'Course unpublished'
                );
            }
        }

        return $this->redirectToRoute('admin_dashboard_courses');
    }

    /**
     * Deletion of a course by authenticated ROLE_ADMIN. The course must be unpublished first.
     * Only courses with no comments, reviews, or enrollments will be allowed to be suppressed.
     *
     * @Route("/admin/dashboard/courses/delete-{id}", name="admin_dashboard_courses_delete")
     *
     * @param int $id The course ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardCoursesDelete(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCoursesController dashboardCoursesDelete ' .
            '/admin/dashboard/courses/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        //$course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        $course = $this->getDoctrine()->getRepository(Course::class)->findOneBy(['id' => $id, 'isPublished' => false]);
        if ($course == null) {
            $this->addFlash(
                'error',
                'Course not found'
            );
        } else if (count($course->getCourseComments()) > 0 ||
            count($course->getCourseReviews()) > 0 ||
            count($course->getCourseRegistrations()) > 0) {
            $this->addFlash(
                'error',
                'Course cannot be deleted, integrity constraints'
            );
        } else {

            try {
                $filename = $course->getImage();
                if ($filename != 'courseDefault.jpeg') {
                    unlink($this->getParameter('course_main_img_dir') . '/' . $filename);
                }
            } catch (FileException $ex) {
                $ex->getMessage();
                $this->addFlash(
                    'error',
                    'File ' . $filename . 'could not be removed'
                );
            }

            $this->getDoctrine()->getManager()->remove($course);
            $this->getDoctrine()->getManager()->flush();

            if ($this->getDoctrine()->getRepository(Course::class)->find($id) == null) {
                $this->addFlash(
                    'success',
                    'Course deleted'
                );
            } else {
                $this->addFlash(
                    'error',
                    'Course not deleted'
                );
            }
        }

        return $this->redirectToRoute('admin_dashboard_courses');
    }
}
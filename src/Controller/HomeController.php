<?php
/**
 * Home controller
 *
 * @author C Leyman
 * @updated_at 2018-12-24
 * @base_url /
 */

namespace App\Controller;

use App\Entity\Course;
use App\Entity\CourseReview;
use App\Entity\User;
use App\Repository\CourseReviewRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Website root.
 */
class HomeController extends AbstractController
{
    /**
     * Render the landing page.
     *
     * @Route("/", name="home")
     *
     * @return Response
     */
    public function index()
    {
        $courseReviewRepository = $this->getDoctrine()->getRepository(CourseReview::class);
        $courseReviews = $courseReviewRepository->findAllPublishedOrderedByRatingDesc(3, 20);

        // Send only reviews about published courses
        $filteredReviews = [];
        foreach ($courseReviews as $review) {
            if($review->getCourse()->getIsPublished()) {
                $filteredReviews[] = $review;
            }
        }

        return $this->render('home/index.html.twig', [
            'courseReviews' => $filteredReviews
        ]);
    }
}

<?php
/**
 * Handles Courses Logs at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-31
 * @base_url /admin/dashboard/logs
 */

namespace App\Controller;

use App\Utils\OpenocxAdminLogger;
use App\Utils\Utils;
use DateTime;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All reading actions related to the management of logs by authenticated ROLE_ADMIN.
 *
 * Access is logged.
 */
class AdminLogsController extends AbstractController
{
    /**
     * Listing of all logs for authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/logs/{type}", name="admin_dashboard_logs")
     *
     * @param OpenocxAdminLogger $adminLogger Access to this URL is logged
     * @param string $type The type of logs we want to retrieve
     *
     * @return Response
     */
    public function dashboardLogs(OpenocxAdminLogger $adminLogger, string $type)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminController /admin/dashboard/logs/' . $type,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // For the table in the view to be build
        $logs = [['datetime' => '0000-00-00', 'type' => 'Dummy', 'log' => 'Dummy record']];
        $appLog = false;
        $usersLog = false;
        $adminLog = false;

        switch($type) {
            case 'applog':
                $appLog = true;
                break;
            case 'userslog':
                $usersLog = true;
                break;
            case 'adminlog':
                $adminLog = true;
                break;
            default:
                break;
        }

        return $this->render('user/dashboard.html.twig', [
            'logs' => $logs,
            'appLog' => $appLog,
            'usersLog' => $usersLog,
            'adminLog' => $adminLog
        ]);
    }

    /**
     * Return the logs in JSON for the type and date specified
     *
     * @Route("/admin/dashboard/logs/{type}/json", name="admin_dashboard_logs_json")
     *
     * @param Request $request The Request received by the server
     * @param OpenocxAdminLogger $adminLogger Access to this URL is logged
     * @param string $type The type of logs we want to retrieve
     *
     * @return Response
     */
    public function dashboardLogsJson(Request $request, OpenocxAdminLogger $adminLogger, string $type)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminController /admin/dashboard/logs/' . $type,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $logs = [];

        if(!empty($request->get('date'))) {
            $nowStr = $request->get('date');
        } else {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $nowStr = $now->format('Y-m-d');
        }

        $logDir = $this->getParameter('monolog_logs');
        $logFile = $logDir . $type . '-' . $nowStr . '.log';

        try {
            $logs = Utils::readLogFile($logFile);
        } catch (\Exception $ex) {
            $adminLogger->log(
                'AdminLogsController dashboardLogs: ' . $ex->getMessage(),
                LogLevel::ERROR
            );
        }

        $response = new Response(json_encode($logs));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
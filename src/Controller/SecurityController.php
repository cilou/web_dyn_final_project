<?php
/**
 * Users security actions controller
 *
 * @updated_at 2018-12-24
 * @base_urls /login, /logout
 */

namespace App\Controller;

use App\Entity\OneTimeToken;
use App\Entity\OneTimeTokenType;
use App\Entity\ResetPasswordRequestUser;
use App\Entity\User;
use App\Form\ResetPasswordRequestType;
use App\Form\ResetPasswordType;
use App\Utils\OpenocxAppLogger;
use App\Utils\OpenocxUsersLogger;
use App\Utils\Utils;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Originally generated with make:auth, takes charge of authentication
 * related actions.
 * Added JWT functions (password reset)
 *
 * Related files are
 *  App\Security\LoginFormAuthenticator
 *  security.yaml
 *  services.yaml
 *  ...
 */
class SecurityController extends AbstractController
{
    /**
     * Render the login form for the user to authenticate and send
     * the data to be processed if no error occurred.
     * @see config security.yaml
     * @see \App\Security\LoginFormAuthenticator.php
     *
     * @Route("/login", name="login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils) : Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * Handle logout action for a user.
     *
     * @Route("/logout", name="logout")
     *
     * @param OpenocxUsersLogger $usersLogger
     * @return Response
     */
    public function logout(OpenocxUsersLogger $usersLogger) : Response
    {
        // Apparently this url is completely bypassed by the firewall...
        ///dd($this->getUser()->getUsername());
        if($this->isGranted('ROLE_MEMBER')) {
            $usersLogger->log(
                'Logout from ' . $_SERVER['REMOTE_ADDR'] .
                ' for username ' . $this->getUser()->getUsername(),
                LogLevel::INFO);
        }

        return $this->render('home/index.html.twig');
    }

    /**
     * Handle reset password requests from unauthenticated users
     *
     * @Route("/reset-password-request", name="reset_password_request")
     *
     * @param Request $request The request received by the server
     * @param CsrfTokenManagerInterface $csrfTokenManager Protection against CSRF attacks
     * @param \Swift_Mailer $mailer For sending external emails
     * @param LoggerInterface $logger For logging activities and possible errors
     *
     * @return RedirectResponse|Response
     */
    public function resetPasswordRequest(Request $request, \Swift_Mailer $mailer, OpenocxUsersLogger $usersLogger, OpenocxAppLogger $appLogger)
    {
        // Only unauthenticated users can have access to this URL
        if($this->isGranted('ROLE_MEMBER')) {
            return $this->redirectToRoute('home');
        }

        $user = new ResetPasswordRequestUser();
        $form = $this->createForm(ResetPasswordRequestType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            //$now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $email = $user->getEmail();
            $clientIPAddress = $_SERVER['REMOTE_ADDR'];
            $usersLogger->log(
                "SecurityController resetPasswordRequest from : $clientIPAddress " .
                    'requested a password reset for email ' . $email,
                LogLevel::INFO);

//            if(!filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
//                $this->addFlash(
//                    'error',
//                    'The email address provided is invalid.'
//                );
//
//                return $this->render('security/reset-password-request.html.twig',
//                    ['form' => $form->createView()]);
//            }
            /**
             * @var User $user
             */
            $user = $this->getDoctrine()->getRepository(User::class)
                ->findOneBy(['email' => $email]);

            if ($user == null) {
                $this->addFlash(
                    'error',
                    'Unknown email address'
                );
                $usersLogger->log(
                    "SecurityController resetPasswordRequest from : $clientIPAddress " .
                        'unknown email ' . $email,
                    LogLevel::WARNING);
            // We do not generate reset password tokens for disabled or deleted accounts
            } else if($user->getIsdisabled() || $user->getIsDeleted()) {
                $this->addFlash(
                    'error',
                    'Unknown email address'
                );
                $usersLogger->log(
                    "SecurityController resetPasswordRequest from : $clientIPAddress " .
                    'disabled or deleted user ' . $user->getEmail(),
                    LogLevel::WARNING);
            // We do not generate reset password tokens for admin accounts
            } else if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $this->addFlash(
                    'error',
                    'Unknown email address'
                );
                $usersLogger->log(
                    "SecurityController resetPasswordRequest from : $clientIPAddress " .
                    'user has admin privileges ' . $user->getEmail(),
                    LogLevel::ALERT);
            // We do not generate reset password tokens for non-confirmed email addresses
            } else if(!$user->getIsEmailConfirmed()) {
                $this->addFlash(
                    'error',
                    'Email address exists but was not confirmed. Please contact us.'
                );
                $usersLogger->log(
                    "SecurityController resetPasswordRequest from : $clientIPAddress " .
                    'Valid but non confirmed email address ' . $user->getEmail(),
                    LogLevel::WARNING);
            } else {
                // We delete previously generated password reset type token(s) (we should only have one)
                $existingResetPasswordTokens = $this->getDoctrine()
                    ->getRepository(OneTimeToken::class)
                    ->findBy(['type' => OneTimeTokenType::RST_PASSWD_TYPE, 'user' => $user]);
                foreach ($existingResetPasswordTokens as $existingResetPasswordToken) {
                    $this->getDoctrine()->getManager()->remove($existingResetPasswordToken);
                    $this->getDoctrine()->getManager()->flush();
                }
                // We generate a new reset password type token
                try {
                    $token = Utils::createJWTOneTimeToken(
                        $user,
                        OneTimeTokenType::RST_PASSWD_TYPE,
                        900,
                        $this->getParameter('jose_secret_key'),
                        $this->getParameter('jose_secret_key_alg'),
                        $appLogger,
                        $usersLogger
                    );
                } catch(\Exception $ex) {
                    $appLogger->log(
                        'SecurityController resetPasswordRequest : ' .
                        'Request password token could not be generated: ' . $ex->getMessage(),
                        LogLevel::ERROR
                    );
                    $usersLogger->log(
                        'SecurityController resetPasswordRequest : ' .
                            'Request password token could not be generated for user ' . $user->getEmail(),
                        LogLevel::ERROR);
                }

                // debug -- read token generated
                // -----------------------------------------------------------------
                //dd($this->isJWTTokenValid($token, $logger));
//                $jsonConverter = new StandardConverter();
//                $serializer = new CompactSerializer($jsonConverter);
//                dd(json_decode(($serializer->unserialize($token))->getPayload()));
                // -----------------------------------------------------------------
                // end debug

                if(!empty($token)) {
                    $this->getDoctrine()->getManager()->persist($token);
                    $this->getDoctrine()->getManager()->flush();
                    if($this->sendResetPasswordLink($token->getTokenString(), $user, $mailer, $appLogger)) {
                        $msg = 'SecurityController resetPasswordRequest: email successfully sent to ' . $user->getEmail();
                        $usersLogger->log($msg, LogLevel::INFO);
                        $this->addFlash(
                            'success',
                            'A reset link with instructions on how to reset your password has been sent 
                            to the email address provided.'
                        );
                    } else {
                        $usersLogger->log(
                            'SecurityController resetPasswordRequest: ' . 'email could not be sent to ' .
                                $user->getEmail(),
                            LogLevel::ERROR
                        );
                        $this->addFlash(
                            'error',
                            'An error occurred while sending the email, please try again later. 
                            If the error persists, please contact us.'
                        );
                    }
                } else {
                    $usersLogger->log(
                        'SecurityController resetPasswordRequest: ' . 'email could not be sent to ' .
                            $user->getEmail() . ', token has not been generated',
                        LogLevel::ERROR
                    );
                    $this->addFlash(
                        'error',
                        'An error occurred while generating the email, please try again later. 
                        If the error persists, please contact us.'
                    );
                }
            }
        }

        return $this->render('security/reset-password-request.html.twig',
            ['form' => $form->createView()]);
    }

    /**
     * Reset the password of the user if the token is valid
     *
     * @Route("/reset-password", name="reset_password")
     *
     * @param Request $request The request received by the server (the token in the url here)
     * @param UserPasswordEncoderInterface $encoder Used to hash the password before persist it
     * @param LoggerInterface $logger Log any error
     * @param \Swift_Mailer $mailer To send a confirmation email
     *
     * @return RedirectResponse|Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $encoder, OpenocxAppLogger $appLogger, OpenocxUsersLogger $usersLogger, \Swift_Mailer $mailer) {

        // Only unauthenticated users can have access to this URL
        if($this->isGranted('ROLE_MEMBER')) {
            return $this->redirectToRoute('home');
        }

        // If token is not set in the url or empty, redirect to homepage
        if(empty($request->get('token'))) {
            return $this->redirectToRoute('home');
        }

        // If token is invalid (expiration, signature, structure)
        $token = $request->get('token');
        if(!Utils::isJWTTokenValid(
            $token,
            $this->getParameter('jose_secret_key'),
            $this->getParameter('jose_secret_key_alg'),
            $appLogger)) {
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );

            //$logger->warning('/reset-password?token= ' . $token . ' => invalid token from ' .  $_SERVER['REMOTE_ADDR']);
            $msg = 'SecurityController resetPassword: /reset-password?token= ' .
                ' => invalid token from ' .  $_SERVER['REMOTE_ADDR'];
            $usersLogger->log($msg, LogLevel::WARNING);

            return $this->redirectToRoute('reset_password_request');
        }

        // extract uuid field in token payload to find if it exists in the database
        // and if it has not been revoked
        $jsonConverter = new StandardConverter();
        $serializer = new CompactSerializer($jsonConverter);
        $jws = null;
        try {
            // We try to load the token.
            $jws = $serializer->unserialize($token);
        } catch (\Exception $ex) {
            $appLogger->log(
                'SecurityController resetPassword: ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            $usersLogger->log(
                'SecurityController resetPassword: token valid but error occurred during unserialization',
                LogLevel::ERROR
            );
            $this->addFlash(
                'error',
                'An error occurred. Please try again.'
            );

            return $this->redirectToRoute('reset_password_request');
        }

        $uuid = json_decode($jws->getPayload())->uuid;
        $oneTimeTokenRepository = $this->getDoctrine()->getRepository(OneTimeToken::class);
        /**
         * @var OneTimeToken $oneTimeToken
         */
        $oneTimeToken = null;
        // If we are submitting, it means that the token has already been set to false when the form was
        // loaded. This ensure that a token cannot be reused twice. When submitted, the token is deleted.
        // todo Other tests to do here ? Is it possible to bypass this control ? To check !!!
        if(!$request->isMethod('POST')) {
            $oneTimeToken = $oneTimeTokenRepository->findOneBy(['uuid' => $uuid, 'isValid' => true]);
        } else {
            $oneTimeToken = $oneTimeTokenRepository->findOneBy(['uuid' => $uuid, 'isValid' => false]);
        }

        if($oneTimeToken == null) {
            $usersLogger->log(
                'SecurityController resetPassword: Token ' . $uuid . ' valid but no valid token found ' .
                    'in the database! Probably revoked.',
                LogLevel::WARNING
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
            // If an invalid token is still in the db, we must delete it. Happens if a user refresh the page
            // instead of submitting the form. The token is marked as invalid since it must only be used once.
            $oneTimeToken = $oneTimeTokenRepository->findOneBy(['uuid' => $uuid, 'isValid' => false]);
            if($oneTimeToken != null) {
                // We delete the token from the database
                $this->getDoctrine()->getManager()->remove($oneTimeToken);
                $this->getDoctrine()->getManager()->flush();
            }

            return $this->redirectToRoute('reset_password_request');
        } else if($oneTimeToken->getType() != OneTimeTokenType::RST_PASSWD_TYPE) {
            $usersLogger->log(
                'SecurityController resetPassword: Token ' . $uuid . ' is not of valid type. It must be ' .
                    OneTimeTokenType::RST_PASSWD_TYPE . '.',
                LogLevel::ERROR);
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reset_password_request');
        }

        // Token found, we revoke it as it can only be used once the url was it but the form is not submitted yet.
        $oneTimeToken->setIsValid(false);
        $this->getDoctrine()->getManager()->persist($oneTimeToken);
        $this->getDoctrine()->getManager()->flush();

        // We get the user to whom the token has been assigned
        /**
         * @var User $user
         */
        $user = $oneTimeToken->getUser();
        if($user == null) {
            $usersLogger->log(
                'SecurityController resetPassword: Token ' . $uuid .
                    ' valid but user not found in the database!',
            LogLevel::ALERT
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );

            // We delete the token from the database
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reset_password_request');
        }

        // This should not happen since we don't generate reset pass tokens for admins
        if(in_array('ROLE_ADMIN', $user->getRoles())) {
            $usersLogger->log(
                'SecurityController resetPassword: Token ' . $uuid . ' valid but user has ROLE_ADMIN role!',
                LogLevel::ALERT
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );

            // We delete the token from the database
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reset_password_request');
        }

        // This should not happen since we don't generate reset pass tokens for invalid users
        if($user->getIsDeleted() || $user->getIsDisabled()) {
            $usersLogger->log(
                'SecurityController resetPassword: User ' . $user->getUsername() .
                    ' with email ' . $user->getEmail() .
                    ' is disabled or deleted and request reset of his/her password',
                LogLevel::WARNING
            );
            $this->addFlash(
                'error',
                'An error occurred. Please contact us.'
            );

            // We delete the token from the database
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            // todo redirect to contact form
            return $this->redirectToRoute('reset_password_request');
        }

        // We generate the form for the view
        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            // We check if the token expires (inavlidate token if browser is kept open
            // for a long period of time, ...)
            // We permit 2 minutes of 'slack' in case the user clicked on the link
            // at the time limit)
            if(($oneTimeToken->getExp() + 120) < time()) {
                $usersLogger->log(
                    'SecurityController resetPassword: Token ' . $uuid . ' for user ' .
                        $oneTimeToken->getUser()->getUsername() . ' with email ' .
                        $oneTimeToken->getUser()->getEmail() .
                        ' expired during submission (exp: ' .
                        $oneTimeToken->getExp() . ')',
                    LogLevel::WARNING);
                $this->addFlash(
                    'error',
                    'The link has expired during your submission. Request a new one and try again.'
                );
                // We delete the token from the database
                $this->getDoctrine()->getManager()->remove($oneTimeToken);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('reset_password_request');
            }
            // We delete the token from the database
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setUpdatedAt($now);
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $usersLogger->log(
                'SecurityController resetPassword: Password successfully changed for user ' .
                    $user->getUsername() . ' with email ' . $user->getEmail(),
                LogLevel::INFO);
            $this->addFlash(
                'success',
                'Your password has been successfully changed. Please login.'
            );

            if(!$this->sendResetPasswordConfirmation($user, $mailer, $appLogger)) {
                $usersLogger->log(
                    'SecurityController resetPassword: Confirmation email could not be sent to ' .
                        $user->getEmail(),
                    LogLevel::ERROR);
            } else {
                $usersLogger->log(
                    'SecurityController resetPassword: Confirmation email sent successfully to ' .
                    $user->getEmail(),
                    LogLevel::INFO);
            }

            return $this->redirectToRoute('login');
        }

        return $this->render('security/reset-password.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Handle the email building + sending when a password reset has been requested
     *
     * @param String $token The JWT token generated
     * @param User $user The user requesting the reset of his/her password
     * @param \Swift_Mailer $mailer The service used to transport the email
     * @param LoggerInterface $logger To log possible errors
     *
     * @return bool return true if the email was sent, false otherwise.
     */
    private function sendResetPasswordLink(String $token, User $user, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger) {
        $url =  'http://localhost' . $this->generateUrl('reset_password') . '?token=' . $token;
        $message = (new \Swift_Message('[OpenOCX]Reset Password Request'))
            ->setFrom('do-not-reply@openocx.org')
            ->setTo($user->getEmail())
            ->setBody($this->render('security/reset-password-link-email.html.twig', [
                'user' => $user,
                'url' => $url
                ]),
                'text/html'
            );

        try {
            $mailer->send($message);
            $msg = 'SecurityController sendResetPasswordLink: email successfully sent to ' . $user->getEmail();
            //$usersLogger->log($msg, LogLevel::INFO);
            $appLogger->log($msg, LogLevel::INFO);
        } catch(\Exception $ex) {
            $appLogger->log(
                'SecurityController sendResetPasswordLink: ' . $ex->getMessage(),
                LogLevel::ERROR
            );
//            $usersLogger->log(
//                'SecurityController sendResetPasswordLink: ' . 'email could not be sent to ' . $user->getEmail(),
//                LogLevel::ERROR
//            );
            return false;
        }

        return true;
    }

    /**
     * Handle the email building + sending when a password reset has been done
     *
     * @param User $user The user requesting the reset of his/her password
     * @param \Swift_Mailer $mailer The service used to transport the email
     * @param LoggerInterface $logger To log possible errors
     *
     * @return bool return true if the email was sent, false otherwise.
     */
    private function sendResetPasswordConfirmation(User $user, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger) {
        $url =  'http://localhost' . $this->generateUrl('reset_password_request');
        $message = (new \Swift_Message('[OpenOCX]You have a new Password!'))
            ->setFrom('do-not-reply@openocx.org')
            ->setTo($user->getEmail())
            ->setBody($this->render('security/reset-password-confirmation-email.html.twig', [
                'user' => $user,
                'url' => $url
            ]),
                'text/html'
            );

        try {
            $mailer->send($message);
            $appLogger->log(
                'SecurityController sendResetPasswordConfirmation: message sent successfully to ' .
                    $user->getEmail(),
                LogLevel::INFO);
        } catch(\Exception $ex) {
            $appLogger->log(
                'SecurityController sendResetPasswordConfirmation: ' . $ex->getMessage(),
                LogLevel::ERROR);
            return false;
        }

        return true;
    }
}

<?php
/**
 * Handles Courses Reviews at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/reviews
 */

namespace App\Controller;

use App\Entity\CourseReview;
use App\Form\CourseReviewType;
use App\Utils\OpenocxAdminLogger;
use App\Utils\Utils;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of courses reviews by authenticated ROLE_ADMIN.
 *
 * List, edit, publish/unpublish.
 * Access is logged.
 */
class AdminReviewsController extends AbstractController
{
    /**
     * List reviews.
     *
     * @Route("/admin/dashboard/reviews", name="admin_dashboard_reviews", methods={"POST", "GET"})
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardReviews(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminReviewsController dashboardReviews ' .
                '/admin/dashboard/reviews',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $reviews = $this->getDoctrine()->getRepository(CourseReview::class)->findAll();
        return $this->render('user/dashboard.html.twig', [
            'reviews' => $reviews
        ]);
    }

    /**
     * Edit a specific review.
     *
     * @Route("/admin/dashboard/reviews/edit-{uid}-{cid}", name="admin_dashboard_reviews_edit")
     *
     * @param Request $request The request received by the server
     * @param int $uid The user ID (owner)
     * @param int $cid The course ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardReviewsEdit(Request $request, int $uid, int $cid, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminReviewsController dashboardReviewsEdit ' .
            '/admin/dashboard/reviews/edit-' . $uid . '-' . $cid,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $review = $this->getDoctrine()
            ->getRepository(CourseReview::class)
            ->findOneBy(['user' => $uid, 'course' => $cid]);

        if($review == null) {
            $this->addFlash(
                'error',
                'Review not found'
            );

            return $this->redirectToRoute('admin_dashboard_reviews');
        }

        $form = $this->createForm(CourseReviewType::class, $review);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update review'
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $review->setUpdatedAt($now);
            $review->setReview(Utils::filterHtmlTags($review->getReview(), ['<h2>', '<strong>', '<em>', '<u>', '<br>']) . ' (Edited by a moderator)');
            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Review updated successfully'
            );

            return $this->redirectToRoute('admin_dashboard_reviews');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'review_edit' => true
        ]);
    }

    /**
     * Publish or unpublish a review.
     *
     * @Route("/admin/dashboard/reviews/publish-{uid}-{cid}", name="admin_dashboard_reviews_publish")
     *
     * @param int $uid The user ID (owner)
     * @param int $cid The course ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardReviewsPublish(int $uid, int $cid, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminReviewsController dashboardReviewsPublish ' .
            '/admin/dashboard/reviews/publish-' . $uid . '-' . $cid,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $review = $this->getDoctrine()
            ->getRepository(CourseReview::class)
            ->findOneBy(['user' => $uid, 'course' => $cid]);
        if($review == null) {
            $this->addFlash(
                'error',
                'Review not found'
            );
        } else {
            $review->setIsPublished(!$review->getIsPublished());
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $review->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();

            if($review->getIsPublished()) {
                $this->addFlash(
                    'success',
                    'Review published'
                );
            } else {
                $this->addFlash(
                    'success',
                    'Review unpublished'
                );
            }
        }

        return $this->redirectToRoute('admin_dashboard_reviews');
    }
}

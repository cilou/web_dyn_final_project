<?php
/**
 * Handles Courses Categories at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/categories
 */

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Utils\OpenocxAdminLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of course categories by authenticated ROLE_ADMIN.
 *
 * CRUD. Access is logged.
 */
class AdminCategoriesController extends AbstractController
{
    /**
     * Listing of all course categories by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/categories", name="admin_dashboard_categories", methods={"POST", "GET"})
     *
     * @param LoggerInterface $logger Access to this URL is logged
     *
     * @return Response
     */
    public function dashboardCategories(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCategoriesController dashboardCategories ' .
            '/admin/dashboard/categories',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        return $this->render('user/dashboard.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * Editing of a specific category by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/categories/edit-{id}", name="admin_dashboard_categories_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The category ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardCategoriesEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCategoriesController dashboardCategoriesEdit ' .
            '/admin/dashboard/categories/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update category'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $check = $this->getDoctrine()->getRepository(Category::class)
                ->findOneBy(['name' => $category->getName()]);
            if ($check != null && $check->getId() != $id) {
                $this->addFlash(
                    'error',
                    'Category already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($category);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Category updated successfully'
                );

                return $this->redirectToRoute('admin_dashboard_categories');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'category_edit' => true
        ]);
    }

    /**
     * Deletion of a specific category by authenticated ROLE_ADMIN. Deletion is permitted only if
     * the category is not in use.
     *
     * @Route("/admin/dashboard/categories/delete-{id}", name="admin_dashboard_categories_delete")
     *
     * @param int $id The category ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardCategoriesDelete(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCategoriesController dashboardCategoriesDelete ' .
            '/admin/dashboard/categories/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        if ($category == null) {
            $this->addFlash(
                'error',
                'Category not found'
            );
        } else if (count($category->getCourses()) > 0) {
            $this->addFlash(
                'error',
                'Category is in use and cannot be deleted'
            );
        } else {
            $this->getDoctrine()->getManager()->remove($category);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Category has been successfully deleted'
            );
        }

        return $this->redirectToRoute('admin_dashboard_categories');
    }

    /**
     * Creation of a new category by authenticated ROLE_ADMIN. Name must be unique.
     *
     * @Route("/admin/dashboard/categories/new", name="admin_dashboard_categories_new")
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardCategoriesNew(Request $request, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCategoriesController dashboardCategoriesNew ' .
            '/admin/dashboard/categories/new',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Add category'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $check = $this->getDoctrine()->getRepository(Category::class)
                ->findOneBy(['name' => $category->getName()]);
            if ($check != null) {
                $this->addFlash(
                    'error',
                    'Category already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($category);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Category created successfully'
                );

                return $this->redirectToRoute('admin_dashboard_categories');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'category_edit' => true
        ]);
    }
}
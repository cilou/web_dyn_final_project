<?php
/**
 * Handles User Roles Management at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/roles
 */

namespace App\Controller;

use App\Entity\UserRole;
use App\Form\UserRoleType;
use App\Utils\OpenocxAdminLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of roles by authenticated ROLE_ADMIN.
 *
 * CRUD. Access is logged.
 */
class AdminRolesController extends AbstractController
{

    /**
     * List available roles.
     *
     * @Route("/admin/dashboard/roles", name="admin_dashboard_roles", methods={"POST", "GET"})
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardRoles(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminRolesController dashboardRoles ' .
            '/admin/dashboard/roles',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $roles = $this->getDoctrine()->getRepository(UserRole::class)->findAll();
        return $this->render('user/dashboard.html.twig', [
            'roles' => $roles
        ]);
    }

    /**
     * Edit a specific role. Name must be unique on the system.
     *
     * @Route("/admin/dashboard/roles/edit-{id}", name="admin_dashboard_roles_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The role ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardRolesEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminRolesController dashboardRolesEdit ' .
                '/admin/dashboard/roles/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $role = $this->getDoctrine()->getRepository(UserRole::class)->find($id);
        $form = $this->createForm(UserRoleType::class, $role);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update role'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $check = $this->getDoctrine()->getRepository(UserRole::class)
                ->findOneBy(['name' => $role->getName()]);
            if ($check != null && $check->getId() != $id) {
                $this->addFlash(
                    'error',
                    'Role already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($role);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Role updated successfully'
                );

                return $this->redirectToRoute('admin_dashboard_roles');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'role_edit' => true
        ]);
    }

    /**
     * Delete a specific role if not in use.
     *
     * @Route("/admin/dashboard/roles/delete-{id}", name="admin_dashboard_roles_delete")
     *
     * @param int $id The role ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardRolesDelete(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminRolesController dashboardRolesDelete ' .
            '/admin/dashboard/roles/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $role = $this->getDoctrine()->getRepository(UserRole::class)->find($id);
        if ($role == null) {
            $this->addFlash(
                'error',
                'Role not found'
            );
        } else if (count($role->getUsers()) > 0) {
            $this->addFlash(
                'error',
                'Role is in use and cannot be deleted'
            );
        } else {
            $this->getDoctrine()->getManager()->remove($role);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Role has been successfully deleted'
            );
        }

        return $this->redirectToRoute('admin_dashboard_roles');
    }

    /**
     * Create a new role. Name must be unique on the system.
     *
     * @Route("/admin/dashboard/roles/new", name="admin_dashboard_roles_new")
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardRolesNew(Request $request, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminRolesController dashboardRolesNew ' .
                '/admin/dashboard/roles/new',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $role = new UserRole();
        $form = $this->createForm(UserRoleType::class, $role);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Add role'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $check = $this->getDoctrine()->getRepository(UserRole::class)
                ->findOneBy(['name' => $role->getName()]);

            if ($check != null) {
                $this->addFlash(
                    'error',
                    'Role already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($role);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Role created successfully'
                );

                return $this->redirectToRoute('admin_dashboard_roles');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'role_edit' => true
        ]);
    }
}
<?php
/**
 * Users settings and actions controller
 *
 * @author C Leyman
 * @updated_at 2018-12-24
 * @base_url /register
 */

namespace App\Controller;

use App\Entity\OneTimeToken;
use App\Entity\OneTimeTokenType;
use App\Entity\User;
use App\Entity\UserRegistration;
use App\Entity\UserRole;
use App\Form\RegistrationType;
use App\Utils\OpenocxAppLogger;
use App\Utils\OpenocxUsersLogger;
use App\Utils\Utils;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * All actions related to users self registration on the system.
 */
class RegistrationController extends AbstractController
{
    /**
     * Handle user registration
     *
     * @Route("/register", name="registration")
     *
     * @param Request $request The request received by the server
     * @param UserPasswordEncoderInterface $encoder The encoder object for password hashing
     * @param \Swift_Mailer $mailer
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxUsersLogger $usersLogger
     * @return RedirectResponse|Response
     * @see file /config/packages/security.yaml
     *
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger, OpenocxUsersLogger $usersLogger)
    {
        // Only unauthenticated users can have access to this URL
        if($this->isGranted('ROLE_MEMBER')) {
            return $this->redirectToRoute('home');
        }

        $user = new UserRegistration();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $usersLogger->log(
                'registrationController index /register : Registration submission from ' .
                $_SERVER['REMOTE_ADDR'],
                LogLevel::INFO
            );
            //dump($user->getImage()); // C:\wamp64\tmp\php1827.tmp
            // https://symfony.com/doc/current/controller/upload_file.html
            /** @var UploadedFile $file */
            $file = $form->get('image')->getData();
            if ($file != 'userDefault.png') {
                try {
                    if (!in_array($file->guessExtension(), ['png', 'jpg', 'jpeg'])) {
                        $this->addFlash(
                            'error',
                            'Only jpg/jpeg and png files are supported'
                        );
                        return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                    //fixme use constant or new image class constant for max size
                    } else if ($file->getSize() > 102400) {
                        $this->addFlash(
                            'error',
                            'File is too big (> 100 Ko)'
                        );
                        return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                    }
                } catch (FileNotFoundException $ex) {
                    $this->addFlash(
                        'error',
                        'File might be too big (max system size is ' .
                        Utils::getIniMaxFileSizeInString() . ' and max supported size here is 100Ko)'
                    );
                    return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                } catch (FileException $ex2) {
                    $this->addFlash(
                        'error',
                        'An error occurred while uploading the image'
                    );
                    return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                }

                $filename = $user->getUsername() . '.' . $file->guessExtension();
                try {
                    $file->move($this->getParameter('user_img_profile_dir'), $filename);

                } catch (FileException $ex) {
                    $ex->getMessage();
                    $this->addFlash(
                        'error',
                        'File could not be moved'
                    );
                    return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
                }
            } else {
                $filename = $file;
            }
            $newUser = new User();

            $newUser->setFirstname($user->getFirstname());
            $newUser->setLastname($user->getLastname());
            $newUser->setUsername($user->getUsername());
            $newUser->setEmail($user->getEmail());
            $newUser->setImage($filename);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $newUser->setCreatedAt($now);
            $newUser->setUpdatedAt($now);
            $newUser->setIsDisabled(false);
            $newUser->setIsDeleted(false);
            $newUser->setIsEmailConfirmed(false);
            $newUser->setPassword($encoder->encodePassword($newUser, $user->getPassword()));
            $roleRepository = $this->getDoctrine()->getRepository(UserRole::class);
            $role = $roleRepository->findOneByName('ROLE_MEMBER');
            $newUser->setUserRole($role);

            $this->getDoctrine()->getManager()->persist($newUser);
            $this->getDoctrine()->getManager()->flush();

            $usersLogger->log(
                'RegistrationController index /register: Successful account creation for user ' .
                $newUser->getUsername(),
                LogLevel::INFO
            );

            // Not generating the $token is not critical here, just for email confirmation.
            // If an exception occurs, a link exists in the user's profile to retry.
            $token = null;
            try {
                $token = Utils::createJWTOneTimeToken(
                    $newUser,
                    OneTimeTokenType::NEW_ACCT_TYPE,
                    604800, // 1 week validity
                    $this->getParameter('jose_secret_key'),
                    $this->getParameter('jose_secret_key_alg'),
                    $appLogger,
                    $usersLogger
                );
            } catch(\Exception $ex) {
                $appLogger->log(
                    'RegistrationController index /register: ' .
                    'New account email validation token could not be generated: ' . $ex->getMessage(),
                    LogLevel::ERROR
                );
                $usersLogger->log(
                    'RegistrationController index /register: ' .
                    'New account email validation token could not be generated',
                    LogLevel::ERROR
                );
            }

            // The account has already been created in the db and an exception about sending an email
            // is not critical for the user here.
            if(!empty($token)) {
                $this->getDoctrine()->getManager()->persist($token);
                $this->getDoctrine()->getManager()->flush();
                try {
                    if(!$this->sendAccountCreationConfirmation($newUser, $token->getTokenString(), $mailer, $appLogger)) {
                        $usersLogger->log(
                            'RegistrationController /register index: Confirmation email could not be sent to '
                            . $newUser->getEmail(),
                            LogLevel::ERROR
                        );
                    } else {
                        $usersLogger->log(
                            'RegistrationController /register index: Confirmation email successfully sent to '
                            . $newUser->getEmail(),
                            LogLevel::INFO
                        );
                    }
                } catch (\Exception $ex) {
                    $appLogger->log(
                        'RegistrationController index /register: call to createJWTOneTimeToken => ' . $ex->getMessage(),
                        LogLevel::ERROR
                    );
                    $usersLogger->log(
                        'RegistrationController /register index: ' .
                        'Token could not be generated for user ' . $user->getUsername() .
                        ' with email ' . $user->getEmail(),
                        LogLevel::ERROR
                    );
                }
            }

            $msg = 'RegistrationController index /register: New account successfully created for user ' .
                        $newUser->getUsername() . ' from ' . $_SERVER['REMOTE_ADDR'];
            $usersLogger->log($msg, LogLevel::INFO);
            $appLogger->log($msg, LogLevel::INFO);
            $this->addFlash(
                'success',
                'Your brand new account has been successfully created. Please log in.'
            );

            return $this->redirectToRoute('login');
        }

        return $this->render('registration/index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Resend email confirmation email to the user
     *
     * @Route("/register/confirm/request", name="registration_confirmation_request")
     *
     * @param \Swift_Mailer $mailer
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxUsersLogger $usersLogger
     * @return RedirectResponse
     */
    public function sendConfirmRegistrationEmailAddressLink(\Swift_Mailer $mailer, OpenocxAppLogger $appLogger, OpenocxUsersLogger $usersLogger) {

        if(!$this->isGranted('ROLE_MEMBER')) {
            return $this->redirectToRoute('home');
        }

        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('user_dashboard_settings');
        }

        $token = null;
        $username = $this->getUser()->getUsername();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $username]);
        if($user == null) {
            $this->addFlash(
                'error',
                'User could not be found!'
            );
            $appLogger->log(
                'User ' . $username . ' cannot be found but request sent from an authenticated user',
                LogLevel::ALERT
            );

            return $this->redirectToRoute('logout');
        }

        // We delete previously generated account confirmation type token(s) (we should only have one)
        $existingResetPasswordTokens = $this->getDoctrine()
            ->getRepository(OneTimeToken::class)
            ->findBy(['type' => OneTimeTokenType::NEW_ACCT_TYPE, 'user' => $user]);
        foreach ($existingResetPasswordTokens as $existingResetPasswordToken) {
            $this->getDoctrine()->getManager()->remove($existingResetPasswordToken);
            $this->getDoctrine()->getManager()->flush();
        }
        // We generate a new account confirmation type token
        try {
            $token = Utils::createJWTOneTimeToken(
                $user,
                OneTimeTokenType::NEW_ACCT_TYPE,
                604800, // 1 week validity
                $this->getParameter('jose_secret_key'),
                $this->getParameter('jose_secret_key_alg'),
                $appLogger,
                $usersLogger
            );
        } catch(\Exception $ex) {
            $appLogger->log(
                'RegistrationController sendConfirmRegistrationEmailAddressLink /register/confirm/request: ' .
                ' call to createJWTOneTimeToken => ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            $usersLogger->log(
                'RegistrationController sendConfirmRegistrationEmailAddressLink /register/confirm/request: ' .
                'Token could not be generated for user ' . $user->getUsername() .
                ' with email ' . $user->getEmail(),
                LogLevel::ERROR
            );
        }

        // The account has already been created in the db and an exception about sending an email
        // is not critical for the user here.
        if(!empty($token)) {
            $this->getDoctrine()->getManager()->persist($token);
            $this->getDoctrine()->getManager()->flush();

            if(!$this->sendEmailValidationConfirmationRequest($user, $token->getTokenString(), $mailer, $appLogger)) {
                $usersLogger->log(
                    'RegistrationController sendConfirmRegistrationEmailAddressLink: ' .
                    'Confirmation email could not be sent to ' . $user->getEmail(),
                    LogLevel::ERROR
                );
                $this->addFlash(
                    'error',
                    'An error occurred. Please try again later.'
                );
            } else {
                $usersLogger->log(
                    'RegistrationController sendConfirmRegistrationEmailAddressLink: ' .
                    'Confirmation email successfully sent to ' . $user->getEmail(),
                    LogLevel::INFO
                );
                $this->addFlash(
                    'success',
                    'An email with instructions on how to confirm your email address ' .
                    'has been sent to the address provided.'
                );
            }
        }

        return $this->redirectToRoute('user_dashboard_settings');
    }

    /**
     * Handle reset password requests from unauthenticated users
     *
     * @Route("/register/confirm", name="registration_confirmation")
     *
     * @param Request $request The request received by the server
     * @param \Swift_Mailer $mailer For sending external emails
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxUsersLogger $usersLogger
     * @return RedirectResponse|Response
     */
    public function confirmRegistrationEmailAddress(Request $request, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger, OpenocxUsersLogger $usersLogger) {

        if(empty($request->get('token'))) {
            return $this->redirectToRoute('home');
        }

        // If token is invalid (expiration, signature, structure)
        $token = $request->get('token');
        if(!Utils::isJWTTokenValid(
            $token,
            $this->getParameter('jose_secret_key'),
            $this->getParameter('jose_secret_key_alg'),
            $appLogger)) {
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
            $usersLogger->log('RegistrationController confirmRegistrationEmailAddress ' .
                '/register/confirm?token= ==> Token Invalid from ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::WARNING
            );

            if($this->isGranted('ROLE_MEMBER')) {
                return $this->redirectToRoute('user_dashboard_settings');
            }

            return $this->redirectToRoute('login');
        }

        // extract uuid field in token payload to find if it exists in the database
        // and if it has not been revoked
        $jsonConverter = new StandardConverter();
        $serializer = new CompactSerializer($jsonConverter);
        $jws = null;
        try {
            // We try to load the token.
            $jws = $serializer->unserialize($token);
        } catch (\Exception $ex) {
            $appLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                ' call to unserialize => ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            $usersLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                'Token could not be unserialized for ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::ERROR
            );
            $this->addFlash(
                'error',
                'An error occurred. Please try again.'
            );

            return $this->redirectToRoute('login');
        }

        $uuid = json_decode($jws->getPayload())->uuid;
        $oneTimeTokenRepository = $this->getDoctrine()->getRepository(OneTimeToken::class);
        /**
         * @var OneTimeToken $oneTimeToken
         */
        $oneTimeToken = $oneTimeTokenRepository->findOneBy(['uuid' => $uuid]);
        if($oneTimeToken == null) {
            $usersLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                'Token ' . $uuid . ' valid but no token found in the database! Deleted.' .
                ' for ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::ERROR
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
        // If the token if of the wrong type
        } else if($oneTimeToken->getType() != OneTimeTokenType::NEW_ACCT_TYPE) {
            $usersLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                'Token ' . $uuid . ' is not of valid type. It must be ' . OneTimeTokenType::NEW_ACCT_TYPE . '.' .
                ' for ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::ERROR
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();
        // If the token validity is set to false in the database (not currently used)
        } else if(!$oneTimeToken->getIsValid()) {
            // We delete the token from the database
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();

            $usersLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                'Token ' . $uuid . ' valid but no token found in the database! Revoked.' .
                ' for ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::ERROR
            );
            $this->addFlash(
                'error',
                'The link provided is invalid. Request a new one and try again.'
            );
        } else {
            $user = $oneTimeToken->getUser();
            $user->setIsEmailConfirmed(true);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->remove($oneTimeToken);
            $this->getDoctrine()->getManager()->flush();
            $this->sendEmailValidationConfirmation($user, $mailer, $appLogger);

            $usersLogger->log(
                'RegistrationController confirmRegistrationEmailAddress /register/confirm: ' .
                'Token ' . $uuid . ' successfully used to confirm email address ' . $user->getEmail() . ' for user ' .
                $user->getUsername() . ' from ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::INFO
            );
            $this->addFlash(
                'success',
                'Your email address has been confirmed.'
            );
        }

        if($this->isGranted('ROLE_MEMBER')) {
            return $this->redirectToRoute('user_dashboard_settings');
        }
        //$_GET['token'] = null;
        return $this->redirectToRoute('login');

    }

    /**
     * Handle the email building + sending when a new account is created
     *
     * @param User $user The user registering
     * @param string $token A JWT string
     * @param \Swift_Mailer $mailer The service used to transport the email
     * @param OpenocxAppLogger $appLogger
     * @return bool return true if the email was sent, false otherwise.
     */
    private function sendAccountCreationConfirmation(User $user, string $token, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger) {
        //$url =  'http://localhost' . $this->generateUrl('reset_password') . '?token=' . $token;
        $url =  'http://localhost' . $this->generateUrl('login');
        $urlEmailConfirmation =  'http://localhost' . $this->generateUrl('registration_confirmation') . '?token=' . $token;
        $message = (new \Swift_Message('[OpenOCX]Welcome to OpenOCX!'))
            ->setFrom('do-not-reply@openocx.org')
            ->setTo($user->getEmail())
            ->setBody($this->render('registration/new-account-confirmation-email.html.twig', [
                'user' => $user,
                'url' => $url,
                'urlEmailConfirmation' => $urlEmailConfirmation
            ]),
                'text/html'
            );

        try {
            $mailer->send($message);
            $appLogger->log(
                'registrationController sendAccountCreationConfirmation: email successfully sent to '
                . $user->getEmail(),
                LogLevel::INFO
            );
        } catch(\Exception $ex) {
            $appLogger->log(
                'registrationController sendAccountCreationConfirmation: email could not be sent to' .
                $user->getEmail() . ' => ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            return false;
        }

        return true;
    }

    /**
     * Handle the email building + sending when a new validation link for emails is requested by a user
     *
     * @param User $user The user registering
     * @param string $token The One Time Token generated
     * @param \Swift_Mailer $mailer The service used to transport the email
     * @param OpenocxAppLogger $appLogger
     * @return bool return true if the email was sent, false otherwise.
     */
    private function sendEmailValidationConfirmationRequest(User $user, string $token, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger) {
        $urlEmailConfirmation =  'http://localhost' . $this->generateUrl('registration_confirmation') . '?token=' . $token;
        $message = (new \Swift_Message('[OpenOCX]Please Confirm your Email Address'))
            ->setFrom('do-not-reply@openocx.org')
            ->setTo($user->getEmail())
            ->setBody($this->render('registration/email-validation-confirmation-email.html.twig', [
                'user' => $user,
                'urlEmailConfirmation' => $urlEmailConfirmation
            ]),
                'text/html'
            );

        try {
            $mailer->send($message);
            $appLogger->log(
                'registrationController sendEmailValidationConfirmationRequest: email successfully sent to '
                . $user->getEmail(),
                LogLevel::INFO
            );
        } catch(\Exception $ex) {
            $appLogger->log(
                'registrationController sendEmailValidationConfirmationRequest: email could not be sent to' .
                $user->getEmail() . ' => ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            return false;
        }

        return true;
    }

    /**
     * Handle the email building + sending when a new account is created
     *
     * @param User $user The user registering
     * @param \Swift_Mailer $mailer The service used to transport the email
     * @param OpenocxAppLogger $appLogger
     * @return bool return true if the email was sent, false otherwise.
     */
    private function sendEmailValidationConfirmation(User $user, \Swift_Mailer $mailer, OpenocxAppLogger $appLogger) {
        //$url =  'http://localhost' . $this->generateUrl('reset_password') . '?token=' . $token;
        $url =  'http://localhost' . $this->generateUrl('login');
        $message = (new \Swift_Message('[OpenOCX]Your email has been confirmed'))
            ->setFrom('do-not-reply@openocx.org')
            ->setTo($user->getEmail())
            ->setBody($this->render('registration/new-account-email-validation-email.html.twig', [
                'user' => $user,
                'url' => $url
            ]),
                'text/html'
            );

        try {
            $mailer->send($message);
            $appLogger->log(
                'registrationController sendEmailValidationConfirmation: email successfully sent to '
                . $user->getEmail(),
                LogLevel::INFO
            );
        } catch(\Exception $ex) {
            $appLogger->log(
                'registrationController sendEmailValidationConfirmation: email could not be sent to' .
                $user->getEmail() . ' => ' . $ex->getMessage(),
                LogLevel::ERROR
            );
            return false;
        }

        return true;
    }
}

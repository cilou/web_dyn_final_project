<?php
/**
 * Handles Courses Comments at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/comments
 */

namespace App\Controller;

use App\Entity\CourseComment;
use App\Form\CourseCommentType;
use App\Utils\OpenocxAdminLogger;
use App\Utils\Utils;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of courses comments by authenticated ROLE_ADMIN.
 *
 * List, edit, publish/unpublish comments.
 * Access is logged.
 */
class AdminCommentsController extends AbstractController
{
    /**
     * Listing of all comments by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/comments", name="admin_dashboard_comments", methods={"POST", "GET"})
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardComments(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCommentsController dashboardComments ' .
            '/admin/dashboard/comments',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $comments = $this->getDoctrine()->getRepository(CourseComment::class)->findAll();
        return $this->render('user/dashboard.html.twig', [
            'comments' => $comments
        ]);
    }

    /**
     * Editing of a specific comment by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/comments/edit-{id}", name="admin_dashboard_comments_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The comment ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardCommentsEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCommentsController dashboardCommentsEdit ' .
            '/admin/dashboard/comments/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $comment = $this->getDoctrine()->getRepository(CourseComment::class)->find($id);

        if ($comment == null) {
            $this->addFlash(
                'error',
                'Comment not found'
            );

            return $this->redirectToRoute('admin_dashboard_comments');
        }

        $form = $this->createForm(CourseCommentType::class, $comment);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update comment'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setUpdatedAt($now);
            $comment->setMessage(Utils::filterHtmlTags($comment->getMessage(), ['<h2>', '<strong>', '<em>', '<u>', '<br>']) . ' (Edited by a moderator)');
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Comment updated successfully'
            );

            return $this->redirectToRoute('admin_dashboard_comments');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'comment_edit' => true
        ]);
    }

    /**
     * Publication or 'unpublication' of a specific comment by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/comments/publish-{id}", name="admin_dashboard_comments_publish")
     *
     * @param int $id The comment ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardCommentsPublish(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminCommentsController dashboardCommentsPublish ' .
            '/admin/dashboard/comments/publish-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $comment = $this->getDoctrine()->getRepository(CourseComment::class)->find($id);
        if ($comment == null) {
            $this->addFlash(
                'error',
                'Comment not found'
            );
        } else {
            $comment->setIsPublished(!$comment->getIsPublished());
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            if ($comment->getIsPublished()) {
                $this->addFlash(
                    'success',
                    'Comment published'
                );
            } else {
                $this->addFlash(
                    'success',
                    'Comment unpublished'
                );
            }
        }

        return $this->redirectToRoute('admin_dashboard_comments');
    }
}
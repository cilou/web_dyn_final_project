<?php
/**
 * Users settings and actions controller
 *
 * @author C Leyman
 * @updated_at 2018-12-24
 * @base_url /users/dashboard
 */

namespace App\Controller;

use App\Entity\CourseComment;
use App\Entity\CourseReview;
use App\Entity\User;
use App\Form\CourseCommentType;
use App\Form\CourseReviewType;
use App\Utils\OpenocxAppLogger;
use App\Utils\OpenocxUsersLogger;
use App\Utils\Utils;
use finfo;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Zend\Cache\Storage\Adapter\Session;

/**
 * All actions related to the users self management through /users/dashboard'.
 *
 * Handling of course enrollments, settings changes, comments management,
 * reviews management, ...
 */
class UserController extends AbstractController
{
    /**
     * Access to user's dashboard if authenticated.
     *
     * @Route("/user/dashboard", name="user_dashboard")
     *
     * @return Response The response returned by the server
     */
    public function dashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        return $this->render('user/dashboard.html.twig');
    }

    /**
     * Access to user' settings if authenticated.
     *
     * @Route("/user/dashboard/settings", name="user_dashboard_settings", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param UserPasswordEncoderInterface $encoder Used for password checks and hashing
     * @param CsrfTokenManagerInterface $csrfTokenManager Used to verify form generated token (CSRF protection)
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxUsersLogger $userLogger
     * @return Response The response returned by the server
     */
    public function dashboardSettings(Request $request, UserPasswordEncoderInterface $encoder, CsrfTokenManagerInterface $csrfTokenManager, OpenocxAppLogger $appLogger, OpenocxUsersLogger $userLogger)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        $user = null;
        //https://symfony.com/doc/current/form/direct_submit.html
        if ($request->isMethod('POST')) {
            $userLogger->log(
                'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                 ' is updating his/her profile from ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::INFO
            );
            $token = new CsrfToken('authenticate', $request->get('_csrf_token'));
            if (!$csrfTokenManager->isTokenValid($token)) {
                $appLogger->log(
                    'UserController dashboardSettings: Invalid CSRF Token for user ' .
                    $this->getUser()->getUsername() . ' from ' . $_SERVER['REMOTE_ADDR'],
                    LogLevel::ALERT
                );
                throw new InvalidCsrfTokenException();
            }

            //$username = $this->get('session')->get(Security::LAST_USERNAME);
            $username = $this->getUser()->getUsername();
            $user = $this->getDoctrine()->getRepository(User::class)
                ->findOneBy(['username' => $username]);

            if($user == null) {
                $this->addFlash(
                    'error',
                    'User not found'
                );
                $appLogger->log(
                    'UserController dashboardSettings: Unknown user ' . $username .
                    ' from ' . $_SERVER['REMOTE_ADDR'],
                    LogLevel::ALERT
                );
                return $this->redirectToRoute('logout');
            }

            $hashedPass = null;
            $currentEmail = $user->getEmail();

            if(!$encoder->isPasswordValid($user, $request->get('currentPassword'))) {
                $this->addFlash(
                    'error',
                    'Current password does not match'
                );

                $userLogger->log(
                    'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                    ' profile not updated (wrong password) from ' . $_SERVER['REMOTE_ADDR'],
                    LogLevel::INFO
                );

                return $this->render('user/dashboard.html.twig', [
                    'settings' => true,
                    'user' => $user,
                ]);
            }
            if ($request->get('newPassword') != null) {
                if ($request->get('newPassword') != $request->get('confirmNewPassword')) {
                    $this->addFlash(
                        'error',
                        'New password and Confirm new password do not match'
                    );

                    $userLogger->log(
                        'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                        ' profile not updated (new password does not match) from ' . $_SERVER['REMOTE_ADDR'],
                        LogLevel::INFO
                    );

                    return $this->render('user/dashboard.html.twig', [
                        'settings' => true,
                        'user' => $user,
                    ]);

                } else {
                    $hashedPass = $encoder->encodePassword($user, $request->get('newPassword'));
                }
            }

            if($request->get('email') != $user->getEmail() && !empty($request->get('email'))) {
                if(!filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                    $this->addFlash(
                        'error',
                        'Email address provided is invalid'
                    );

                    $userLogger->log(
                        'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                        ' profile not updated (new email is invalid) from ' . $_SERVER['REMOTE_ADDR'],
                        LogLevel::INFO
                    );

                    return $this->render('user/dashboard.html.twig', [
                        'settings' => true,
                        'user' => $user,
                    ]);
                }

                $userRepository = $this->getDoctrine()->getRepository(User::class);
                $userWithEmailAddress = $userRepository->findOneBy(['email' => $request->get('email')]);
                if($userWithEmailAddress != null && $userWithEmailAddress->getId() != $user->getId()) {
                    $this->addFlash(
                        'error',
                        'Email address provided is already used'
                    );

                    $userLogger->log(
                        'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                        ' profile not updated (new email is already used) from ' . $_SERVER['REMOTE_ADDR'],
                        LogLevel::INFO
                    );

                    return $this->render('user/dashboard.html.twig', [
                        'settings' => true,
                        'user' => $user,
                    ]);
                } else {
                    $user->setEmail($request->get('email'));
                }

            }

            if(isset($_FILES['image']) && $_FILES['image']['error'] != UPLOAD_ERR_NO_FILE) {

                if($this->isUploadedImageValid(102400)) {

                    $fileExtension = substr($_FILES['image']['name'],
                        strrpos($_FILES['image']['name'],'.'));

                    if(!move_uploaded_file(
                        $_FILES['image']['tmp_name'],
                        $this->getParameter('user_img_profile_dir') .
                        '/' . $user->getUsername() . $fileExtension)) {

                        $this->addFlash(
                            'error',
                            'The file could not be uploaded'
                        );

                        return $this->render('user/dashboard.html.twig', [
                            'settings' => true,
                            'user' => $user,
                        ]);
                    } else {
                        $user->setImage($user->getUsername() . $fileExtension);
                    }
                } else {
                    return $this->render('user/dashboard.html.twig', [
                        'settings' => true,
                        'user' => $user,
                    ]);
                }
            }

            if($hashedPass != null) {
                $user->setPassword($hashedPass);
                $userLogger->log(
                    'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                    ' profile updated (new password) from ' . $_SERVER['REMOTE_ADDR'],
                    LogLevel::INFO
                );
            }
            if($currentEmail != $user->getEmail()) {
                $user->setIsEmailConfirmed(false);
                $userLogger->log(
                    'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                    ' profile updated (new email => previous was 
                    ' . $currentEmail . ', new is ' . $user->getEmail() .
                    ') from ' . $_SERVER['REMOTE_ADDR'],
                    LogLevel::INFO
                );
            }
            $user->setFirstname($request->get('firstname'));
            $user->setLastname($request->get('lastname'));
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setUpdatedAt($now);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $userLogger->log(
                'UserController dashboardSettings: ' . $this->getUser()->getUsername() .
                ' profile successfully updated from ' . $_SERVER['REMOTE_ADDR'],
                LogLevel::INFO
            );

            $this->addFlash(
                'success',
                'Your profile has been updated successfully'
            );
        }
        
        return $this->render('user/dashboard.html.twig', [
            'settings' => true,
            'user' => $user,
        ]);
    }


    /**
     * Access to all courses the user has enrolled in if authenticated.
     *
     * @Route("/user/dashboard/courses", name="user_dashboard_courses", methods={"POST", "GET"})
     *
     * @return RedirectResponse|Response
     */
    public function dashboardCourses()
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        $courses = null;
        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null) {
            return $this->redirectToRoute('logout');
        } else {
            $courses = $user->getCourseRegistrations();
        }

        return $this->render('user/dashboard.html.twig', [
            'courses' => $courses
        ]);
    }

    /**
     * Access to all user's comments if authenticated.
     *
     * @Route("/user/dashboard/comments", name="user_dashboard_comments", methods={"POST", "GET"})
     *
     * @return RedirectResponse|Response
     */
    public function dashboardComments()
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        $comments = null;
        $username = $this->get('session')->get(Security::LAST_USERNAME);

        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null) {
            return $this->redirectToRoute('logout');
        } else {
            $comments= $user->getCourseComments();
        }

        return $this->render('user/dashboard.html.twig', [
            'comments' => $comments
        ]);
    }

    /**
     * Allow authenticated users to edit a owned comment specified by an id.
     *
     * @Route("/user/dashboard/comments/edit-{id}", name="user_dashboard_comments_edit", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param int $id The id of the comment to edit
     *
     * @return RedirectResponse|Response
     */
    public function dashboardCommentsEdit(Request $request, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null) {
            return $this->redirectToRoute('logout');
        }

        $comment = $this->getDoctrine()->getRepository(CourseComment::class)
            ->findOneBy(['id' => $id, 'user' => $user]);
        if($comment == null) {
            $this->addFlash(
                'error',
                'Comment not found'
            );

            return $this->redirectToRoute('user_dashboard_comments');
        }


        $form = $this->createForm(CourseCommentType::class, $comment);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update comment'
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($comment->getIsDeleted()) {
                $comment->setIsPublished(false);
            }
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setUpdatedAt($now);
            $comment->setMessage(Utils::filterHtmlTags($comment->getMessage(), ['<h2>', '<strong>', '<em>', '<u>', '<br>']) . ' (Edited)');
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Comment updated successfully'
            );

            return $this->redirectToRoute('user_dashboard_comments');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'comment_edit' => true
        ]);
    }

    /**
     * Allow user to publish or unpublished a owned comment if authenticated.
     *
     * @Route("/user/dashboard/comments/publish-{id}", name="user_dashboard_comments_publish", methods={"POST", "GET"})
     *
     * @param int $id The id of the comment to publish/unpublish
     *
     * @return RedirectResponse
     */
    public function dashboardCommentsPublish(int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null) {
            return $this->redirectToRoute('logout');
        }

        $comment = $this->getDoctrine()->getRepository(CourseComment::class)
            ->findOneBy(['id' => $id, 'user' => $user]);
        if($comment == null) {
            $this->addFlash(
                'error',
                'Comment not found'
            );

            return $this->redirectToRoute('user_dashboard_comments');
        }

        if(!$comment->getIsDeleted()) {
            $comment->setIsPublished(!$comment->getIsPublished());
        }
        $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
        $comment->setUpdatedAt($now);
        $this->getDoctrine()->getManager()->persist($comment);
        $this->getDoctrine()->getManager()->flush();

        if($comment->getIsPublished()) {
            $this->addFlash(
                'success',
                'Comment published successfully'
            );
        } else {
            $this->addFlash(
                'success',
                'Comment unpublished successfully'
            );
        }

        return $this->redirectToRoute('user_dashboard_comments');
    }

    /**
     * Allow user to delete a owned comment if authenticated.
     *
     * @Route("/user/dashboard/comments/delete-{id}", name="user_dashboard_comments_delete", methods={"POST", "GET"})
     *
     * @param int $id The id of the comment to delete
     *
     * @return RedirectResponse
     */
    public function dashboardCommentsDelete(int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null) {
            return $this->redirectToRoute('logout');
        }

        $comment = $this->getDoctrine()->getRepository(CourseComment::class)
            ->findOneBy(['id' => $id, 'user' => $user]);
        if($comment == null) {
            $this->addFlash(
                'error',
                'Comment not found'
            );

            return $this->redirectToRoute('user_dashboard_comments');
        }

        if(!$comment->getIsDeleted()) {
            $comment->setIsPublished(false);
            $comment->setIsDeleted(true);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Comment deleted successfully'
            );
        } else {
            $this->addFlash(
                'error',
                'Comment has already been deleted'
            );
        }

        return $this->redirectToRoute('user_dashboard_comments');
    }

    /**
     * Allow a user to view all of its reviews if authenticated.
     *
     * @Route("/user/dashboard/reviews", name="user_dashboard_reviews", methods={"POST", "GET"})
     *
     * @return Response
     */
    public function dashboardReviews()
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        $reviews = null;
        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user != null) {
            $reviews= $user->getCourseReviews();
        }

        return $this->render('user/dashboard.html.twig', [
            'reviews' => $reviews,
        ]);
    }

    /**
     * Allow a user to edit a owned review if authenticated.
     *
     * @Route("/user/dashboard/reviews/edit-{uid}-{cid}", name="user_dashboard_reviews_edit", methods={"POST", "GET"})
     *
     * @param Request $request The request received by the server
     * @param int $uid The user ID
     * @param int $cid The course ID
     *
     * @return RedirectResponse|Response
     */
    public function dashboardReviewsEdit(Request $request, int $uid, int $cid)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null || $user->getId() != $uid) {
            return $this->redirectToRoute('logout');
        }

        $review = $this->getDoctrine()->getRepository(CourseReview::class)
            ->findOneBy(['user' => $uid, 'course' => $cid]);

        if($review == null) {
            $this->addFlash(
                'error',
                'Review not found'
            );

            return $this->redirectToRoute('user_dashboard_reviews');
        }

        $form = $this->createForm(CourseReviewType::class, $review);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update review'
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($review->getIsDeleted()) {
                $review->setIsPublished(false);
            }
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $review->setUpdatedAt($now);
            $review->setReview(Utils::filterHtmlTags($review->getReview(), ['<h2>', '<strong>', '<em>', '<u>', '<br>']) . ' (Edited)');
            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Review updated successfully'
            );

            return $this->redirectToRoute('user_dashboard_reviews');
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'review_edit' => true
        ]);
    }

    /**
     * Allow a user to publish/unpublish a owned review if authenticated.
     *
     * @Route("/user/dashboard/reviews/publish-{uid}-{cid}", name="user_dashboard_reviews_publish", methods={"POST", "GET"})
     * @param int $uid The user ID
     * @param int $cid The course ID
     *
     * @return RedirectResponse
     */
    public function dashboardReviewsPublish(int $uid, int $cid)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null || $user->getId() != $uid) {
            return $this->redirectToRoute('logout');
        }

        $review = $this->getDoctrine()->getRepository(CourseReview::class)
            ->findOneBy(['user' => $uid, 'course' => $cid]);

        if($review == null) {
            $this->addFlash(
                'error',
                'Review not found'
            );

            return $this->redirectToRoute('user_dashboard_reviews');
        }

        if(!$review->getIsDeleted()) {
            $review->setIsPublished(!$review->getIsPublished());
        }
        $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
        $review->setUpdatedAt($now);
        $this->getDoctrine()->getManager()->persist($review);
        $this->getDoctrine()->getManager()->flush();

        if($review->getIsPublished()) {
            $this->addFlash(
                'success',
                'Review published successfully'
            );
        } else {
            $this->addFlash(
                'success',
                'Review unpublished successfully'
            );
        }

        return $this->redirectToRoute('user_dashboard_reviews');
    }

    /**
     * Allow a user to deleted a owned review if authenticated.
     *
     * @Route("/user/dashboard/reviews/delete-{uid}-{cid}", name="user_dashboard_reviews_delete", methods={"POST", "GET"})
     *
     * @param int $uid The user ID
     * @param int $cid The course ID
     *
     * @return RedirectResponse
     */
    public function dashboardReviewsDelete(int $uid, int $cid)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $username = $this->get('session')->get(Security::LAST_USERNAME);
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if($user == null || $user->getId() != $uid) {
            return $this->redirectToRoute('logout');
        }

        $review = $this->getDoctrine()->getRepository(CourseReview::class)
            ->findOneBy(['user' => $uid, 'course' => $cid]);

        if($review == null) {
            $this->addFlash(
                'error',
                'Review not found'
            );

            return $this->redirectToRoute('user_dashboard_reviews');
        }

        if(!$review->getIsDeleted()) {
            $review->setIsPublished(false);
            $review->setIsDeleted(true);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $review->setUpdatedAt($now);
            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Review deleted successfully'
            );
        } else {
            $this->addFlash(
                'error',
                'Review has already been deleted'
            );
        }

        return $this->redirectToRoute('user_dashboard_reviews');
    }

    /**
     * Check if the image uploaded is valid following size constraint.
     * Formats supported are JPEG and PNG
     *
     * @param int $maxSize The maximum size allowed in bytes
     *
     * @return bool Returns true if image is valid, false otherwise
     */
    private function isUploadedImageValid(int $maxSize) : bool {
        if($_FILES['image']['error'] != 0) {
            $this->addFlash(
                'error',
                'An error occured while uploading the image (code ' . $_FILES['image']['error'] . ')'
            );

            return false;
        }

        // fixme use variable instead of hard coded value in message
        if($_FILES['image']['size'] > $maxSize) {
            $this->addFlash(
                'error',
                'Image is too big (> 100Ko)'
            );

            return false;
        }

        if($_FILES['image']['size'] == 0) {
            $this->addFlash(
                'error',
                'Image has a size of 0'
            );

            return false;
        }

        if(empty($_FILES['image']['tmp_name'])) {
            $this->addFlash(
                'error',
                'Temporary location is not set (server error)'
            );

            return false;
        }

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $fileExtension = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'],'.'));
        if(!in_array($finfo->file($_FILES['image']['tmp_name']), [
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
            ]) ||
            !in_array($fileExtension, ['.png', '.jpeg', '.jpg'])) {

            $this->addFlash(
                'error',
                'Only image types supported are PNG or JPG/JPEG'
            );

            return false;
        }

        return true;
    }
}

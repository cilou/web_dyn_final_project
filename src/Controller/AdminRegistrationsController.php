<?php
/**
 * Handles Users Courses Enrollments at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/registrations
 */

namespace App\Controller;

use App\Entity\CourseRegistration;
use App\Utils\OpenocxAdminLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Listing of all courses enrollments by authenticated ROLE_ADMIN.
 *
 * List all information about each course enrollment.
 * Access is logged.
 */
class AdminRegistrationsController extends AbstractController
{
    /**
     * Listing of courses enrollments.
     *
     * @Route("/admin/dashboard/registrations", name="admin_dashboard_registrations", methods={"POST", "GET"})
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardRegistrations(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminRegistrationsController dashboardRegistrations ' .
            '/admin/dashboard/registrations',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $registrations = $this->getDoctrine()
            ->getRepository(CourseRegistration::class)
            ->findAll();
        return $this->render('user/dashboard.html.twig', [
            'registrations' => $registrations
        ]);
    }
}
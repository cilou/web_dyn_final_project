<?php
/**
 * Courses non-admin actions controller
 *
 * @author C Leyman
 * @updated_at 2018-12-24
 * @base_url /course
 */

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Course;
use App\Entity\CourseComment;
use App\Entity\CourseRegistration;
use App\Entity\CourseReview;
use App\Entity\User;
use App\Form\CourseRegistrationType;
use App\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * All actions related to the course view and usage by users through /course'.
 *
 * Course search, filtering, enrollments, listing, ...
 */
class CourseController extends AbstractController
{
    //fixme should be sent by the view not imposed by the controller
    //fixme plus phpDocumentor does not support T_CONST apparently
    // https://github.com/phpDocumentor/Reflection/issues/124
    private const COURSE_PER_PAGE = 6;

    /**
     * Catalog first page
     *
     * @Route("/course", name="course")
     *
     * @return Response
     */
    public function index()
    {
        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $courses = $courseRepository->findAllPublishedOrderedByNewest(0, self::COURSE_PER_PAGE);
        $courseCount = $courseRepository->getAllPublishedTotalCount();
        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => 1,
            'categories' => $categories
        ]);
    }

    /**
     * Catalog pagination
     *
     * @Route("/course/page-{numPage}", name="course_page")
     *
     * @param int $numPage The catalog page number we want to get
     *
     * @return Response
     */
    public function coursePage(int $numPage)
    {
        if($numPage < 1) {
            return $this->render('course/index.html.twig', [
                'courseCount' => 0
            ]);
        }

        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $offset = ($numPage-1) * self::COURSE_PER_PAGE;
        $courses = $courseRepository->findAllPublishedOrderedByNewest($offset, self::COURSE_PER_PAGE);
        $courseCount = $courseRepository->getAllPublishedTotalCount();
        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => $numPage,
            'categories' => $categories
        ]);
    }

    /**
     * View a specific course
     *
     * @Route("/course/view-{id}", name="course_view")
     *
     * @param Request $request The request received by the server
     * @param int $id The id of the course
     * @param bool $showReviews True by default, display the reviews tab on the course page.
     * If set in the request, set to false here and display the comments tab on the course page.
     *
     * @return Response
     */
    public function courseView(Request $request, int $id, bool $showReviews = true)
    {
        if(isset($_GET['showReviews'])) {
            $showReviews = false;
        }

        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $course = $courseRepository->find($id);
        if($course == null) {
            $this->addFlash(
                'error',
                'Course not found'
            );
            return $this->redirectToRoute('course');
        }

        // so we can check if user is already enrolled and disable enroll button
        // fixme would it be better to retrieve all courses the user is enrolled in instead ?
        $courseRegistrations = $course->getCourseRegistrations();
        $enrolledUserIds = [];
        foreach($courseRegistrations as $courseRegistration) {
            $enrolledUserIds[] = $courseRegistration->getUser()->getId();
        }

        $reviews = $course->getCourseReviews();
        $comments = $course->getCourseComments();

        return $this->render('course/view.html.twig', [
            'course' => $course,
            'reviews' => $reviews,
            'comments' => $comments,
            'showReviews' => $showReviews,
            'enrolledUserIds' => $enrolledUserIds
        ]);
    }

    /**
     * Filter courses by category
     *
     * @Route("/course/category-{categoryId}", name="course_category")
     *
     * @param int $categoryId The id of the category
     *
     * @return Response
     */
    public function courseCategory(int $categoryId)
    {
        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $courses = $courseRepository->findAllPublishedByCategoryOrderedByNewest($categoryId, 0, self::COURSE_PER_PAGE);
        $courseCount = $courseRepository->getAllPublishedByCategoryTotalCount($categoryId);
        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => 1,
            'categoryId' => $categoryId,
            'categories' => $categories
        ]);
    }

    /**
     * Filter course by category pagination
     *
     * @Route("/course/category-{categoryId}/page-{numPage}", name="course_category_page")
     *
     * @param int $categoryId The id of the category
     * @param int $numPage The page number
     *
     * @return Response
     */
    public function courseCategoryPage(int $categoryId, int $numPage)
    {
        if($numPage < 1) {
            return $this->render('course/index.html.twig', [
                'courseCount' => 0
            ]);
        }

        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $offset = ($numPage-1) * self::COURSE_PER_PAGE;
        $courses = $courseRepository->findAllPublishedByCategoryOrderedByNewest($categoryId, $offset, self::COURSE_PER_PAGE);
        $courseCount = $courseRepository->getAllPublishedByCategoryTotalCount($categoryId);
        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => $numPage,
            'categoryId' => $categoryId,
            'categories' => $categories
        ]);
    }

    /**
     * Search course in catalog based on exact sentence or one or several keywords
     *
     * @Route("/course/search", name="course_search", methods={"POST"})
     *
     * @param Request $request The request received by the server
     *
     * @return Response
     */
    public function courseSearch(Request $request)
    {
        $keyword = $request->get('keyword');
        //dump($keyword);
        //dump($request->get('anyWord'));

        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $offset = 0 * self::COURSE_PER_PAGE;
        $courses = null;
        $anyWord = false;

        // if 'anyWord' has been set, we split the keyword string received and add SQL '%' to each one
        if($request->get('anyWord') == "on") {
            $anyWord = true;
            $keywordArray = explode(' ',$keyword);
            $keywordArrayToSend = [];
            foreach ($keywordArray as $kw) {
                $kw = '%' . $kw . '%';
                $keywordArrayToSend[] = $kw;
            }
            $courses = $courseRepository
                ->findAllPublishedByMultipleKeywordsOrderedByNewest($keywordArrayToSend,
                    $offset, self::COURSE_PER_PAGE);
            $courseCount = $courseRepository->getAllPublishedByMultipleKeywordsTotalCount($keywordArrayToSend);
        } else {
            $courses = $courseRepository->findAllPublishedByKeywordsOrderedByNewest('%'.$keyword.'%', $offset, self::COURSE_PER_PAGE);
            $courseCount = $courseRepository->getAllPublishedByKeywordsTotalCount('%'.$keyword.'%');
        }

        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => 1,
            'keyword' => $keyword,
            'categories' => $categories,
            'anyWord' => $anyWord
        ]);
    }

    /**
     * Search course in catalog based on exact sentence or one or several keywords pagination
     *
     * @Route("/course/search/page-{numPage}", name="course_search_page", methods={"GET"})
     *
     * @param Request $request The request received by the server
     * @param int $numPage The page number
     *
     * @return Response
     */
    public function courseSearchPage(Request $request, int $numPage)
    {
        $keyword = trim($request->get('keyword'), '"');
        $courseRepository = $this->getDoctrine()->getRepository(Course::class);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $offset = ($numPage - 1) * self::COURSE_PER_PAGE;
        $courses = null;
        $anyWord = false;

        if($request->get('anyWord') == true) {
            $anyWord = true;
            $keywordArray = explode(' ',$keyword);
            $keywordArrayToSend = [];
            foreach ($keywordArray as $kw) {
                $kw = '%' . $kw . '%';
                $keywordArrayToSend[] = $kw;
            }
            $courses = $courseRepository
                ->findAllPublishedByMultipleKeywordsOrderedByNewest($keywordArrayToSend,
                    $offset, self::COURSE_PER_PAGE);
            $courseCount = $courseRepository->getAllPublishedByMultipleKeywordsTotalCount($keywordArrayToSend);
        } else {
            $courses = $courseRepository->findAllPublishedByKeywordsOrderedByNewest('%'.$keyword.'%', $offset, self::COURSE_PER_PAGE);
            $courseCount = $courseRepository->getAllPublishedByKeywordsTotalCount('%'.$keyword.'%');
        }

        $categories = $categoryRepository->findAll();

        return $this->render('course/index.html.twig', [
            'courses' => $courses,
            'courseCount' => $courseCount,
            'coursePerPage' => self::COURSE_PER_PAGE,
            'numPageSelected' => $numPage,
            'keyword' => $keyword,
            'categories' => $categories,
            'anyWord' => $anyWord
        ]);
    }

    /**
     * Allow a user with at least the ROLE_MEMBER but not with ROLE_ADMIN to enroll to a course.
     *
     * @Route("/course/enroll/{id}", name="course_enroll")
     *
     * @param Request $request The request received by the server
     * @param int $id The course ID
     * @param CsrfTokenManagerInterface $csrfTokenManager To protect the submission from CSRF attacks (form)
     *
     * @return RedirectResponse|Response
     */
    public function courseEnrollment(Request $request, int $id, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if($course == null) {
            $this->addFlash(
                'error',
                'Course not found'
            );
            return $this->redirectToRoute('course');
        }

        if ($request->isMethod('POST')) {

            $token = new CsrfToken('authenticate', $request->get('_csrf_token'));
            if (!$csrfTokenManager->isTokenValid($token)) {
                throw new InvalidCsrfTokenException();
            }

            $username = $this->get('session')->get(Security::LAST_USERNAME);
            $user = $this->getDoctrine()->getRepository(User::class)
                ->findOneBy(['username' => $username]);
            if($user == null) {
                $this->addFlash(
                    'error',
                    'User not found'
                );
                return $this->redirectToRoute('login');
            }

            if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $this->addFlash(
                    'error',
                    'Administrators cannot register to courses, please use your regular account'
                );

                return $this->render('user/dashboard.html.twig', [
                    'courses' => [],
                ]);
            }

            // We verify that the user is not already enrolled in the course selected
            $check = $this->getDoctrine()->getRepository(CourseRegistration::class)
                ->findBy(['user' => $user, 'course' => $course]);
            if($check == null) {
                $registration = new CourseRegistration();
                $registration->setCourse($course);
                $registration->setUser($user);
                $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
                $registration->setCreatedAt($now);
                $registration->setUpdatedAt($now);
                $registration->setPaidAmount($request->get('amountToPay'));
                $registration->setTaxRate($request->get('courseTaxRate'));
                $registration->setIsCancelled(false);

                $this->getDoctrine()->getManager()->persist($registration);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Registration successful'
                );
            } else {
                $this->addFlash(
                    'error',
                    'You are already registered for that course'
                );
            }

            $courses = $user->getCourseRegistrations();

            return $this->render('user/dashboard.html.twig', [
                'courses' => $courses,
            ]);
        }

        return $this->render('course/enroll.html.twig', [
            'course' => $course
        ]);
    }

    /**
     * Handle the creation of comments on a course page from ROLE_MEMBER users
     * but not from ROLE_ADMIN ones
     *
     * @Route("/course/{id}/comment/new", name="course_new_comment", methods={"POST"})
     *
     * @param Request $request The resuest received by the server
     * @param int $id The course ID
     *
     * @return RedirectResponse|Response
     */
    public function courseNewComment(Request $request, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        if ($request->isMethod('POST')) {
            $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
            if($course == null) {
                $this->addFlash(
                    'error',
                    'Course not found'
                );
                return $this->redirectToRoute('course');
            }

            $user = $this->getDoctrine()->getRepository(User::class)
                ->findOneBy(['username' =>$request->get('username')]);
            if($user == null) {
                $this->addFlash(
                    'error',
                    'User not found'
                );
                return $this->redirectToRoute('login');
            }

            if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $this->addFlash(
                    'error',
                    'Administrators cannot post comments, please use your regular account'
                );
            } else {
                $courseComment = new CourseComment();
                $courseComment->setUser($user);
                $courseComment->setCourse($course);
                // Escape => Twig does it
                //$courseComment->setMessage($request->get('message'));
                try {
                    $courseComment->setMessage(Utils::sanitizeQuillEditorInput($request->get('message')));
                } catch(Exception $ex) {
                    $this->addFlash('error', $ex->getMessage());
                    return $this->courseView($request, $id, false);
                }

                $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
                $courseComment->setCreatedAt($now);
                $courseComment->setUpdatedAt($now);
                $courseComment->setIsPublished(true);
                $courseComment->setIsDeleted(false);

                $this->getDoctrine()->getManager()->persist($courseComment);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->courseView($request, $id, false);
    }

    /**
     * Handle the creation of reviews on a course page from ROLE_MEMBER users
     * but not from ROLE_ADMIN ones
     *
     * @Route("/course/{id}/review/new", name="course_new_review", methods={"POST"})
     *
     * @param Request $request The resuest received by the server
     * @param int $id The course ID
     *
     * @return RedirectResponse|Response
     */
    public function courseNewReview(Request $request, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        if ($request->isMethod('POST')) {

            $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
            if($course == null) {
                $this->addFlash(
                    'error',
                    'Course not found'
                );
                return $this->redirectToRoute('course');
            }

            $user = $this->getDoctrine()->getRepository(User::class)
                ->findOneBy(['username' =>$request->get('username')]);
            if($user == null) {
                $this->addFlash(
                    'error',
                    'User not found'
                );
                return $this->redirectToRoute('login');
            }

            if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $this->addFlash(
                    'error',
                    'Administrators cannot post reviews, please use your regular account'
                );

                return $this->courseView($request, $id);
            }

            $courseReview = $this->getDoctrine()->getRepository(CourseReview::class)
                ->findOneBy(['user' => $user, 'course' => $course]);
            if($courseReview == null) {
                $courseReview = new CourseReview();
                $courseReview->setUser($user);
                $courseReview->setCourse($course);
                $courseReview->setRating($request->get('rating'));
                // Escape => Twig does it
                //$courseReview->setreview($request->get('review'));
                try {
                    $courseReview->setReview(Utils::sanitizeQuillEditorInput($request->get('review')));
                } catch(Exception $ex) {
                    $this->addFlash('error', $ex->getMessage());
                    return $this->courseView($request, $id);
                }
                $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
                $courseReview->setCreatedAt($now);
                $courseReview->setUpdatedAt($now);
                $courseReview->setIsPublished(true);
                $courseReview->setIsDeleted(false);

                $this->getDoctrine()->getManager()->persist($courseReview);
                $this->getDoctrine()->getManager()->flush();
            } else {
                $this->addFlash(
                    'error',
                    'You already have posted a review for this course'
                );
            }
        }

        return $this->courseView($request, $id);
    }
}

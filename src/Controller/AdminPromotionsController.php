<?php
/**
 * Handles Courses Promotions Management at Admin Level.
 *
 * @author C Leyman
 * @updated_at 2018-12-25
 * @base_url /admin/dashboard/promotions
 */

namespace App\Controller;

use App\Entity\CoursePromotion;
use App\Form\CoursePromotionType;
use App\Utils\OpenocxAdminLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * All actions related to the management of courses promotions by authenticated ROLE_ADMIN.
 *
 * CRUD. Access is logged.
 */
class AdminPromotionsController extends AbstractController
{
    /**
     * Listing of all courses promotions by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/promotions", name="admin_dashboard_promotions", methods={"POST", "GET"})
     *
     * @param OpenocxAdminLogger $adminLogger
     * @return Response
     */
    public function dashboardPromotions(OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminPromotionsController dashboardPromotions ' .
                '/admin/dashboard/promotions',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $promotions = $this->getDoctrine()->getRepository(CoursePromotion::class)->findAll();
        return $this->render('user/dashboard.html.twig', [
            'promotions' => $promotions
        ]);
    }

    /**
     * Editing of a specific course promotion by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/promotions/edit-{id}", name="admin_dashboard_promotions_edit")
     *
     * @param Request $request The request received by the server
     * @param int $id The promotion ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardPromotionsEdit(Request $request, int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminPromotionsController dashboardPromotionsEdit ' .
            '/admin/dashboard/promotions/edit-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $promotion = $this->getDoctrine()->getRepository(CoursePromotion::class)->find($id);
        $form = $this->createForm(CoursePromotionType::class, $promotion);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Update promotion'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $check = $this->getDoctrine()->getRepository(CoursePromotion::class)
                ->findOneBy(['code' => $promotion->getCode()]);
            if ($check != null && $check->getId() != $id) {
                $this->addFlash(
                    'error',
                    'Promotion code already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($promotion);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Promotion updated successfully'
                );

                return $this->redirectToRoute('admin_dashboard_promotions');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'promotion_edit' => true
        ]);
    }

    /**
     * Deletion of a specific course promotion by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/promotions/delete-{id}", name="admin_dashboard_promotions_delete")
     *
     * @param int $id The promotion ID
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse
     */
    public function dashboardPromotionsDelete(int $id, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminPromotionsController dashboardPromotionsDelete ' .
            '/admin/dashboard/promotions/delete-' . $id,
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $promotion = $this->getDoctrine()->getRepository(CoursePromotion::class)->find($id);
        if ($promotion == null) {
            $this->addFlash(
                'error',
                'Promotion not found'
            );
        } else if (count($promotion->getCourses()) > 0) {
            $this->addFlash(
                'error',
                'Promotion is in use and cannot be deleted'
            );
        } else {
            $this->getDoctrine()->getManager()->remove($promotion);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Promotion has been successfully deleted'
            );
        }

        return $this->redirectToRoute('admin_dashboard_promotions');
    }

    /**
     * Creation of a new course promotion by authenticated ROLE_ADMIN.
     *
     * @Route("/admin/dashboard/promotions/new", name="admin_dashboard_promotions_new")
     *
     * @param Request $request The request received by the server
     * @param OpenocxAdminLogger $adminLogger
     * @return RedirectResponse|Response
     */
    public function dashboardPromotionsNew(Request $request, OpenocxAdminLogger $adminLogger)
    {
        $username = $this->getUser()->getUsername();
        $adminLogger->log(
            'Username ' . $username . ' accessing AdminPromotionsController dashboardPromotionsNew ' .
            '/admin/dashboard/promotions/new',
            LogLevel::INFO
        );
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $promotion = new CoursePromotion();
        $form = $this->createForm(CoursePromotionType::class, $promotion);
        $form->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
            'label' => 'Add promotion'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $check = $this->getDoctrine()->getRepository(CoursePromotion::class)
                ->findOneBy(['code' => $promotion->getCode()]);
            if ($check != null) {
                $this->addFlash(
                    'error',
                    'Promotion code already exists'
                );
            } else {
                $this->getDoctrine()->getManager()->persist($promotion);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Promotion created successfully'
                );

                return $this->redirectToRoute('admin_dashboard_promotions');
            }
        }

        return $this->render('user/dashboard.html.twig', [
            'form' => $form->createView(),
            'promotion_edit' => true
        ]);
    }
}
<?php
/**
 * Handle users authentication. Created by Symfony.
 *
 * @adapted_by C Leyman
 * @updated_at 2018-12-24
 */
namespace App\Security;

use App\Entity\User;
use App\Utils\OpenocxAdminLogger;
use App\Utils\OpenocxAppLogger;
use App\Utils\OpenocxUsersLogger;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Handle users authentication. Works with guards (security.yaml)
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $entityManager;
    private $router;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $usersLogger;
    private $appLogger;
    private $adminLogger;
    private $username;

    /**
     * LoginFormAuthenticator constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface $router
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param OpenocxUsersLogger $usersLogger
     * @param OpenocxAppLogger $appLogger
     * @param OpenocxAdminLogger $adminLogger
     */
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder, OpenocxUsersLogger $usersLogger, OpenocxAppLogger $appLogger, OpenocxAdminLogger $adminLogger)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->usersLogger = $usersLogger;
        $this->adminLogger = $adminLogger;
        $this->appLogger = $appLogger;
    }

    /**
     * Checks that the request comes from the login page
     *
     * @param Request $request The request received by the server
     *
     * @return bool Return true if teh request is valid, false otherwise
     */
    public function supports(Request $request)
    {
        return 'login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * Retrieve the data posted by the user
     *
     * @param Request $request The request received by the server
     *
     * @return array|mixed
     */
    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        $this->username = $credentials['username'];

        return $credentials;
    }

    /**
     * Get the user object from the database
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return null|object|UserInterface The user object
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            $this->usersLogger->log(
                'Login attempt failure from ' . $_SERVER['REMOTE_ADDR'] .
                    ' for username ' . $this->username . ': INVALID CSRF TOKEN',
                LogLevel::WARNING);
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['username' => $credentials['username']]);

        if (!$user || $user->getIsDeleted()) {
            // fail authentication with a custom error
            $this->usersLogger->log(
                'Login attempt failure from ' . $_SERVER['REMOTE_ADDR'] .
                    ' for username ' . $this->username . ': USER UNKNOWN OR DELETED',
                LogLevel::INFO);
            throw new CustomUserMessageAuthenticationException('Username could not be found.');
        } else if($user->getIsDisabled()) {
            $this->usersLogger->log(
                'Login attempt failure from ' . $_SERVER['REMOTE_ADDR'] .
                    ' for username ' . $this->username . ': USER ACCOUNT DISABLED',
                LogLevel::INFO);
            throw new CustomUserMessageAuthenticationException('User account disabled.');
        }

        return $user;
    }

    /**
     * Verify the credentials entered by he user
     *
     * @param mixed $credentials
     * @param UserInterface $user The user object
     *
     * @return bool Return true if the credentials are valid, false otherwise
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if(!$this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setLastFailedLogin($now);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->usersLogger->log(
                'Login attempt failure from ' . $_SERVER['REMOTE_ADDR'] .
                    ' for username ' . $this->username . ': INVALID PASSWORD',
                LogLevel::WARNING);
            if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $this->adminLogger->log(
                    'Login attempt failure from ' . $_SERVER['REMOTE_ADDR'] .
                    ' for username ' . $this->username . ': INVALID PASSWORD',
                    LogLevel::ALERT);
            }
            return false;
        }
        return true;
    }

    /**
     * Handle successful authentications
     *
     * @param Request $request The request received by the server
     * @param TokenInterface $token
     * @param string $providerKey
     *
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // set last login date
        $credentials = $this->getCredentials($request);
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['username' => $credentials['username']]);
        $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
        $user->setLastSuccessfulLogin($user->getLastLogin());
        $user->setLastLogin($now);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->usersLogger->log(
            'Login attempt success from ' . $_SERVER['REMOTE_ADDR'] .
            ' for username ' . $this->username . ': USER KNOWN AND VALID',
            LogLevel::INFO);

        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {

            return new RedirectResponse($targetPath);
        }

        // For example : return new RedirectResponse($this->router->generate('some_route'));
        //throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
        return new RedirectResponse($this->router->generate('user_dashboard'));
    }

    /**
     * Retrieve the login URL set up for the application
     *
     * @return string
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }
}
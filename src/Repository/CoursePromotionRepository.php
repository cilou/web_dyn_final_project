<?php

namespace App\Repository;

use App\Entity\CoursePromotion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CoursePromotion|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursePromotion|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursePromotion[]    findAll()
 * @method CoursePromotion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursePromotionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CoursePromotion::class);
    }

//    /**
//     * @return CoursePromotion[] Returns an array of CoursePromotion objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoursePromotion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

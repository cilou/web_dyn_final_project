<?php

namespace App\Repository;

use App\Entity\CourseReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CourseReview|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseReview|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseReview[]    findAll()
 * @method CourseReview[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseReviewRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CourseReview::class);
    }

    /**
     * Get all published reviews ordered by their rating in descending order.
     *
     * @param int $minRating The minimum rating a review must have
     * @param int $maxResult The maximum results that must be returned
     *
     * @return CourseReview[] Returns an array of CourseReview objects
     */
    public function findAllPublishedOrderedByRatingDesc(int $minRating, int $maxResult)
    {
        return $this->createQueryBuilder('cr')
            ->andWhere('cr.rating >= :minRating')
            ->setParameter('minRating', $minRating)
            ->andWhere('cr.isPublished = true')
            ->andWhere('cr.isDeleted = false')
            ->orderBy('cr.rating', 'DESC')
            ->setMaxResults($maxResult)
            ->getQuery()
            ->getResult()
            ;
    }
}

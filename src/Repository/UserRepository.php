<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $value
     * @return User|null
     */
    public function findOneByUsername($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findAllByRoleName($value)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('App\Entity\UserRole', 'ur')
            ->addSelect('ur')
            ->andWhere('ur.name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findAllNotWithRoleName($value)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('DISTINCT u.id')
            ->from('App\Entity\User', 'u')
            ->innerJoin('App\Entity\UserRole', 'ur')
            ->andWhere('ur.name = :val')
            ->andWhere('u.userRole = ur')
            ->orderBy('u.id', 'ASC')
            ;

        $qb2 = $this->_em->createQueryBuilder();
        $qb2->select('u2')
            //->addSelect('u2')
            ->from('App\Entity\User', 'u2')
            ->andWhere($qb2->expr()->notIn('u2.id', $qb->getDQL()))
        ;

        $qb2->setParameter('val', $value);
        $query = $qb2->getQuery();

        return $query->getResult();
    }

    /*
    public function findAllActiveEnabledByRoleName($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('userRole', 'ur')
            ->addSelect('ur')
            ->andWhere('ur.name = :val')
            ->andWhere('u.isDisabled = false')
            ->andWhere('u.isDeleted = false')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }
    */

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

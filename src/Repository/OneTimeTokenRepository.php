<?php

namespace App\Repository;

use App\Entity\OneTimeToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OneTimeToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method OneTimeToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method OneTimeToken[]    findAll()
 * @method OneTimeToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OneTimeTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OneTimeToken::class);
    }

    /**
     * Find all expired tokens (filed exp less than current time()
     *
     * @param int $currentTimestamp generated with time()
     *
     * @return OneTimeToken[] Returns an array of OneTimeToken objects
     */
    public function findAllWithExpLessThanCurrentTimestamp(int $currentTimestamp)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exp <= :val')
            ->setParameter('val', $currentTimestamp)
            ->orderBy('o.id', 'ASC')
            //->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return OneTimeToken[] Returns an array of OneTimeToken objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OneTimeToken
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

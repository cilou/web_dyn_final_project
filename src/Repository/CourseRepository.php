<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Course::class);
    }

    // pagination
    // https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/query-builder.html#limiting-the-result

    /**
     * @return mixed Returns total count of published Courses
     */
    public function getAllPublishedTotalCount()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isPublished = true')
            ->select('COUNT(c.id) AS countId')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @return Course[] Returns an array of Course objects
     */
    public function findAllPublishedOrderedByNewest(int $offset, int $limit)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isPublished = true')
            ->orderBy('c.updatedAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return mixed Returns total count of published Courses
     */
    public function getAllPublishedByCategoryTotalCount(int $categoryId)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'cat')
            ->addSelect('cat')
            ->andWhere('cat.id = :val')
            ->andWhere('c.isPublished = true')
            ->setParameter('val', $categoryId)
            ->orderBy('c.updatedAt', 'DESC')
            ->select('COUNT(c.id) AS countId')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @return Course[] Returns an array of Course objects
     */
    public function findAllPublishedByCategoryOrderedByNewest(int $categoryId, int $offset, int $limit)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'cat')
            ->addSelect('cat')
            ->andWhere('cat.id = :val')
            ->andWhere('c.isPublished = true')
            ->setParameter('val', $categoryId)
            ->orderBy('c.updatedAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return mixed Returns total count of published Courses
     */
    public function getAllPublishedByKeywordsTotalCount(string $keywords)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'cat')
            ->addSelect('cat')
            ->andWhere('(c.title LIKE :val OR c.smallDescription LIKE :val OR c.fullDescription LIKE :val OR cat.name LIKE :val)')
            ->andWhere('c.isPublished = true')
            ->setParameter('val', $keywords)
            ->orderBy('c.updatedAt', 'DESC')
            ->select('COUNT(DISTINCT c.id) AS countId')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @return Course[] Returns an array of Course objects
     */
    public function findAllPublishedByKeywordsOrderedByNewest($keywords, int $offset, int $limit)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'cat')
            ->addSelect('cat')
            ->andWhere('(c.title LIKE :val OR c.smallDescription LIKE :val OR c.fullDescription LIKE :val OR cat.name LIKE :val)')
            ->andWhere('c.isPublished = true')
            ->setParameter('val', $keywords)
            ->orderBy('c.updatedAt', 'DESC')
            ->groupBy('c.id')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Course[] Returns an array of Course objects
     */
    public function findAllPublishedByMultipleKeywordsOrderedByNewest(array $keywords, int $offset, int $limit)
    {
        $courses = [];

        foreach($keywords as $keyword) {

           $currentCourses = $this->createQueryBuilder('c')
                ->leftJoin('c.category', 'cat')
                ->addSelect('cat')
                ->andWhere('(c.title LIKE :val OR c.smallDescription LIKE :val OR c.fullDescription LIKE :val OR cat.name LIKE :val)')
                ->andWhere('c.isPublished = true')
                ->setParameter('val', $keyword)
                ->orderBy('c.updatedAt', 'DESC')
                ->groupBy('c.id')
                //->setFirstResult($offset)
                //->setMaxResults($limit)
                ->getQuery()
                ->getResult()
            ;

           foreach($currentCourses as $currentCourse) {
               //dump($currentCourse);
               if(!in_array($currentCourse, $courses)) {
                   //dump('not in array');
                   $courses[] = $currentCourse;
               }
           }

           $coursesToSend = [];
           for($i = $offset; $i < count($courses); $i++) {
               $coursesToSend[] = $courses[$i];
               if(count($coursesToSend) == $limit) {
                   break;
               }
           }
        }

        return $coursesToSend;
    }

    /**
     * @return mixed Returns total count of published Courses
     */
    public function getAllPublishedByMultipleKeywordsTotalCount(array $keywords)
    {
        $courses = [];

        foreach($keywords as $keyword) {

            $currentCourses = $this->createQueryBuilder('c')
                ->leftJoin('c.category', 'cat')
                ->addSelect('cat')
                ->andWhere('(c.title LIKE :val OR c.smallDescription LIKE :val OR c.fullDescription LIKE :val OR cat.name LIKE :val)')
                ->andWhere('c.isPublished = true')
                ->setParameter('val', $keyword)
                ->orderBy('c.updatedAt', 'DESC')
                ->groupBy('c.id')
                //->setFirstResult($offset)
                //->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            foreach ($currentCourses as $currentCourse) {
                //dump($currentCourse);
                if (!in_array($currentCourse, $courses)) {
                    //dump('not in array');
                    $courses[] = $currentCourse;
                }
            }
        }

        return count($courses);
    }

//    /**
//     * @return Course[] Returns an array of Course objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Course
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Form;

use App\Entity\ResetPasswordRequestUser;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetPasswordRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,
                [
                    'attr' => [
                        'placeholder' => 'Your email',
                        'autofocus' => true,
                        'maxlength' => 200,
                    ]
                ])
            ->add('captchaCode', CaptchaType::class,
                [
                    'captchaConfig' => 'ResetPasswordRequestCaptcha',
                    'attr' => [
                        'class' => 'mt-3 mb-3',
                        'maxlength' => 6,
                    ],
                    'label' => 'Retype the characters from the picture'
                ]
            )
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary btn-block mt-3 mb-3'
                ],
                'label' => 'Send me instructions'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResetPasswordRequestUser::class,
        ]);
    }
}

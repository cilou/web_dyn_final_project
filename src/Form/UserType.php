<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserRole;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('firstname')
            //->add('lastname')
            ->add('username', TextType::class, ['attr' => ['readonly' => true]])
            ->add('email', TextType::class, ['attr' => ['readonly' => true]])
            //->add('image')
            //->add('password')
            //->add('createdAt')
            //->add('updatedAt')
            //->add('lastLogin')
            ->add('isDisabled')
            ->add('isDeleted')
            ->add('userRole', EntityType::class, [
                'class' => UserRole::class,
                'choice_label' => function($role) { return $role->getName();}
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
                'label' => 'Update user'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

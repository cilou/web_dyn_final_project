<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserRegistration;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, ['attr' => ['placeholder' => 'Firstname', 'autofocus' => true]])
            ->add('lastname', TextType::class, ['attr' => ['placeholder' => 'Lastname']])
            ->add('username', TextType::class, ['attr' => ['placeholder' => 'Username']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Email']])
            ->add('image', FileType::class, [
                'empty_data' => 'userDefault.png',
                'required' => false
            ])
            ->add('password', PasswordType::class, ['attr' => ['placeholder' => 'Password']])
            ->add('confirmPassword', PasswordType::class, ['attr' => ['placeholder' => 'Confirm your password']])
            ->add('captchaCode', CaptchaType::class,
                [
                    'captchaConfig' => 'RegistrationCaptcha',
                    'attr' => [
                        'class' => 'mt-3 mb-3',
                        'maxlength' => 6,
                    ],
                    'label' => 'Retype the characters from the picture'
                ]
            )
            ->add('tos', CheckboxType::class, [
                'attr' => ['required' => true],
                'label' => 'I have read the Privacy Policy and I Agree'
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
                'label' => 'Create account'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserRegistration::class,
        ]);
    }
}

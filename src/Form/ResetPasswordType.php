<?php

namespace App\Form;

use App\Entity\ResetPasswordUser;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, ['attr' => ['placeholder' => 'Password', 'autofocus' => true]])
            ->add('confirmPassword', PasswordType::class, ['attr' => ['placeholder' => 'Confirm your password']])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
                'label' => 'Reset my password'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\CoursePromotion;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoursePromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('startDateValidity')
            ->add('endDateValidity')
            ->add('percentOnPrice')
            ->add('courses', EntityType::class, [
                'class' => Course::class,
                'choice_label' => function($course) {
                    return $course->getId() . ' - ' .$course->getTitle();
                },
                'expanded' => true,
                'multiple' => true,
                //'required' => false
                'disabled' => true
            ])
            /*
            ->add('courses', EntityType::class, [
                'class' => Course::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->innerJoin('CoursePromotion', 'cp')
                        ->addSelect('cp');
                },
                'choice_label' => 'title',
                'expanded' => true,
                'multiple' => true,
                'disabled' => true
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CoursePromotion::class,
        ]);
    }
}

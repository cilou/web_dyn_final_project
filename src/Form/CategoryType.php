<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['maxlength' => 50]])
            ->add('description', TextType::class, ['attr' => [
                'required' => false,
                'empty_data' => 'n/a',
                'maxlength' => 250]
            ])
            //->add('courses')
            /*
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
                'label' => 'Update category'
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}

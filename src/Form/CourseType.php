<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Course;
use App\Entity\CoursePromotion;
use App\Entity\Language;
use App\Entity\TaxRate;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    private $entityManager;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('image', FileType::class, [
                'empty_data' => 'courseDefault.jpeg',
                'required' => false,
            ])
            ->add('smallDescription')
            ->add('fullDescription')
            ->add('duration')
            ->add('price')
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choice_label' => function($language) { return $language->getIsoName(); }
            ])
            ->add('taxRate', EntityType::class, [
                'class' => TaxRate::class,
                'choice_label' => function($taxRate) { return $taxRate->getRate(); }
            ])
            /*
            ->add('coursePromotion', EntityType::class, [
                'class' => CoursePromotion::class,
                'choice_label' =>
                    function($coursePromotion) {
                        if($coursePromotion->getStartDateValidity() >= new \DateTime('now', new \DateTimeZone('Europe/Brussels'))) {
                            return $coursePromotion->getCode();
                        }

                    },
                'required' => false,
                'group_by' => function($coursePromotion) {
                    return $coursePromotion == null ? null : $coursePromotion->getCode();
                }
                //'empty_data' => null
            ])
            */
            ->add('coursePromotion', EntityType::class, [
                'class' => CoursePromotion::class,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('cp')
                        ->andWhere('cp.startDateValidity >= :val')
                        ->setParameter('val', new \DateTime('now', new \DateTimeZone('Europe/Brussels')))
                        ->orderBy('cp.startDateValidity', 'ASC');
                },
                'choice_label' => 'code',
                'required' => false
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function($category) { return $category->getName(); },
                'expanded' => true,
                'multiple' => true
            ])
            //->add('tag')
            //->add('isPublished')
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-block mt-3 mb-3'],
                'label' => 'Update course'
            ])
            ->get('image')
            ->addModelTransformer(new CallbackTransformer(
                function($imageAsString) {
                    $file = null;
                    try {
                        $file = new File($this->params->get('course_main_img_dir'). '/' . $imageAsString);
                    } catch(FileException $ex) {}

                    return $file;
                },
                function($imageAsFile) {
                    return $imageAsFile;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}

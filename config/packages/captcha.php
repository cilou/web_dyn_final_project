<?php

if (!class_exists('CaptchaConfiguration')) { return; }

// BotDetect PHP Captcha configuration options

return [
    // Captcha configuration for example page
    'ExampleCaptcha' => [
        'UserInputID' => 'captchaCode',
        'ImageWidth' => 250,
        'ImageHeight' => 50,
    ],
    // Captcha configuration for reset-password-request page
    'ResetPasswordRequestCaptcha' => [
        'UserInputID' => 'captchaCode',
        'CodeLength' => CaptchaRandomization::GetRandomCodeLength(4, 6),
        'ImageStyle' => [
            ImageStyle::Radar,
            ImageStyle::Collage,
            ImageStyle::Fingerprints,
            ImageStyle::Graffiti,
            ImageStyle::Bullets,
        ],
        'SoundEnabled' => false
    ],
    // Captcha configuration for register page
    'RegistrationCaptcha' => [
        'UserInputID' => 'captchaCode',
        'CodeLength' => CaptchaRandomization::GetRandomCodeLength(4, 6),
        'ImageStyle' => [
            ImageStyle::Radar,
            ImageStyle::Collage,
            ImageStyle::Fingerprints,
            ImageStyle::Graffiti,
            ImageStyle::Bullets,
        ],
        'SoundEnabled' => false
    ],
    //'' => [],

];
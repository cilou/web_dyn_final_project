$('document').ready(function() {
    //=====================================
    // Quill Editor for Comments and Review
    //=====================================
    /* Reviews */
    let quillReview = new Quill('#review', {
        modules: {
            toolbar: [
                //[{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline']
            ]
        },
        placeholder: 'Your review. If you already have posted a review for this course, it will be discarded.',
        theme: 'snow'  // or 'bubble'
    });

    let formReview = $('#form-review');
    formReview.submit(function(e) {
        // Populate hidden form on submit
        let about = formReview.find('input[name=review]');
        about.val(JSON.stringify(quillReview.getContents()));
        if(about.val() === "{\"ops\":[{\"insert\":\"\\n\"}]}" || about.value === "" ) {
            //If empty, do not submit
            e.preventDefault();
            formReview.find('#review').before($('<span>', {
                'text': 'Review message cannot be empty!',
                'class': 'invalid-empty'
            }));
            console.log('empty!');
        }
    });

    /* Comments */
    let quillMessage = new Quill('#message', {
        modules: {
            toolbar: [
                //[{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline']
            ]
        },
        placeholder: 'Your message...',
        theme: 'snow'  // or 'bubble'
    });

    let formMessage = $('#form-message');
    formMessage.submit(function(e) {
        // Populate hidden form on submit
        let about = formMessage.find('input[name=message]');
        about.val(JSON.stringify(quillMessage.getContents()));
        if(about.val() === "{\"ops\":[{\"insert\":\"\\n\"}]}" || about.value === "" ) {
            //If empty, do not submit
            e.preventDefault();
            formMessage.find('#message').before($('<span>', {
                'text': 'Comment cannot be empty!',
                'class': 'invalid-empty'
            }));
            console.log('empty!');
        }
    });
    //=========================================
    // END Quill Editor for Comments and Review
    //=========================================
});


$('document').ready( function() {
    let inputLogDate = $('#log-date');
    let currentLogs = [];
    let table = $('#admin-log-table');
    let tableAdminLog = table.DataTable({
        // "ajax": {
        //     data: {date:inputLogDate.val()},
        //     type: 'GET',
        //     url: window.location.href + '/json'
        // },
        "columns" : [
            {"data": "datetime"},
            {"data": "type"},
            {"data": "log"}
        ]
    });

    inputLogDate.on('change', function() {
        $.ajax({
            dataType: 'json',
            url: window.location.href + '/json',
            method: 'GET',
            data: {'date': $(this).val()}
        })
        .done(function(data) {
            currentLogs = data;
            if(currentLogs !== null && currentLogs.length > 0) {
                tableAdminLog.clear().draw();
                tableAdminLog.rows.add(currentLogs).draw();
                tableAdminLog.columns.adjust().draw();
            } else {
                tableAdminLog.clear().draw();
            }
        })
        .fail(function(xhr, status) {
            console.log(xhr, status);
        })
        .always(function(){
            console.log(`Loading logs ${$(this).val()} done`);
        });


    });

    // We trigger the first load
    inputLogDate.trigger('change');

});
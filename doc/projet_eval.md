# PROJET EVALUATION
## Consignes
Le projet peut être réalisé sur base du travail fournit en Symfony, sur base du futur TFE ou encore à partir d'un tout nouveau projet selon votre convenance.

Il est obligatoire qu'il soit conçu sur une architecture MVC personnelle ou suivant le modèle vu en classe.

S'il s'agit d'une première version du TFE, elle doit être fonctionnelle et s'il s'agit de la suite du projet Symfony, il doit y avoir des apports nouveaux.

Le projet contiendra:
1. une partie publique (une page peut suffire)
2. une partie privée qui proposera à l'utilisateur connecté un service ou des fonctionnalités supplémentaires
3. une partie administration (CMS)

Fonctionnalités:
1. un envois de mails
2. une inscription en ligne
3. upload de fichiers
4. système de logs (identification des personnes qui se sont connectées)

Indicateurs d'évaluation:
1. le travail et la recherche personnelle
2. la conformité et l'usage du PHP7:
    annotations, le typage des fonctions...
3. Le soin apporté au template
4. L'organisation du code

Les critères d'exellences:
1. Implémentation de JavaScript et d'Ajax
2. La qualité et le schéma uml de la DB
3. La documentation du code (PHPDocumentor)

Bon travail à tous !
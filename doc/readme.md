# Infos diverses
## Users
* admin, password, ROLE_ADMIN
* jdoe, password, ROLE_MEMBER

## Reload DB
Voir DB/resetDB.md

## Tester l'envoi de mails avec mailtrap.io
Il faudra créer un compte (gratuit) et changer les settings dans .env (MAILER_URL)  
`MAILER_URL=smtp://smtp.mailtrap.io:2525?auth_mode=login&username=<suite de chiffres indiqué par mailtrap>&password=<password (suite de chiffres) indiqué par mailtrap>`

### Ce qui a été fait
- Ajout d'un slider JS pour les reviews sur la page principale

- Ajout Quill dans commentaires et reviews avec fonctionnalités basiques + contrôles (empty, ...)
- Added basic html tag support in creating and editing comments and reviews (Utils) (for users and admins)

- Correction BUG -- Promotion displayed in enroll course even if out of date (twig enroll.html.twig)

- phpDocumentor + Refactoring Controllers, Security, and Utils

- Installed JWT + SwiftMailer Bundles
    - Configured mailtrap.io
    - Added links to login page (reset pass + new account)
    - Created reset-password template in security subfolder
    - Added resetPass in SecurityController
    - Users can reset their password through a link generated with a token limited 
	to 15 minutes sent by email.
	- Password changes are notified to the user by email
	- Reset password tokens can only be used once. They are stored in the database
	and revoked a their first usage. Admins, disabled users, and users with unconfirmed email address 
	cannot generate reset password tokens.
	- Delete previous token if new one of the same type for the same user is generated

- Installed Captcha
    - Reset password requests require a captcha code (DDOS protection)
    - User registrations require a captcha code

- New account registration is confirmed by email to the user. A confirmation token for
email address validation is sent at the same time and has a validity of 1 week.
Users must confirm their email address before being able to use their email to reset their password for 
example. Users can request that the email with a new token be sent again.

- Added admin interface for managing tokens.

- Created cutomized logger +  log rotation
Added 3 channels + 3 services (App, Users, Admin)

- added admin interface for logs
  - Add AJAX + add date picker to choose the log file (no database)

- Added lastSuccessfulLogin and lastFailedLogin to user entity + display in profile

- Cleaning / Commenting functions / adding PHP Documentor (see doc/phpdoc)

### Ce qui doit encore être amélioré/fait
- Factoriser le code car encore beaucoup de redondance pour le moment
- Utiliser les rôles (permissions spéciales, ...)
- Améliorer le design ;-) et implémenter plus de JS afin de rendre la navigation sur le site plus fluide et plus sympa
- Ajouter des stats dans l'interface admin (graphiques, ...)
- ajouter un formulaire de contact
- Mettre en place un historique et des archives dans la DB afin de permettre la "suppression" d'élements.
- Une fois enrollé, un user devrait pouvoir avoir accès à un cours complet avec cours, vidéos, progress status, forum, ...
- ajouter Privacy Policy
- Customiser les pages d'erreur
- Meilleure gestion des cookies
- AJAX pour commentaires et reviews
- Newsletter
- Intégration des réseaux sociaux (partager, ...)
- Envoyer un email de confirmation lorsqu'un user est enrollé dans un cours
- Permettre au user de souscrire à des updates d'un cours
- refaire la fonctionnalité d'envoi d'email avec SwiftMail (mieux la penser)  
- ...


-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: openocx
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'PHP',NULL),(2,'Javascript',NULL),(3,'CSS',NULL),(4,'HTML',NULL),(5,'Database',NULL),(6,'Python',NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `course_promotion_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `price` double NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_169E6FB982F1BAF4` (`language_id`),
  KEY `IDX_169E6FB9FDD13F95` (`tax_rate_id`),
  KEY `IDX_169E6FB992578969` (`course_promotion_id`),
  CONSTRAINT `FK_169E6FB982F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`),
  CONSTRAINT `FK_169E6FB992578969` FOREIGN KEY (`course_promotion_id`) REFERENCES `course_promotion` (`id`),
  CONSTRAINT `FK_169E6FB9FDD13F95` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,1,2,10,'Aliquam odit ut quod eum rerum nulla.','1.jpeg','Suscipit quibusdam sed corrupti eum laboriosam ex rerum. Non voluptas beatae numquam iure maiores. Eum similique ut et qui.','Cumque dicta maiores magnam explicabo sit est eligendi dolores. Velit maxime asperiores harum aspernatur unde porro recusandae. Nulla inventore ab ut vitae unde eum voluptate. Maxime at vero pariatur non. Quam fugit qui alias ut quis inventore eum quia. Est consequatur sed vel ipsum repellendus molestias aut. Maiores placeat excepturi aut. Similique voluptatem et non quo magni. Perferendis laboriosam iste esse itaque pariatur cumque ut. Non sint ut doloremque dolor iste unde. Odio dolorum est voluptatem rerum nobis illo incidunt. Nihil odio ut voluptates minus molestiae animi et. Doloribus molestiae sapiente accusantium consectetur sit quo quos ut. Aspernatur dolorem impedit reprehenderit et qui optio. Repellat molestiae totam soluta voluptas ratione aut sed. Tenetur est nulla voluptatibus ea. Voluptatibus blanditiis sit ab non maxime magnam saepe. Explicabo saepe voluptatibus nihil non facere. Magnam numquam explicabo quibusdam illo est est deleniti perspiciatis. Qui necessitatibus deserunt ullam sint incidunt nemo veniam. Alias molestias quod aliquam nisi et sint quos magni. Laudantium beatae recusandae dolor possimus animi. Voluptatum repudiandae quia quae est. Est enim dolorem quo quisquam fugit ipsum et. Voluptas porro doloremque et sit aut est. Voluptatem qui animi voluptas labore voluptatem. Deleniti sit eum voluptatibus saepe quia. Voluptas minus ducimus placeat. Minus consequatur consequatur ipsum quam quo. Id quia enim quia velit. Minima qui nobis dolor fuga. Nesciunt optio sint assumenda. Aperiam cum ut voluptas quam sed sint ut soluta.',18,78,1,'2018-12-12 10:47:02','2018-12-12 14:46:06'),(2,1,2,2,'Non amet quia error est.','2.jpeg','Earum et odit commodi porro. Quas qui autem quam veniam. Enim molestiae officia magni perspiciatis aperiam. Qui exercitationem et voluptate omnis.','Veritatis architecto ipsa facere mollitia quis et non. Et non odio quae dolorum sit. Sed id quidem corporis eos et nihil. Placeat totam voluptas facilis optio qui quia asperiores. Culpa voluptas corporis aut culpa numquam. Error ipsa id aspernatur consectetur aut autem. Delectus ea deserunt enim eius magnam qui quo maiores. Eligendi quibusdam aut perferendis autem rem nihil. Consectetur itaque dicta dolor esse qui. Sequi quae et sit. Asperiores saepe quia sint sint provident tempora. Magnam dolor atque veritatis nostrum est nemo laudantium harum. Voluptates dolor enim deleniti voluptas explicabo doloribus quos quod. Nobis laboriosam enim et delectus. Consequatur omnis ea quia. Pariatur sint iure unde ut explicabo non. Id voluptas delectus qui unde. Est ipsa omnis blanditiis deserunt illum explicabo cupiditate. Quasi consectetur numquam quisquam voluptatum. Facilis blanditiis laboriosam veritatis consequuntur distinctio. Ex molestiae tenetur accusantium sed. Vel non et ea enim porro id. Rerum voluptatem commodi laborum et earum. Quia nihil deserunt voluptas quisquam eligendi. Vitae id fuga culpa qui veritatis. Non assumenda sunt nam quaerat aut vel. Iure ut nam ut. Molestias magni veniam quaerat molestiae excepturi consectetur optio. Nihil incidunt ipsum minus deserunt voluptatem debitis. Eos nihil incidunt repellendus aliquam nemo fugit vitae. Non sint sequi consequatur. Unde necessitatibus in quia laudantium. Odio et omnis optio ut recusandae illum non. Maiores explicabo et rerum ducimus itaque quaerat molestiae. Quod fugit ullam culpa voluptas blanditiis quibusdam quia.',31,175,1,'2018-11-01 22:30:43','2018-11-08 05:19:14'),(3,3,1,6,'Velit quam eos sapiente perspiciatis.','3.jpeg','Aut quia autem earum ipsam sapiente. Explicabo earum quas voluptate.','Laborum officiis eos sint cum. Suscipit assumenda officiis dolor sint impedit consequatur. Molestias temporibus consectetur odit quasi ut natus aut. Dolore neque quas blanditiis similique sit voluptatum iste. Doloremque ratione quo ut explicabo. Rerum porro facilis delectus voluptatem quaerat et quis qui. Sint quia eum voluptatem nisi optio. Aut sed adipisci sit recusandae alias explicabo blanditiis. Quibusdam nostrum ut eos voluptatum esse voluptas dolorem nemo. Explicabo velit est libero saepe. Non qui repellat necessitatibus aliquid. Voluptas nemo maiores culpa dolorem dignissimos sed. Sed repellat commodi eos numquam. Eum cum tenetur atque voluptates. Quia magni aliquid est aut. Et dolores dolorem cupiditate. Rerum et omnis explicabo. Dolores veniam odio inventore dolorem maiores in rerum. Sapiente et et eos in veritatis sit officiis. Hic ea aliquid facilis distinctio maiores. Iste laboriosam sint fuga. Est sit et tempora dolor libero consectetur. Ab saepe vel ipsa facere quisquam in eos omnis. Cupiditate nam quibusdam vero. Non dolorem omnis tempora enim quia molestiae sed. Eum incidunt blanditiis minus aspernatur. Iure odio ut porro consequatur deleniti. Qui atque facere dolores aut. Nihil dolorem eum saepe sed. Omnis suscipit officiis qui commodi. Cum vel consequatur id ullam ratione. Delectus blanditiis accusantium sit illo harum cum ullam. Reiciendis vel neque aliquid inventore temporibus eius. Laudantium cupiditate minima enim nihil velit. Qui quas provident fuga impedit quis. Id est eaque qui perferendis et iste delectus.',12,109,1,'2018-12-21 19:55:29','2018-12-31 02:32:03'),(4,3,3,6,'Autem est qui hic repudiandae dolor.','4.jpeg','Aut itaque atque quos neque. Ut et quisquam qui minus voluptatem laborum. Corrupti nisi maiores quis quos. Quos quasi veniam dolorem consequatur minus est neque aut.','Eum aliquid ipsa sit veritatis vel ullam. Sint quia reprehenderit consectetur. Optio qui et minima est quam sit repellendus. Harum eum quibusdam enim accusantium est. Beatae animi doloribus reiciendis dolor est corporis minus. Est quis harum non animi. Culpa molestiae in et eos placeat voluptatem eligendi voluptates. Commodi ut sed soluta. Ratione nihil et eos modi. Cupiditate esse laboriosam esse aut et sequi eveniet tempora. Quis quia provident voluptas atque fuga. Sed a qui ex dignissimos voluptatem praesentium sit. Animi at illum nesciunt aperiam ratione cupiditate pariatur. Cum adipisci possimus sint temporibus voluptatum ipsam. Dolor eaque ea expedita quia deleniti. Temporibus cumque blanditiis in ipsum facilis incidunt. Tenetur ipsum molestias impedit voluptate tenetur. Excepturi illo reiciendis facilis quidem repudiandae odit nobis reprehenderit. Quasi eveniet expedita velit consectetur asperiores consequatur occaecati. Aut enim totam magni et repudiandae rerum sit. Repudiandae consequatur alias error in modi. Cumque at vero qui optio velit sit unde cumque. Laboriosam saepe iste suscipit officiis quaerat iusto aut. Vel laudantium occaecati ut explicabo dolor magni beatae. Eius sit id laborum odit qui. Exercitationem aliquid necessitatibus et iste incidunt fugit est. Ut voluptates et magnam asperiores. Nihil non libero quibusdam quisquam. Ducimus qui ut et ab suscipit dignissimos. Eius eaque esse unde ut. Quo hic ut est qui. At dolores laudantium temporibus commodi ut reiciendis. Quasi sapiente facilis non debitis vel aut. Reprehenderit corrupti ad consequuntur.',40,125,1,'2018-11-21 00:41:07','2019-01-01 11:27:43'),(5,2,1,6,'Sit numquam molestiae facere illo.','5.jpeg','Quas repudiandae et repellendus cum quo. Animi cum sint sint pariatur expedita impedit illum. Molestiae temporibus vitae hic et nihil.','Nostrum sint enim impedit sequi quis atque. Deleniti minima corporis ad nihil. Sunt placeat ut quia esse. Commodi dolore consequatur illum vitae. Ut molestiae assumenda quisquam nulla esse eum. Vel et ipsa commodi ut adipisci velit totam. Libero est corrupti dolor suscipit mollitia suscipit fugiat. Quisquam dolores neque dolorem non id ipsa. Autem sed sed qui quibusdam vel aut. Rerum eum consequatur ut aperiam voluptas quia voluptatum. Quia dolor laudantium eum qui. Nisi placeat quidem dolores autem est harum unde. Quibusdam in excepturi dolor autem odit molestiae. Deleniti corrupti porro rerum libero qui possimus illo. Provident esse voluptatem maiores. Impedit eum occaecati dolorem. Consectetur qui dolorem voluptas sint recusandae eum adipisci deleniti. Ducimus accusantium nihil ullam. Molestiae sed quibusdam sunt et. Aut unde possimus ex veniam qui nesciunt quia dolorem. Rerum nostrum modi dicta laboriosam excepturi. Minus sit facere est voluptatem ullam veritatis. Magnam natus tempore et mollitia cumque. Dolorum itaque enim eligendi provident. Minus quo reiciendis eaque debitis voluptas eum. Aut qui nisi voluptas rerum nisi sint. Impedit cum tenetur magnam expedita voluptas. Praesentium et unde ex temporibus saepe neque. Enim cupiditate autem veritatis vero dolore occaecati recusandae ut. Minus ex maiores corporis earum. Architecto unde illo aut perferendis a sunt. Aut numquam at quidem molestiae. Aliquam aut numquam sunt et qui recusandae. Error eius sunt earum. Et exercitationem et eius animi enim alias. Voluptatem et recusandae iusto modi qui nam. Impedit et voluptatem earum.',45,112,1,'2018-11-22 01:41:38','2018-11-29 19:01:24'),(6,1,2,7,'Inventore quam cumque aut et et.','6.jpeg','Modi quo facere blanditiis ut veniam provident. Asperiores omnis minima dolorem quidem a. Aut et autem quidem sed. Impedit vitae nam optio voluptas.','Cum neque autem saepe ipsa autem molestiae. Nisi nulla et maiores odit excepturi dolorem at. Nostrum quam optio quia aut ipsam mollitia molestiae minima. Minima accusantium vitae ipsum temporibus autem. Omnis enim debitis omnis illo quae nisi vero. Molestias dolores numquam excepturi dignissimos velit quisquam et nesciunt. Ad repellendus ipsa aut id sit rem quis. Aspernatur accusamus quas doloremque quaerat. Aut aut iusto iusto eaque quo quae. Error eos optio sunt. Aliquam est quia perspiciatis consequatur quis sed debitis. Nobis omnis blanditiis omnis et nulla illum veniam. Non et dolorem occaecati aliquid ab. Eum corrupti quidem in odit. Ad nisi tenetur nulla quod voluptas facilis rerum. Dolore aperiam minima voluptatum et. In nihil itaque illum rerum saepe ipsam accusantium. Natus molestiae sed corporis illum ad id sed. Eius sit quod nesciunt eum. Suscipit quis culpa eos molestiae non. Mollitia consequatur autem itaque quis sit repellat esse. Ut quae quo quia velit. Quia nam ducimus sequi. Mollitia libero asperiores iusto. Sed inventore dolorum aperiam molestiae nisi qui eius. Similique omnis nostrum aut aut. Fuga pariatur mollitia qui qui libero. Odio et quis consequatur qui qui ratione itaque. Voluptates sint minus enim. Fugiat ipsum repellendus consequatur. Voluptatem et sint possimus magnam reiciendis facilis qui illo. Et est assumenda repudiandae tempora dicta explicabo laudantium. Aut modi quisquam provident. Ea fugiat saepe alias voluptatem. Iure corrupti praesentium vero iure eos nihil. Voluptatem ad sed aut sunt voluptatum. Aut saepe maxime suscipit itaque.',29,52,0,'2018-11-13 06:22:17','2018-12-20 11:53:15'),(7,2,1,7,'Consequatur ratione quas iusto.','7.jpeg','Et voluptatibus ratione aut quam est cumque laborum. Non similique qui laborum quia.','Provident sunt tempore maxime atque quia accusamus sit. Veniam quo temporibus corporis sequi autem. Veritatis molestias veniam voluptatibus officiis autem atque. Omnis quia vel sed eos aliquid. Ea delectus ipsa magni maiores at magni. Fugiat perferendis quidem inventore et enim aliquam. Voluptatem dolor odit est pariatur assumenda. Rerum exercitationem molestiae nihil ipsum et in eveniet. Fuga dolores sit voluptas ab. Esse adipisci a non magni maiores error. In tempore optio qui. Tenetur maxime qui aut quo a totam ipsa et. Consequatur repellat voluptates dolor non. Optio eligendi enim expedita. Dolorem totam ut et eius et ut. Iste qui voluptatem libero deleniti minima libero accusamus. Est recusandae dolorem repudiandae occaecati nemo. Officiis officiis et quos enim et tempore dignissimos. Quia quidem voluptatum aliquid. Illo debitis ad aut suscipit quasi accusamus commodi. Iusto quia nihil ipsa consequuntur expedita. Quasi blanditiis non nulla dicta. Aliquid dolorem ipsum quos officiis. Sed culpa non iusto voluptas laborum omnis soluta. Quidem ut maiores accusamus nihil. Alias nesciunt facere et fugit quisquam. Molestias tempora recusandae sed quos omnis consequatur et. Quis aut accusantium laboriosam et eum exercitationem. Sapiente quia velit ut rerum cumque. Quo explicabo et adipisci tempora. Neque vitae et vel quia qui. Blanditiis asperiores enim voluptates maiores cupiditate. Libero eum quod asperiores sed possimus in ipsum magnam. Quo quis voluptatem nihil quibusdam. Eos ut est dolore reprehenderit qui. Et inventore minima reprehenderit suscipit non. Qui magnam ut incidunt et. Qui illo blanditiis non ut error qui. Ipsum impedit accusantium et modi et.',59,76,1,'2018-11-26 06:17:51','2018-12-31 21:32:22'),(8,2,1,9,'Exercitationem quam illum nulla est.','8.jpeg','Corporis sed dolores consequatur quaerat nisi eos corrupti rerum. Sit ea perspiciatis error perspiciatis sint qui voluptas.','Et iure consequatur quisquam. Nisi sed amet eos accusantium nihil aut. Ipsa animi et provident explicabo vel sed incidunt. Voluptatem repudiandae est rerum labore. Numquam ipsa qui occaecati vel rerum sed nesciunt. Deleniti ducimus et beatae aut. Ut laudantium aliquam necessitatibus tempora. Perspiciatis et numquam cupiditate. Repellat reiciendis excepturi ea quia cupiditate et. Quos dolor dicta sed vitae enim est id. Ullam quis rerum necessitatibus doloremque. Dolor dolor quibusdam adipisci. Natus nesciunt nostrum saepe rerum commodi nostrum. Et hic saepe et ad consequatur aut. Dolorem sapiente omnis labore. Fuga sed laudantium rerum beatae quia iste. Dolor consequatur praesentium quia cupiditate omnis consequatur sint. Neque totam et ab laboriosam dolor praesentium quia. Earum laudantium error sit temporibus suscipit fugiat. Libero quis soluta voluptate possimus iure iure. Voluptatem rerum et deleniti harum commodi. Voluptate quis aut accusantium cumque. Ratione laborum esse inventore sint illum eos deleniti est. Ex molestias suscipit dolor ducimus nostrum et eveniet. Molestiae autem id doloribus aperiam ut voluptatum. Alias perspiciatis soluta et ipsam quaerat sed labore. Molestiae illum possimus sunt voluptas velit sit et. Vitae et tempora at quam quod ab. Ipsum et qui laborum accusantium. Perspiciatis repudiandae necessitatibus dolore impedit. Sit rerum vel eum voluptatem. Numquam rerum tempore ut est aut delectus. Soluta reiciendis delectus at vel tenetur. Recusandae dolor nisi tenetur reiciendis ea.',68,128,1,'2018-11-24 03:40:28','2018-12-18 09:22:01'),(9,2,3,4,'Neque ut aliquid rerum occaecati.','9.jpeg','Id vel optio ipsum quae exercitationem. Voluptates nostrum similique commodi non aperiam modi. Autem voluptatem sit dolorum laboriosam molestiae et.','Asperiores ut nihil fuga unde fugit doloribus. Veritatis tempora voluptas nam sapiente. Numquam omnis omnis aut harum iusto ea. Itaque et doloribus qui voluptate impedit. Quaerat hic voluptatem natus voluptatem dolorem. A saepe quaerat expedita dolores. Illum fugit omnis et id. Laboriosam eius est exercitationem libero dolor quia in. Quis et iste culpa commodi tenetur sed. Ut dolor eos et. Dolorem vel quisquam adipisci et quo soluta. Et ipsum nostrum autem maiores exercitationem similique voluptate et. Qui a aspernatur dignissimos illo dicta et est. Delectus sed exercitationem iste quia qui saepe. Sed aut rem porro nesciunt. Nesciunt voluptatum dolores iure doloribus a architecto. Ipsam rerum est officia ea est modi. Est ipsa amet qui saepe optio. Ullam quaerat distinctio sed officia officia molestias. Et magni dolorem laudantium sit eveniet. Porro nulla placeat omnis at. Nam ducimus aut nihil quia. Molestias earum harum rerum odio labore quas tempore ullam. Incidunt omnis quasi quod illo occaecati rerum perferendis. Aut saepe aut corrupti omnis numquam rerum. Nostrum repellendus eum saepe amet omnis eius et. Voluptas placeat quas et. Ut nobis quia dolor. Quia sit vel voluptatem quia exercitationem neque omnis. Et error velit voluptas numquam velit ipsa non similique. Veritatis rem et omnis quis a. Culpa deserunt eveniet quo est error. Non aut rerum consequatur laudantium consequatur ab cumque pariatur. Dignissimos aperiam dolores sit tenetur qui tempore cum. Eius soluta eius consequuntur provident expedita itaque ut laborum. Atque sit harum accusamus. Hic rerum minus et repellendus saepe.',69,140,1,'2018-12-07 18:54:20','2018-12-26 14:04:29'),(10,2,3,10,'At eos fuga dolorum aut et.','10.jpeg','Magni occaecati et nemo exercitationem hic delectus. Error sed dolores illum soluta. Esse iure repellendus voluptatem ut. Ut quibusdam praesentium eum illo porro.','Ut eveniet molestiae qui magni explicabo atque. Nesciunt ut nesciunt voluptatem molestiae aliquid quia quibusdam. Vel saepe rerum iusto dolore dolores. Vitae omnis est magni expedita neque esse consequuntur dolorem. Ut possimus iusto error voluptas magni eum velit. Est doloribus adipisci qui aut quam odit. Sapiente non id quia ut. Eum tenetur quis voluptas qui corrupti nihil. Recusandae assumenda nesciunt qui. Voluptas eos est libero nobis quae cupiditate quam. Omnis dolores quibusdam placeat reprehenderit. Sed ad quasi impedit. Et occaecati quod sed deserunt quibusdam adipisci deleniti. Magni sed non voluptatibus vitae necessitatibus. Occaecati sed cum id. Alias eaque earum inventore deleniti animi ipsa voluptate. Alias voluptatem incidunt ducimus eos aut ducimus. Harum dolore commodi maxime unde voluptas et ea. Ut iure aliquid placeat aut reprehenderit molestiae mollitia. Facere debitis asperiores aliquam odit. Quia architecto incidunt totam et quae veniam voluptas. Iure rem beatae nihil placeat sed adipisci hic sit. Atque rerum voluptas reprehenderit. Nesciunt molestiae veritatis hic vero. Repudiandae omnis aut natus aliquid minima minus fuga. Corporis sit repellat quia provident ducimus dolores. Quibusdam nostrum esse maiores praesentium minima nihil suscipit qui. Itaque praesentium non sit accusamus et eos porro. Eum tempora nisi perspiciatis praesentium quod sed. Perspiciatis occaecati adipisci consequuntur vero. Neque eum voluptas autem maxime minima asperiores voluptas esse. Voluptas possimus qui et soluta et non officia. Aliquid et dolores sit voluptas. Quaerat minus vitae ea ut rem. Et atque reiciendis commodi.',77,99,1,'2018-12-04 00:49:57','2018-12-11 21:31:56'),(11,3,1,1,'Quas iure aut vel sed autem.','11.jpeg','Aut tenetur placeat optio rerum iste. Corporis vero illum minima illo est ratione et.','Eius quos corporis eos sed voluptas. Consequatur vitae dolorem deserunt eum voluptatem. Rerum velit quod et optio itaque. Sunt porro officia quasi eaque officiis. Ad qui laborum nihil nisi. Odio et ducimus consequatur quibusdam sit non. Quasi natus illum et sunt et voluptas quia. Porro molestiae quia et optio sint delectus. Fugiat unde repellat doloremque excepturi placeat sit. Voluptatem excepturi in temporibus nobis cupiditate veritatis consequuntur et. Id dolor qui est quis. Impedit suscipit et architecto est similique. Aut necessitatibus dicta et ratione in omnis. Officia alias quia velit corrupti. Delectus incidunt est repudiandae et. Rerum hic dolorum rerum quia molestiae omnis et. Voluptatum ipsum consectetur ut molestiae praesentium. Dolorem corporis laborum aut ipsa quod iusto. Quia quo sapiente voluptatem enim. Vitae et iure dolores. Non itaque animi enim aut vitae ab voluptatem ipsum. Odio est reprehenderit sint. Ea consequuntur corrupti illum. Sit iusto et laboriosam pariatur enim dolore est. Tempore maiores sit reprehenderit rerum ut eligendi incidunt natus. Laborum omnis non repellat placeat eum soluta. Corporis vitae et odio impedit similique. Est similique qui soluta similique quia dolores. Nobis odio fuga sed aliquid ut quis et. Dignissimos earum porro ut facilis quia. Ut in nemo enim consectetur quidem earum esse. Est animi quas rerum. Sed ut corporis sed odit iusto excepturi ea. Maxime est reiciendis exercitationem quia tempore. Et quia earum incidunt quia aut ut neque optio. Quaerat asperiores in odio deleniti nihil earum qui id. Nostrum sit dolorum non sint. Est ipsa eum id quis. Enim eos temporibus ut sunt autem molestiae.',57,196,1,'2018-10-28 12:25:34','2018-12-26 15:22:34'),(12,1,1,1,'Rerum sint ut cumque.','12.jpeg','Sed aperiam sed et vel. Cumque voluptas non nihil in sed maxime sit. Praesentium sed velit harum sapiente.','Error quia dolores occaecati aut sit adipisci. Exercitationem est deleniti a earum impedit aut. Occaecati ipsum est ad animi nemo quae minima. Consequatur aliquid ratione magni repellendus alias. Totam qui quo doloribus voluptates. Mollitia suscipit eum quidem autem commodi ut. Autem repellat perferendis est maxime impedit sit molestiae inventore. Ut beatae consequatur culpa facere laboriosam repellendus eos aliquam. Ea aut molestiae eveniet tempora labore expedita voluptas. Labore excepturi nihil natus corrupti. Labore assumenda velit occaecati alias ea nobis neque. Et rem voluptas perferendis enim rem perferendis. Eligendi nostrum sint nam nobis et praesentium ut. Maxime quia sed debitis culpa accusamus. Accusamus occaecati voluptate libero est aut explicabo. Ut a at rerum. Aut nobis placeat voluptatem beatae rerum cumque at. Vitae dignissimos non ipsam adipisci sed provident facilis. Dolor odio voluptas unde. Aliquid nostrum illo est et. Eligendi rerum velit sed labore optio qui eos. Sit maxime id molestiae voluptatem assumenda et. Nihil ut rem et voluptatem repudiandae omnis ut tempora. Explicabo quas blanditiis sed sunt. Nam quos consequuntur doloremque eum ut. Fuga in sit ipsum in laboriosam et libero. Inventore est veniam cumque et et molestiae. Iste omnis excepturi temporibus animi aut tempora. Quo sit a id sit odit. Soluta recusandae voluptatem corporis ullam libero sed. Et eligendi est quam quos ut error. Eius animi et excepturi qui veritatis ratione. Distinctio doloremque voluptate error. Maiores adipisci necessitatibus eos reiciendis nesciunt. Occaecati et reprehenderit quam repellendus sint. Et temporibus voluptatem in autem. Ipsa quidem aut ea sit id.',27,72,1,'2018-10-09 06:08:49','2018-11-19 21:14:50'),(13,1,2,6,'Occaecati modi et nemo quisquam.','13.jpeg','Corrupti ut et aliquid laboriosam dolores est atque. Minima veniam itaque eveniet odit fugit et. Cumque assumenda eos sed quis aut.','Esse optio ex dolorem et ex dolores neque. Et recusandae vel eveniet in est id. Quos atque similique nulla illum fuga. Omnis cumque voluptatem nobis veniam laborum est velit. Non distinctio eveniet error voluptas dolor veniam. Sunt ut perspiciatis harum nemo qui doloribus. Ad impedit non voluptatem rem minus deleniti. Aut et qui et et. Ad perferendis repellat sit autem quia odit expedita. Soluta quidem tenetur pariatur vero molestias vel. Dignissimos cumque rerum odit vel quo modi repudiandae. Quia blanditiis facere nihil officia quibusdam quaerat dolorem. Doloribus neque recusandae corporis aut. A magnam non corporis quasi cumque exercitationem quaerat. Officia dolore non quis non itaque odit qui. Quo quia impedit est amet dolor maxime nulla. Dolorum recusandae occaecati sint sit qui distinctio. Deleniti repellendus voluptatem perspiciatis nostrum et quia. Esse ut odit libero. Repellendus nulla est dolore error. Earum ipsa assumenda velit eaque. Temporibus velit dolorem sit eaque illo necessitatibus qui. Qui quo itaque delectus architecto omnis quia. Nemo rerum aut non aperiam culpa maxime et. Omnis iure blanditiis amet blanditiis. Exercitationem alias amet qui provident dolor. Animi libero ut explicabo sit accusantium officiis. Laboriosam numquam ut voluptatem atque. Rem et delectus tenetur. Tempora est ut aut. Et officiis ab est odit voluptates nemo nobis. Enim quasi et a architecto. Dolore iste excepturi consectetur ut. Cupiditate consequatur ullam cumque ex. Consequatur aliquam nobis deserunt quia.',37,117,0,'2018-11-05 12:20:42','2018-12-31 00:23:39'),(14,2,1,3,'Cum sit aut qui.','14.jpeg','Est modi ex nam exercitationem doloribus vitae sit. Ratione culpa vel labore sint omnis. Saepe perferendis qui aut alias culpa.','Facilis corrupti facilis cum dolore quisquam. Ut et voluptatem voluptatem odio similique et blanditiis. Sint qui est et officia perspiciatis. Officia exercitationem ut qui dicta. Est ratione dignissimos voluptas doloribus ut amet. Ad minus neque voluptatem qui dicta sint itaque. Ab quasi ea pariatur consequatur ad. Harum non non rem. Natus et voluptatem aspernatur suscipit incidunt eligendi aut reiciendis. In molestiae sed ipsa est. Nulla earum cum dolor illum aliquid. Praesentium et totam qui voluptas aut. Explicabo et id aut voluptatem ad minima officiis ratione. Voluptatum dolor provident occaecati non dignissimos soluta aut. Fugiat expedita voluptatibus doloribus alias numquam mollitia velit. Ratione quae molestiae commodi aut. Eveniet cupiditate nostrum unde velit beatae facere voluptatum. Quia consequatur est sunt consequatur aperiam enim est ut. Optio vero quaerat quam sed et quas. Aliquam dolor sit qui voluptas. Temporibus accusamus sed maiores occaecati recusandae. Enim quas aut qui iure. Magnam nostrum adipisci a ut fugiat. Dolores libero veritatis libero in quia facere id. Sed quam et assumenda et nihil. Voluptas doloremque sunt et sunt eum. Officia natus illum dolorem quo veritatis optio et. Alias quod velit labore distinctio maxime dolorem. Rerum nesciunt voluptatem praesentium odio saepe qui illo. Et voluptatem ut sequi. Eos aut culpa laborum officia aut officia. Est mollitia et qui et molestiae. Et qui nam sapiente atque. Architecto in debitis nam vel. Sit magni voluptatum vitae tempore quae quisquam. Laborum voluptatum sed voluptatem molestiae qui quo est. Atque unde et quis commodi cumque.',55,246,1,'2018-11-24 20:31:05','2018-12-13 22:47:04'),(15,3,2,9,'Placeat ut modi laboriosam voluptas.','15.jpeg','Est ratione eos blanditiis. In explicabo quia ut eos dicta omnis. Optio ut ad nobis id aperiam placeat quo.','Officiis est et illum et. Consequatur et nulla quod quaerat eveniet. Amet suscipit iste non voluptatibus provident tenetur. Sed nesciunt ullam tempora placeat repellat. Quas sapiente beatae maxime et. Modi asperiores et amet eos. Deserunt soluta molestias fugiat molestiae quia. Non et natus atque quod praesentium. Explicabo voluptatem sint ut sed architecto. Est et qui est sequi saepe ut eum. Et unde nulla sit blanditiis itaque. Voluptates enim sed velit modi. Expedita culpa rerum quas eveniet eaque voluptatem illo. Dolore vel qui dolorum doloribus rem eius vel. Qui ratione iusto ipsa fuga deleniti autem. Numquam porro voluptatem quod quia nesciunt ducimus deleniti. Fuga dolorem enim magnam enim ullam ut. Laborum ut illo ex est aut minima. Aliquam unde non quaerat qui voluptatum cum officia. Modi iure quia dolores voluptate ea quae provident. Sequi porro quo ipsam rerum voluptas laborum. Voluptas cumque delectus asperiores tempora. Accusamus illum aspernatur consequatur et. Sint hic cum quaerat pariatur qui. Dolore voluptas minus voluptas error ducimus repellat blanditiis. Explicabo commodi nemo cumque et quasi molestiae omnis. Distinctio consequatur dolorum voluptatem aut eum. Deleniti et enim voluptatem fugiat et quia nam. Perferendis ut laborum ex voluptatum. Et perspiciatis beatae ea aut harum commodi. Quo autem et accusantium dolore ex quidem. Et perferendis voluptas nulla consequatur aut nobis. Est corrupti modi ut veniam quos quia. Vitae deserunt sequi magnam qui rerum corporis libero. Dolorum quia magnam mollitia itaque placeat. Aperiam debitis magni nulla ipsum modi veniam qui. Quis provident tempora dolor nulla.',43,180,1,'2018-12-16 19:10:50','2018-12-24 10:57:43'),(16,1,3,2,'Reiciendis velit quaerat mollitia.','16.jpeg','Autem in possimus ea ab omnis est ad. Quae non omnis at error. At est dolorum esse exercitationem soluta minus quis. Nostrum beatae rerum dolor tenetur et.','Recusandae libero molestiae dolorum autem dignissimos. Dolor iusto dolorum iusto et. Sint magni ut qui repudiandae quia iusto cupiditate. Est libero sapiente animi rerum. Omnis ex omnis iusto sunt. Suscipit animi dolores beatae. Maiores aliquid unde accusamus quod omnis. Et quasi corrupti sapiente perferendis omnis nulla iure. Qui quia explicabo possimus iste id architecto facere. Sit recusandae libero explicabo. Sunt sed magnam praesentium qui consequuntur. Commodi adipisci maiores molestiae sint voluptatum. Adipisci molestias et fugiat incidunt. Consequatur voluptatem id alias ab. Excepturi consequatur corrupti a exercitationem ullam exercitationem. Voluptas neque ab ut ut. Illum voluptatem nihil porro est architecto nobis. Harum dolore quidem mollitia esse. Quibusdam deserunt sint cumque mollitia hic. Voluptatem ullam labore nesciunt at consectetur. Qui neque quo est et et. Deleniti nobis et ea iure id. A vero ea nobis. Suscipit sequi laborum voluptates velit omnis nam maxime. Voluptatum enim labore perferendis. Qui fugit vitae eum non minima blanditiis. Enim est sit dicta corporis. Repudiandae quia officia exercitationem vitae molestiae. Perferendis reiciendis suscipit est cum. Ut eius magni perferendis et minus aperiam beatae. Excepturi qui necessitatibus ullam nihil saepe aut. Sed nulla voluptate id porro nesciunt. Iure laudantium dolores id odio. Sit qui sit minus sapiente sit. Fugiat et earum earum consequuntur. Nesciunt odit et ullam error. Doloribus qui non quos in. Neque officiis harum libero eaque possimus rerum. Ea et mollitia esse iste suscipit modi. Et autem alias assumenda amet rerum repudiandae. Vero facilis enim fuga perspiciatis eos eum eveniet.',80,214,1,'2018-10-31 01:38:31','2019-01-02 06:18:28'),(17,3,3,8,'Qui quod itaque itaque eos.','17.jpeg','Et aut est illum dicta sunt. Expedita soluta eveniet amet non explicabo ipsa. Quos quia aut non ea.','Omnis aspernatur dolorum minus qui velit. Aut saepe nihil rerum soluta velit qui. Ipsa facilis ad consequatur rerum beatae quam. Ullam doloribus id qui molestiae. Blanditiis earum ipsam enim vel nostrum autem voluptatem. Qui dolorem autem quis voluptatum hic numquam. Vel doloribus possimus animi perspiciatis reprehenderit magnam assumenda sunt. Architecto tenetur hic non enim dignissimos recusandae occaecati. Sequi ex dolor explicabo soluta fugiat totam saepe. Sed quia omnis est. Quis omnis in quia numquam omnis numquam ipsa. Dolore consequatur quos facere numquam non et et. Rerum ullam ut in quis aut velit. Minima quod tempore praesentium quos quibusdam vel commodi. Harum explicabo qui omnis. Dolorem exercitationem doloribus similique qui quae corporis est. Minima est officiis pariatur rerum tempora aut. Occaecati odio voluptatem praesentium. Voluptate officia aut nobis eos suscipit inventore. Rem maiores tempora tenetur ipsum corrupti cum. Nihil praesentium adipisci tempore modi sapiente est corporis saepe. Accusantium ullam ea incidunt aut inventore nihil fugit. Cum sint ullam quia rerum maiores autem est. Reprehenderit est eius fugit quos exercitationem consectetur. Rerum itaque commodi qui omnis dolore. Iure magni dolor consequatur voluptas id distinctio distinctio et. Vero possimus aut suscipit exercitationem aut tempore rerum. Eligendi explicabo vitae repellendus eaque qui corrupti culpa. Occaecati mollitia commodi doloremque aut. Et iusto qui nesciunt magni maxime odit velit. Voluptatibus aspernatur nihil non sunt omnis. Occaecati voluptas quae aut animi. Architecto et omnis maiores rem.',24,110,1,'2018-11-28 00:57:27','2018-12-16 03:52:21'),(18,1,2,5,'Est ipsum inventore molestiae.','18.jpeg','Omnis cumque aliquam et sapiente culpa. Quo consequuntur ipsa esse a cum est. Aut optio est repellendus et.','Dicta maiores maxime iure magnam rerum. Laboriosam omnis repellat repellat maiores rerum ullam ipsa est. Quae alias atque non sed tenetur. Quaerat qui officiis non deserunt totam. Doloremque id voluptates est rerum magnam aut voluptas consequatur. Eius nisi exercitationem quo. Voluptatem in fuga excepturi commodi ipsa laborum deserunt. Aut ut voluptatem qui et dolore. Aut assumenda optio in quo ipsum mollitia. A dolor quo dolorum atque culpa aliquid. Quibusdam aut enim totam consequatur voluptate temporibus fugit. Quibusdam ut laboriosam id rem. Quidem cum porro laboriosam esse sit in dignissimos ipsum. Suscipit eaque saepe enim distinctio harum est. Aut omnis sed reprehenderit et. Tempora asperiores autem eum qui ab. Enim animi optio nemo fuga et reprehenderit. Laborum occaecati in explicabo et dolorem non ea. Nam reprehenderit praesentium voluptas minima voluptas. Quia provident quo aut cupiditate nemo et. Dolorem eveniet natus blanditiis beatae ad minus. Quas autem velit quasi qui magni. Alias qui doloremque fuga dolores. Sapiente occaecati sit ut minus repellat temporibus quo. Ut illo sit cupiditate totam. Officia optio est nulla ut nobis. Velit vitae minus facilis inventore velit facilis. Et corrupti adipisci tempora et molestiae sit cupiditate. Quam officiis similique labore qui et. Facilis itaque reiciendis eum est debitis. Sit animi earum veritatis occaecati dolor. Ea nam sunt ut natus voluptatem sed et. Itaque illo aliquam amet et officiis culpa voluptatem. Est numquam ea doloribus consequatur. Molestiae expedita quia cupiditate. Nam autem saepe ut iste molestiae placeat.',75,182,0,'2018-12-28 02:12:20','2019-01-01 08:48:14'),(19,2,3,4,'Minima ab sequi vel tempora.','19.jpeg','Et voluptates aut ut laborum quis praesentium voluptatem qui. Enim quia inventore natus suscipit nulla qui. Odio ratione repellat et eos.','Reprehenderit neque enim voluptatem dolores itaque blanditiis quisquam. Beatae ullam tenetur aliquam autem sapiente. Accusamus est eius in et. Voluptas aut et est. Omnis incidunt consectetur aut laudantium alias ab enim. Maxime aliquid dolores laborum quidem fugiat earum. Iusto aut voluptatem et nemo similique et odit. Labore omnis voluptatem omnis quia. Ut nihil soluta id omnis ut. Delectus voluptas dolorum eligendi quia qui enim. Voluptatibus consequuntur at necessitatibus fuga est aspernatur consequatur aliquam. Ad aliquid quae eos voluptas. Reiciendis ad ab ut beatae est est consequuntur aut. Deleniti mollitia dolorem magni ea placeat. Magni aut tempora reiciendis assumenda. Aliquid non est repellendus laudantium. Hic velit qui consectetur velit soluta earum harum quo. Sint voluptas ex quo autem quos omnis sit. In et facilis reiciendis ipsam nobis. Ea quasi eaque repellat cum voluptas quaerat. Iste debitis numquam adipisci eos quo. Ut commodi aut quisquam eius architecto aperiam. Suscipit ut iure ut nemo. Doloremque aliquid fugiat velit voluptates possimus laborum. Dolore ipsam quis nisi placeat dolores voluptatem et. Sed architecto quasi incidunt maiores aut ea quod non. Perferendis rerum accusantium omnis molestiae sequi. Ut omnis totam cumque quia voluptates nemo qui. Velit soluta molestias quia odit nesciunt dolor voluptatem. Autem voluptatem deserunt quibusdam error quos reprehenderit. Ratione ut aut sunt velit laboriosam omnis magni. Voluptatibus voluptas maxime placeat. Quidem est eligendi maiores odit nisi nesciunt et.',61,57,1,'2018-10-28 16:23:04','2018-11-10 12:54:26'),(20,2,2,4,'Natus beatae voluptatem aut.','20.jpeg','Ipsam molestiae quaerat placeat optio alias est qui labore. Quae magnam odio est.','Laborum ut numquam maiores quam. Vero reprehenderit nihil omnis sunt voluptatem repellendus. Et facere dolorem quasi saepe doloremque quo eveniet. Rerum ut ea nisi temporibus eveniet vel voluptatem ea. Natus ea temporibus animi quia est est omnis vero. Minus et non est occaecati et et et. Tempore facere recusandae consequatur assumenda consectetur. Ab facere dolore laborum voluptate adipisci inventore exercitationem. Quia ipsam non dolorem enim. Ducimus impedit rerum sed. Et ullam doloremque qui eos et non quia. Suscipit ratione deserunt magni velit. Et sequi tempore in distinctio officiis aut. Eveniet aut ipsa in id. Eveniet accusamus voluptatem aut quaerat odio aperiam maiores. Sint vitae at sequi veniam. Facilis unde rerum molestias ab quae maiores. Deserunt eum facere et illo corrupti autem eos. Voluptatibus nobis qui voluptas possimus facilis exercitationem tempora. Ab molestiae libero sit et. Dignissimos placeat laborum officia non. Ex at veritatis ad voluptatem voluptatem vero. Provident praesentium ut est aut ut nihil. Qui aut at veniam quis consectetur voluptates. Nihil dolor voluptatum facilis quod alias. Doloribus molestiae perspiciatis molestiae distinctio. Et rerum et maiores. Ullam omnis quae nisi optio soluta. Pariatur est autem sunt tempore. Repellendus qui culpa nihil non est tenetur rerum fugit. Maiores asperiores dolor ipsam dolores qui amet. Odit sed numquam et. Omnis voluptatum accusantium non voluptas unde. Laboriosam molestiae occaecati harum rerum quis et. Sed delectus tempora tempore soluta a rem distinctio quis.',37,182,1,'2018-11-15 12:58:02','2019-01-03 17:20:55');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_category`
--

DROP TABLE IF EXISTS `course_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_category` (
  `course_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`,`category_id`),
  KEY `IDX_AFF87497591CC992` (`course_id`),
  KEY `IDX_AFF8749712469DE2` (`category_id`),
  CONSTRAINT `FK_AFF8749712469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AFF87497591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_category`
--

LOCK TABLES `course_category` WRITE;
/*!40000 ALTER TABLE `course_category` DISABLE KEYS */;
INSERT INTO `course_category` VALUES (1,2),(1,4),(1,6),(2,5),(2,6),(3,5),(4,5),(5,4),(5,6),(6,1),(6,6),(7,2),(7,3),(8,2),(8,6),(9,2),(9,5),(9,6),(10,5),(11,2),(11,3),(12,4),(12,5),(13,1),(13,4),(14,3),(15,2),(15,5),(16,3),(16,4),(17,2),(18,6),(19,2),(20,5);
/*!40000 ALTER TABLE `course_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_comment`
--

DROP TABLE IF EXISTS `course_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9CB75780A76ED395` (`user_id`),
  KEY `IDX_9CB75780591CC992` (`course_id`),
  KEY `IDX_9CB75780727ACA70` (`parent_id`),
  CONSTRAINT `FK_9CB75780591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_9CB75780727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `course_comment` (`id`),
  CONSTRAINT `FK_9CB75780A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_comment`
--

LOCK TABLES `course_comment` WRITE;
/*!40000 ALTER TABLE `course_comment` DISABLE KEYS */;
INSERT INTO `course_comment` VALUES (1,47,5,NULL,'2018-12-12 22:00:55','2019-01-03 15:18:39','Sint rerum odit est doloribus cum.',1,0),(2,23,12,NULL,'2018-11-01 11:21:58','2018-11-27 20:08:39','Et sit quasi molestiae vel asperiores occaecati.',1,0),(3,8,11,NULL,'2018-11-15 09:14:07','2018-12-19 00:33:44','Enim molestias qui dolores accusamus ex.',1,0),(4,1,16,NULL,'2018-11-29 18:05:18','2018-12-22 07:36:34','Vel dignissimos quasi eos.',0,0),(5,2,1,NULL,'2018-12-26 09:39:16','2018-12-31 01:48:02','Occaecati aliquid atque eum.',1,0),(6,26,12,NULL,'2018-10-16 13:00:50','2018-12-13 04:23:23','Rerum eaque velit minima est minus sequi.',1,0),(7,43,13,NULL,'2018-11-22 16:25:17','2019-01-01 12:18:44','Consequatur nam vel autem vel.',1,0),(8,12,6,NULL,'2018-12-23 11:55:44','2018-12-31 06:52:58','Repellendus et deserunt et et recusandae.',1,0),(9,61,1,5,'2018-12-16 13:59:06','2018-12-30 00:29:44','Veritatis quia quibusdam ut est aut.',1,0),(10,40,12,NULL,'2018-10-13 15:12:37','2018-10-19 16:53:48','Tenetur est laudantium id est explicabo.',1,0),(11,38,19,NULL,'2018-12-05 01:43:01','2018-12-15 15:49:04','Quia quasi beatae sunt est autem accusamus.',1,0),(12,32,16,4,'2018-11-02 02:09:29','2018-11-11 12:19:16','Deleniti non eum totam est nam rem culpa debitis.',1,0),(13,3,9,NULL,'2018-12-10 01:49:07','2018-12-16 14:28:35','Delectus molestiae quos sint unde.',1,0),(14,16,13,7,'2018-12-24 11:56:31','2018-12-31 22:05:28','Est optio provident reiciendis veritatis.',1,0),(15,36,12,10,'2018-12-30 12:09:04','2018-12-31 09:03:26','Perspiciatis alias cumque est rerum.',1,0),(16,33,1,5,'2018-12-25 20:32:18','2019-01-04 05:10:27','Voluptatem quaerat illo laborum.',1,0),(17,2,5,1,'2018-12-13 00:38:47','2018-12-16 04:21:00','Voluptas quaerat velit asperiores quisquam.',1,0),(18,23,1,16,'2018-12-29 20:07:00','2019-01-04 05:23:25','Laudantium inventore aspernatur ea.',0,1),(19,32,5,17,'2018-12-12 01:34:15','2018-12-23 20:17:19','Sunt occaecati et quasi aut.',1,0),(20,60,19,11,'2018-12-23 12:20:56','2018-12-24 02:48:24','Necessitatibus repudiandae eveniet repellat.',1,0),(21,45,12,10,'2018-12-30 19:16:57','2019-01-01 03:51:04','Sequi pariatur beatae sunt.',0,0),(22,40,16,4,'2018-12-02 16:19:19','2018-12-15 23:10:07','Est similique qui ex id optio itaque fugiat.',0,1),(23,10,5,1,'2018-12-13 18:39:49','2019-01-03 17:04:47','Dolorem ratione tenetur sint hic.',1,0),(24,23,12,10,'2018-11-15 16:19:12','2018-11-26 18:57:14','Molestiae doloremque ipsam ducimus suscipit cum.',1,0),(25,57,19,NULL,'2018-12-31 23:40:41','2019-01-01 03:04:18','At beatae natus iste alias similique.',1,0),(26,2,16,4,'2018-12-25 05:45:34','2019-01-01 15:40:41','Tempora ullam rerum delectus cupiditate.',1,0),(27,26,4,NULL,'2018-12-29 01:55:52','2018-12-31 14:47:05','Eum non reiciendis et perferendis sunt incidunt.',1,0),(28,41,4,NULL,'2018-12-08 16:30:39','2018-12-12 18:56:32','Mollitia accusantium iusto consequatur ad.',1,0),(29,9,5,17,'2018-12-17 18:51:28','2018-12-27 11:43:15','Quia deserunt quia saepe qui eos rerum.',1,0),(30,23,5,23,'2018-12-23 18:17:30','2018-12-31 04:31:59','Ullam corporis laboriosam et vitae esse quis.',1,0),(31,61,1,9,'2018-12-17 14:23:49','2019-01-01 00:41:14','Eos laudantium est illum dignissimos velit sed.',1,0),(32,7,11,3,'2018-12-23 14:17:51','2018-12-28 13:29:31','Sed sed facilis qui molestias sed qui sunt.',1,0),(33,32,16,22,'2018-12-30 05:06:48','2019-01-02 18:35:00','Nostrum voluptatum soluta incidunt ducimus.',1,0),(34,51,14,NULL,'2019-01-03 22:21:36','2019-01-04 05:24:23','Optio facere rerum ut corporis.',1,0),(35,38,3,NULL,'2018-12-31 17:25:12','2019-01-03 20:40:20','Velit quia vel recusandae qui sint.',0,1),(36,48,5,30,'2018-12-23 19:42:34','2018-12-29 15:25:38','Omnis qui sit quisquam quidem rerum.',0,0),(37,4,12,24,'2018-12-28 10:38:14','2018-12-29 22:53:59','Ipsa recusandae at illum.',1,0),(38,36,18,NULL,'2019-01-01 13:01:47','2019-01-02 23:12:28','Ea quam perferendis recusandae ipsam.',1,0),(39,33,19,20,'2018-12-30 12:46:03','2019-01-01 04:51:12','Quia molestiae ab quis molestiae veritatis.',1,0),(40,28,3,35,'2018-12-22 20:41:55','2018-12-24 13:54:03','Corporis doloribus quo ratione eum repellat.',0,0);
/*!40000 ALTER TABLE `course_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_promotion`
--

DROP TABLE IF EXISTS `course_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date_validity` datetime NOT NULL,
  `end_date_validity` datetime NOT NULL,
  `percent_on_price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_promotion`
--

LOCK TABLES `course_promotion` WRITE;
/*!40000 ALTER TABLE `course_promotion` DISABLE KEYS */;
INSERT INTO `course_promotion` VALUES (1,'#b605bf#b9c876','2019-01-28 11:39:59','2019-01-29 22:27:58',21),(2,'#213f75#e3d627','2019-01-01 15:59:26','2019-01-04 01:54:00',19),(3,'#a1ceba#5730f0','2019-01-13 01:22:46','2019-01-28 06:59:18',25),(4,'#728678#bca6dd','2019-01-03 05:54:18','2019-01-18 04:40:44',15),(5,'#82f049#2a3592','2019-01-14 05:44:32','2019-02-02 17:47:22',26),(6,'#3dc242#a37c84','2018-12-27 11:48:26','2019-01-09 01:56:44',17),(7,'#0cab6c#195aaa','2019-01-01 05:42:23','2019-01-21 06:13:21',22),(8,'#834669#b0e468','2019-01-12 10:14:20','2019-01-18 13:15:41',14),(9,'#638d46#473961','2019-01-22 00:49:26','2019-01-23 00:22:05',24),(10,'#cba5cb#312eb1','2019-01-20 18:50:41','2019-01-26 16:20:00',12);
/*!40000 ALTER TABLE `course_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_registration`
--

DROP TABLE IF EXISTS `course_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `paid_amount` double NOT NULL,
  `tax_rate` int(11) NOT NULL,
  `is_cancelled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E362DF5AA76ED395` (`user_id`),
  KEY `IDX_E362DF5A591CC992` (`course_id`),
  CONSTRAINT `FK_E362DF5A591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_E362DF5AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_registration`
--

LOCK TABLES `course_registration` WRITE;
/*!40000 ALTER TABLE `course_registration` DISABLE KEYS */;
INSERT INTO `course_registration` VALUES (1,60,9,'2018-12-08 04:58:38','2018-12-27 21:08:28',140,21,0),(2,10,6,'2018-11-22 07:08:59','2018-11-30 21:33:42',52,12,1),(3,5,6,'2018-11-25 18:06:01','2018-11-29 05:59:19',52,12,0),(4,4,11,'2018-11-23 20:25:08','2018-12-14 18:25:46',196,6,0),(5,35,16,'2018-11-15 00:39:58','2018-12-11 12:22:05',214,21,0),(6,46,10,'2018-12-24 05:12:46','2018-12-29 02:00:12',99,21,0),(7,40,7,'2018-11-27 02:35:49','2018-12-25 13:22:13',76,6,0),(8,42,13,'2018-12-01 23:40:07','2018-12-19 14:23:34',117,12,0),(9,43,5,'2018-12-17 20:08:16','2018-12-28 01:00:35',112,6,0),(10,32,15,'2018-12-31 07:33:22','2019-01-01 22:15:47',180,12,0),(11,8,16,'2018-11-05 07:03:32','2018-12-08 20:44:01',214,21,1),(12,47,1,'2018-12-21 10:37:32','2019-01-04 07:27:45',78,12,1),(13,50,3,'2018-12-27 16:18:38','2018-12-31 13:21:36',109,6,1),(14,12,16,'2018-11-17 21:01:25','2018-11-18 19:32:36',214,21,0),(15,48,18,'2018-12-28 06:50:24','2018-12-29 05:19:34',182,12,0),(16,54,7,'2018-12-30 20:51:17','2019-01-04 07:40:31',76,6,0),(17,14,13,'2018-12-01 13:42:56','2019-01-03 19:01:52',117,12,0),(18,51,7,'2018-12-02 04:41:12','2018-12-09 14:07:26',76,6,0),(19,53,8,'2018-12-30 22:53:13','2019-01-02 01:48:28',128,6,1),(20,36,6,'2018-12-11 23:26:38','2018-12-12 19:39:42',52,12,1);
/*!40000 ALTER TABLE `course_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_review`
--

DROP TABLE IF EXISTS `course_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_review` (
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` smallint(6) NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`course_id`,`user_id`),
  KEY `IDX_D77B408B591CC992` (`course_id`),
  KEY `IDX_D77B408BA76ED395` (`user_id`),
  CONSTRAINT `FK_D77B408B591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_D77B408BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_review`
--

LOCK TABLES `course_review` WRITE;
/*!40000 ALTER TABLE `course_review` DISABLE KEYS */;
INSERT INTO `course_review` VALUES (1,14,2,'Cum labore nostrum quaerat magni et voluptatem omnis. Numquam sed qui occaecati quaerat sit enim.','2018-12-12 12:19:26','2018-12-31 08:17:23',0,0),(1,35,3,'Aut eius et rem aut. Debitis sit et amet consectetur eveniet et. Est eum facilis non aut enim non.','2018-12-22 18:27:28','2019-01-01 15:44:15',1,0),(3,45,3,'Vitae unde quasi sed quia. Voluptatum nemo eum itaque veniam qui. Suscipit beatae quidem accusantium aut est.','2018-12-26 04:34:59','2019-01-02 04:28:11',1,0),(4,8,1,'Maxime sapiente dolor impedit aut. Eaque autem blanditiis iste et autem.','2018-11-21 07:14:39','2018-12-25 09:44:35',1,0),(4,33,2,'Vero facilis at itaque autem. Sit in iste odit vero eos voluptatem soluta.','2019-01-04 08:37:07','2019-01-04 08:47:13',0,0),(4,51,3,'Pariatur placeat quia repellat eum. Ut architecto totam distinctio sed necessitatibus sint. Qui qui in unde odit enim.','2019-01-03 03:21:11','2019-01-03 17:04:32',1,0),(4,58,5,'Quo ipsa et aut vel et reiciendis. Et cupiditate quod nobis harum. Vero eveniet repudiandae fugit et similique.','2018-12-21 07:59:38','2018-12-28 11:28:02',1,0),(5,38,4,'Modi quis deleniti voluptas eligendi eos sed voluptas. Iusto fugiat tenetur rerum atque optio aut est.','2018-12-05 09:05:46','2018-12-08 13:25:55',1,0),(6,41,3,'Quos quisquam nobis deleniti. Occaecati ratione sint explicabo ut. Similique maiores nostrum vero a eos.','2018-12-25 23:16:01','2018-12-26 15:36:07',1,0),(6,48,2,'Et rem porro aut tempora veritatis neque. Possimus laudantium ex deleniti. Tenetur non id quia minus est quo.','2018-12-07 13:47:47','2018-12-15 10:36:19',1,0),(7,57,4,'Qui porro fugit vel maiores libero a praesentium. Quia odit ut excepturi. A aut voluptatem voluptas.','2019-01-03 15:35:44','2019-01-03 20:55:08',0,0),(7,61,4,'Et ipsam voluptatem totam. Atque maxime quidem rerum et sed.','2018-12-04 15:20:03','2018-12-06 07:55:11',0,1),(8,2,4,'Culpa harum fugiat maxime debitis. Praesentium ullam quod enim et. Assumenda eius ullam quas quas quam.','2018-12-10 07:55:49','2018-12-31 19:26:58',1,0),(8,3,4,'Non repellat aut facilis quidem ut. Omnis quis asperiores et. Earum earum excepturi quo soluta at qui.','2018-12-03 10:59:09','2018-12-17 14:41:19',0,0),(8,33,3,'Dolor eum consectetur officia dolorem mollitia. Est qui libero optio non ipsa. Quia sit ut ut laboriosam.','2018-12-25 07:23:16','2019-01-02 08:53:35',1,0),(9,33,3,'Ipsa magni error perferendis sed sint non est. Et perferendis facilis officia in dolores necessitatibus.','2018-12-14 06:20:53','2018-12-31 04:13:04',1,0),(12,1,4,'Dolorum quis quis et qui. Rerum odio quaerat sed. Praesentium recusandae possimus quaerat. Voluptas quas aliquam nihil.','2018-10-28 20:43:41','2018-11-02 15:34:17',1,0),(12,31,1,'Ea corrupti exercitationem vero eius. Officiis fugit dicta commodi. Maxime dolore totam fugit nobis omnis sed.','2018-11-26 02:44:55','2018-12-02 03:05:35',1,0),(12,36,4,'Vitae omnis sequi molestiae commodi quia. Iste corporis aliquid voluptatum voluptatem eius ratione.','2018-11-27 13:04:37','2018-12-26 03:54:16',1,0),(13,18,4,'Provident minima doloremque eos distinctio. Saepe non sit enim magni aperiam. Nostrum porro aliquam dolorem earum.','2018-11-21 02:21:39','2018-12-16 11:00:50',0,0),(14,23,2,'Eos ipsum quaerat magni autem quos omnis. Quisquam repudiandae illum consectetur repudiandae in qui aut.','2018-12-21 09:03:35','2019-01-03 09:50:58',1,0),(14,28,4,'Enim a ea possimus aliquid ducimus tempora qui excepturi. Quidem quasi incidunt dolor ex.','2018-11-28 09:18:25','2018-12-14 01:47:59',0,1),(14,41,2,'Quo consequatur assumenda earum soluta consequatur repellat beatae. Possimus ea aliquid voluptas sapiente.','2018-12-21 15:47:32','2018-12-31 17:53:07',1,0),(15,4,4,'At at eum autem commodi ut ipsum. Consequatur est ut magni rerum. Velit atque porro dolore et quibusdam.','2019-01-01 13:50:59','2019-01-02 08:13:29',1,0),(15,41,2,'Reiciendis perferendis veritatis eius quod. Quam eos qui est et. Quia veniam eos quisquam vitae nam quod nihil.','2018-12-18 22:49:04','2018-12-28 15:54:05',1,0),(16,5,4,'Voluptas quis consequatur repellat aut explicabo ea neque. Impedit itaque suscipit doloremque molestiae ratione odio.','2018-12-30 14:45:12','2018-12-31 10:17:41',1,0),(18,36,4,'A qui odio sit et culpa nihil. Fugit et eligendi omnis eum quia. Voluptate a distinctio est hic dolor.','2018-12-29 20:59:46','2018-12-31 09:05:42',0,0),(18,45,4,'Aut magni dolorem accusantium. Qui vel cupiditate et vitae harum qui.','2018-12-28 04:46:11','2018-12-31 18:22:28',1,0),(20,4,1,'Dolore totam ad veritatis. Iure natus repellendus eos vel eum soluta.','2018-11-25 16:48:42','2018-12-27 22:20:48',0,0),(20,26,3,'Molestiae vel est deleniti ullam. Et sit consequuntur sint ut. Nisi voluptate quisquam itaque accusantium ex.','2018-12-17 14:54:15','2019-01-01 13:36:43',0,0);
/*!40000 ALTER TABLE `course_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_tag`
--

DROP TABLE IF EXISTS `course_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_tag` (
  `course_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`,`tag_id`),
  KEY `IDX_760531B1591CC992` (`course_id`),
  KEY `IDX_760531B1BAD26311` (`tag_id`),
  CONSTRAINT `FK_760531B1591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_760531B1BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_tag`
--

LOCK TABLES `course_tag` WRITE;
/*!40000 ALTER TABLE `course_tag` DISABLE KEYS */;
INSERT INTO `course_tag` VALUES (1,7),(2,2),(3,5),(4,4),(5,2),(5,4),(6,5),(6,7),(7,2),(7,7),(8,4),(9,7),(10,2),(10,5),(11,7),(12,5),(13,1),(13,2),(14,2),(15,3),(16,5),(16,6),(17,1),(17,7),(18,1),(18,5),(19,1),(19,6),(20,6);
/*!40000 ALTER TABLE `course_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso_code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'en-US','English (United States)'),(2,'fr-BE','French (Belgium)'),(3,'es-ES','Spanish (Spain)');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190104084831');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_time_token`
--

DROP TABLE IF EXISTS `one_time_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_time_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `uuid` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iat` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5D9F8E3DA76ED395` (`user_id`),
  CONSTRAINT `FK_5D9F8E3DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_time_token`
--

LOCK TABLES `one_time_token` WRITE;
/*!40000 ALTER TABLE `one_time_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_time_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'Symfony'),(2,'JQuery'),(3,'Laravel'),(4,'Doctrine'),(5,'Promises'),(6,'Bootstrap'),(7,'MVC');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rate`
--

DROP TABLE IF EXISTS `tax_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rate`
--

LOCK TABLES `tax_rate` WRITE;
/*!40000 ALTER TABLE `tax_rate` DISABLE KEYS */;
INSERT INTO `tax_rate` VALUES (1,6),(2,12),(3,21);
/*!40000 ALTER TABLE `tax_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_successful_login` datetime DEFAULT NULL,
  `last_failed_login` datetime DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_email_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_8D93D6498E0E3CA6` (`user_role_id`),
  CONSTRAINT `FK_8D93D6498E0E3CA6` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,3,'Helmer','Treutel','HelmerTreutel','HelmerTreutel@gmail.com','http://placehold.it/50.png','$2y$13$9ASMl/5ushvFZa4F4umd5Otco48RusgNOSDevuc67CitGokJGgkUe','2018-09-04 16:11:43','2019-01-04 08:50:10','2019-01-04 08:50:10',NULL,NULL,1,0,1),(2,3,'Arianna','Kerluke','AriannaKerluke','AriannaKerluke@gmail.com','http://placehold.it/50.png','$2y$13$.9mqwzU6O51lKw4j76KJp.HbdxnJYY8KqFaIs8uuODlmm7K9naExy','2018-11-24 21:39:56','2019-01-04 08:50:11','2019-01-04 08:50:11',NULL,NULL,0,0,0),(3,2,'Reuben','Walter','ReubenWalter','ReubenWalter@gmail.com','http://placehold.it/50.png','$2y$13$Q6uISjxDGwvfskDR7qlu..vFPfPYu6Ck0A5dgBqpVR2R4j66v4qsy','2018-07-26 14:45:34','2019-01-04 08:50:11','2019-01-04 08:50:11',NULL,NULL,0,0,0),(4,2,'Scot','Olson','ScotOlson','ScotOlson@gmail.com','http://placehold.it/50.png','$2y$13$H8kypK33prcDrIFutqbS2uaWxaun03wR5KIJGzfJj8hMb/527Rld.','2018-08-10 09:06:14','2019-01-04 08:50:12','2019-01-04 08:50:12',NULL,NULL,0,0,1),(5,3,'Florine','Zieme','FlorineZieme','FlorineZieme@gmail.com','http://placehold.it/50.png','$2y$13$2MTzp2h3eluKVoZk6sWTreblne3ISUXKqKz4s2saQKQAMfjLV8aAi','2018-10-07 20:15:12','2019-01-04 08:50:12','2019-01-04 08:50:12',NULL,NULL,1,0,1),(6,5,'Dee','Wuckert','DeeWuckert','DeeWuckert@gmail.com','http://placehold.it/50.png','$2y$13$cczzSFXaZNeA3HbCcEluz.ztf9.pkXpI2MnUd36QSPyc5KzDO2BBS','2018-08-02 18:54:37','2019-01-04 08:50:13','2019-01-04 08:50:13',NULL,NULL,0,0,0),(7,3,'Karlee','Denesik','KarleeDenesik','KarleeDenesik@gmail.com','http://placehold.it/50.png','$2y$13$LFPgyY.2yf7cc/HqdZdoN.k8eJAGxoAHwFG9ELrt82twfhnJ4aTwW','2018-11-01 01:01:35','2019-01-04 08:50:14','2019-01-04 08:50:14',NULL,NULL,0,0,0),(8,4,'Lorine','Fisher','LorineFisher','LorineFisher@gmail.com','http://placehold.it/50.png','$2y$13$kpx2FuYxwIAJJBRymPX6venLG8Aw2RIOe5NbDctbpxUU.6kqpPhUy','2018-12-05 12:12:33','2019-01-04 08:50:14','2019-01-04 08:50:14',NULL,NULL,0,0,0),(9,4,'Desiree','Watsica','DesireeWatsica','DesireeWatsica@gmail.com','http://placehold.it/50.png','$2y$13$2RkSZultU/mZSxlSEOvYUujbVzET2sdikghpDggVvC2Bky5ddHVqK','2018-10-11 17:51:05','2019-01-04 08:50:15','2019-01-04 08:50:15',NULL,NULL,0,1,1),(10,3,'Providenci','Nader','ProvidenciNader','ProvidenciNader@gmail.com','http://placehold.it/50.png','$2y$13$93Cze0o34Fdzuage0b228eNUEO.yAJPKEMzddc/eBF0a7qsOnlZ3a','2018-11-09 09:36:18','2019-01-04 08:50:15','2019-01-04 08:50:15',NULL,NULL,0,0,0),(11,3,'Cecil','McKenzie','CecilMcKenzie','CecilMcKenzie@gmail.com','http://placehold.it/50.png','$2y$13$jKoUiLVM4SmvkltQsbDze.pxLAG59AHwJJyAPOrSC6MqSecs3Lcs2','2018-11-29 12:29:36','2019-01-04 08:50:16','2019-01-04 08:50:16',NULL,NULL,0,1,0),(12,4,'Joe','Macejkovic','JoeMacejkovic','JoeMacejkovic@gmail.com','http://placehold.it/50.png','$2y$13$xDdiRQcTvi/GXvcjdE9NkOxGQLvZZSxobJxmKYa6B2ZoDEDyki5s.','2018-07-20 20:45:17','2019-01-04 08:50:16','2019-01-04 08:50:16',NULL,NULL,0,0,1),(13,5,'Reymundo','Schuppe','ReymundoSchuppe','ReymundoSchuppe@gmail.com','http://placehold.it/50.png','$2y$13$yU55k/fRNdc3riA2D5nLJejn9Iq16rMrjRc/FcgrMqVvrho97DlF2','2018-10-01 19:15:51','2019-01-04 08:50:17','2019-01-04 08:50:17',NULL,NULL,1,0,1),(14,4,'Osbaldo','Schoen','OsbaldoSchoen','OsbaldoSchoen@gmail.com','http://placehold.it/50.png','$2y$13$F1XndHpaNZ6wXjZMKNKpveWHTaDHpy6cn771rh9NvlexeteT.jO3a','2018-09-14 19:01:50','2019-01-04 08:50:17','2019-01-04 08:50:17',NULL,NULL,0,1,1),(15,4,'Weston','Hirthe','WestonHirthe','WestonHirthe@gmail.com','http://placehold.it/50.png','$2y$13$gdHizg2m2Xoo5WoVrSBZsezXuAAJUtPBrwIplWQShJX.lwqssWe4i','2018-07-19 20:51:39','2019-01-04 08:50:18','2019-01-04 08:50:18',NULL,NULL,0,0,0),(16,3,'Karen','Thompson','KarenThompson','KarenThompson@gmail.com','http://placehold.it/50.png','$2y$13$xzW8xO.0ONGGj3/0k/4EreH62BBDNTljuSHwqAWC0BDtptdSJNafm','2018-12-08 12:44:36','2019-01-04 08:50:19','2019-01-04 08:50:19',NULL,NULL,0,0,1),(17,5,'Lula','McLaughlin','LulaMcLaughlin','LulaMcLaughlin@gmail.com','http://placehold.it/50.png','$2y$13$2FQhJVXcQrC9TircwgtK3.Aa2Xgc4kUURsOlREn52Q6KR.bSSkK2W','2018-10-05 02:18:43','2019-01-04 08:50:19','2019-01-04 08:50:19',NULL,NULL,1,0,1),(18,4,'Kaley','Roberts','KaleyRoberts','KaleyRoberts@gmail.com','http://placehold.it/50.png','$2y$13$IxWNTEtWo8c6BdYEj0.3kOsPYvE560V0Im/wCPgwAaOoJWKOwWlt.','2018-11-16 18:24:10','2019-01-04 08:50:20','2019-01-04 08:50:20',NULL,NULL,0,0,1),(19,5,'Haylie','Wiza','HaylieWiza','HaylieWiza@gmail.com','http://placehold.it/50.png','$2y$13$8DwmzZ5bIMKJXdscV4NSHexNt9i.DapdnoT2rJ21448bqZF1/uKE.','2018-08-07 07:37:12','2019-01-04 08:50:20','2019-01-04 08:50:20',NULL,NULL,0,0,1),(20,5,'Cassie','Miller','CassieMiller','CassieMiller@gmail.com','http://placehold.it/50.png','$2y$13$Rt1DlxT8PtOsUGpamQr9lOlLpf7U/XJwKcInDFW3YfzYow7xGjYPu','2018-10-21 09:38:08','2019-01-04 08:50:21','2019-01-04 08:50:21',NULL,NULL,0,0,0),(21,5,'Ignacio','Buckridge','IgnacioBuckridge','IgnacioBuckridge@gmail.com','http://placehold.it/50.png','$2y$13$wbjBB8TXIV5BV6feVHBVneN9z1yG4XfzVBzXZVDzVXjiZlAoLr8Re','2018-12-10 09:28:57','2019-01-04 08:50:21','2019-01-04 08:50:21',NULL,NULL,0,0,0),(22,5,'Christy','McLaughlin','ChristyMcLaughlin','ChristyMcLaughlin@gmail.com','http://placehold.it/50.png','$2y$13$pSFu0a/QEXIl8DF4K/Kih.ewjBA8nX86NArt6TzdFHi.Cum1NfGGe','2018-12-03 15:15:55','2019-01-04 08:50:22','2019-01-04 08:50:22',NULL,NULL,0,0,1),(23,4,'Celestine','Mosciski','CelestineMosciski','CelestineMosciski@gmail.com','http://placehold.it/50.png','$2y$13$5993/KDpKnyBOp5X6KC6c.MmMzG.UX3TeR7MOu05nDZC7aiqQso2.','2018-12-13 17:59:44','2019-01-04 08:50:22','2019-01-04 08:50:22',NULL,NULL,0,0,1),(24,5,'Domenick','Gislason','DomenickGislason','DomenickGislason@gmail.com','http://placehold.it/50.png','$2y$13$P.1RYVjk3Twyqoh/NCxA3uHicfRoyR9vBf0VnrmK4zApVOlBx8FVy','2018-12-17 15:27:48','2019-01-04 08:50:23','2019-01-04 08:50:23',NULL,NULL,0,0,1),(25,5,'Ellen','Weber','EllenWeber','EllenWeber@gmail.com','http://placehold.it/50.png','$2y$13$.8g.wKYK0T4zntJ7NPCxM.kbjifKv/kgjGbtsK0rPQsUskG/UvEe6','2018-07-22 12:15:25','2019-01-04 08:50:23','2019-01-04 08:50:23',NULL,NULL,0,0,0),(26,3,'Quinten','Farrell','QuintenFarrell','QuintenFarrell@gmail.com','http://placehold.it/50.png','$2y$13$y0M5Wb1q1yoyvcZTEcHr3uF/4a9BRN2sV5.8mf.oLLfF34pBqcwTa','2018-08-26 02:45:49','2019-01-04 08:50:24','2019-01-04 08:50:24',NULL,NULL,0,0,0),(27,5,'Janie','Shields','JanieShields','JanieShields@gmail.com','http://placehold.it/50.png','$2y$13$dp1PbCihtTu3/jdhgD0v2eYYOtuvror3bQtDcCeO3p3j8T3uBOyeC','2018-09-05 11:52:47','2019-01-04 08:50:25','2019-01-04 08:50:25',NULL,NULL,0,0,0),(28,3,'Katelyn','Lehner','KatelynLehner','KatelynLehner@gmail.com','http://placehold.it/50.png','$2y$13$q7ZS5WbZa0q6rUcpr90hsekLBhI5ujrh.zuEAhoXcrZgfd3DhDBK6','2018-07-05 21:47:06','2019-01-04 08:50:25','2019-01-04 08:50:25',NULL,NULL,0,0,0),(29,5,'Velma','Osinski','VelmaOsinski','VelmaOsinski@gmail.com','http://placehold.it/50.png','$2y$13$GdlhgJkm1o6qRADnnYP.IeacP9U9avhkk2Hce8dce/QUKFzRCLCBG','2018-11-05 13:05:01','2019-01-04 08:50:26','2019-01-04 08:50:26',NULL,NULL,1,0,0),(30,5,'Ari','McLaughlin','AriMcLaughlin','AriMcLaughlin@gmail.com','http://placehold.it/50.png','$2y$13$f9BqXhciQTU3ruCiK54IRO.p5O1ukupKLBOCYbpzAtRCBQbjaNaSS','2018-07-12 05:05:16','2019-01-04 08:50:26','2019-01-04 08:50:26',NULL,NULL,0,0,1),(31,4,'Anabel','Waters','AnabelWaters','AnabelWaters@gmail.com','http://placehold.it/50.png','$2y$13$tgS0b9N6C8huajYQ4L0P7OxZe41Nton5qOmhGUS3dFLjF.v.6pzgC','2018-12-06 18:07:55','2019-01-04 08:50:27','2019-01-04 08:50:27',NULL,NULL,0,0,1),(32,2,'Elwyn','Ryan','ElwynRyan','ElwynRyan@gmail.com','http://placehold.it/50.png','$2y$13$ZSqOdlqGyFMBvvQfEy3N/OPyQkQWqraOdJFfA9/ZmSyQgrMFJ.UAC','2018-12-11 07:54:32','2019-01-04 08:50:27','2019-01-04 08:50:27',NULL,NULL,0,0,0),(33,3,'Bertha','Gibson','BerthaGibson','BerthaGibson@gmail.com','http://placehold.it/50.png','$2y$13$1W4sGj./REBKXymDwY35d.Ek93QKUMzEQ1i2jofamMsmRpEFaeWSu','2018-12-21 10:48:01','2019-01-04 08:50:28','2019-01-04 08:50:28',NULL,NULL,1,0,1),(34,5,'William','Reilly','WilliamReilly','WilliamReilly@gmail.com','http://placehold.it/50.png','$2y$13$f5aVfhs83gsjwAgsv63BZ.oLBFkH4E0C4dHxUQZPVubUE6hY3JDSq','2018-11-10 06:25:50','2019-01-04 08:50:28','2019-01-04 08:50:28',NULL,NULL,0,0,1),(35,4,'Amani','Fritsch','AmaniFritsch','AmaniFritsch@gmail.com','http://placehold.it/50.png','$2y$13$MTvG0KObA7vJHEjm54IMj.2wVl84uW0NjNDYvqJtdx.GUULh8MIQG','2018-08-23 22:29:06','2019-01-04 08:50:29','2019-01-04 08:50:29',NULL,NULL,0,0,0),(36,2,'Austyn','Hauck','AustynHauck','AustynHauck@gmail.com','http://placehold.it/50.png','$2y$13$KZgo89Ve6egoq/t7mJZRgu1Z/JoNSj37JJSNVptD4dZSsKvXep1Ru','2018-12-27 23:39:17','2019-01-04 08:50:29','2019-01-04 08:50:29',NULL,NULL,0,0,0),(37,2,'Alysa','Rosenbaum','AlysaRosenbaum','AlysaRosenbaum@gmail.com','http://placehold.it/50.png','$2y$13$Gzzpj7DJSuq/pAZxU6zzS.fbJETqImJmM7fVPQdXDMRd5IEN7SGO6','2018-10-13 02:25:21','2019-01-04 08:50:30','2019-01-04 08:50:30',NULL,NULL,1,0,0),(38,4,'Oswald','Kemmer','OswaldKemmer','OswaldKemmer@gmail.com','http://placehold.it/50.png','$2y$13$mvCxpy2BynwSg3vjd0PrWOmAW66Jtd0I8zxibY6mPXXe8yMtbYDhC','2018-08-01 23:46:53','2019-01-04 08:50:31','2019-01-04 08:50:31',NULL,NULL,0,0,1),(39,5,'Damian','Rowe','DamianRowe','DamianRowe@gmail.com','http://placehold.it/50.png','$2y$13$RGdhwk3KOh1x12/DUdltmupa3GnxZzxdS6.yc40eGUQ5KgFVyaOoK','2018-09-07 01:06:19','2019-01-04 08:50:31','2019-01-04 08:50:31',NULL,NULL,0,0,0),(40,4,'Celestine','Schowalter','CelestineSchowalter','CelestineSchowalter@gmail.com','http://placehold.it/50.png','$2y$13$2U/bc4HBkHCF1iTNAfVAxOkW9me/.X4f1FlI0Eo5JiCQtiFz7IRNK','2018-07-21 11:59:29','2019-01-04 08:50:32','2019-01-04 08:50:32',NULL,NULL,0,0,1),(41,2,'Luisa','Schaefer','LuisaSchaefer','LuisaSchaefer@gmail.com','http://placehold.it/50.png','$2y$13$NRSIJ6jHOqX.fui65yKLbuG2QJn4.TTuEOLJ9oNmn3bJg98p9x7NG','2018-10-08 18:47:34','2019-01-04 08:50:32','2019-01-04 08:50:32',NULL,NULL,0,0,0),(42,2,'Vickie','Labadie','VickieLabadie','VickieLabadie@gmail.com','http://placehold.it/50.png','$2y$13$FpVhM6fZCqMtoAOw37BluO3sjp7HgHgQzQFHFre9AHLkNJ2Xja1sq','2018-09-02 23:30:39','2019-01-04 08:50:33','2019-01-04 08:50:33',NULL,NULL,0,0,1),(43,4,'Okey','Bartoletti','OkeyBartoletti','OkeyBartoletti@gmail.com','http://placehold.it/50.png','$2y$13$vaoaekM2pPdtKn4fqqgcAOW4phn.om4OCEqjj1EIyI9/cdIy3S9WO','2018-11-17 06:01:27','2019-01-04 08:50:33','2019-01-04 08:50:33',NULL,NULL,0,0,1),(44,5,'Cale','Kozey','CaleKozey','CaleKozey@gmail.com','http://placehold.it/50.png','$2y$13$bvzYEFjAz1JEnVNLOgsiXOaQkVV1JMMfE1xYVkpk0QVfVjndzQ9ue','2018-09-16 10:34:42','2019-01-04 08:50:34','2019-01-04 08:50:34',NULL,NULL,0,0,1),(45,4,'Federico','Kilback','FedericoKilback','FedericoKilback@gmail.com','http://placehold.it/50.png','$2y$13$irNZEI2L9eB7YZfzF7GdKujBFkaIiZuyU.oYumlwouVbsqk3Sn/m6','2018-07-17 22:44:16','2019-01-04 08:50:34','2019-01-04 08:50:34',NULL,NULL,0,0,0),(46,3,'Zelda','Rohan','ZeldaRohan','ZeldaRohan@gmail.com','http://placehold.it/50.png','$2y$13$.sjVt/1Lf/8Rwum1vv5nWuokQ/K33zlfRrYVD6E2yT8V1i1Wt3stS','2018-11-06 18:20:39','2019-01-04 08:50:35','2019-01-04 08:50:35',NULL,NULL,0,0,0),(47,2,'Lonie','Hoeger','LonieHoeger','LonieHoeger@gmail.com','http://placehold.it/50.png','$2y$13$58sKKFWs4wZmPelEzhBAke1WeF4Eb3w9Qa457JyRy0CzPfi7/oenC','2018-10-17 13:54:16','2019-01-04 08:50:36','2019-01-04 08:50:36',NULL,NULL,0,0,0),(48,2,'Ila','Deckow','IlaDeckow','IlaDeckow@gmail.com','http://placehold.it/50.png','$2y$13$RBqN7FRNdmc3F9bcrWwvBu/OEJVPCyhxPmpXl962Ja04Acult6fOm','2018-09-28 18:51:25','2019-01-04 08:50:36','2019-01-04 08:50:36',NULL,NULL,0,0,0),(49,4,'Joy','Rosenbaum','JoyRosenbaum','JoyRosenbaum@gmail.com','http://placehold.it/50.png','$2y$13$Oz7rn0Hs/1Zs7qBeQSDBL..4mIpZmHwpuGMdSVYgrFzvAWByHnn16','2018-09-23 23:31:13','2019-01-04 08:50:37','2019-01-04 08:50:37',NULL,NULL,0,0,0),(50,4,'Chaim','Heller','ChaimHeller','ChaimHeller@gmail.com','http://placehold.it/50.png','$2y$13$RhWceYQ1JpoyQMOIM5tvEuXASCHwmjYa2cek87/KDX92IMEr3NvDO','2018-08-25 22:07:28','2019-01-04 08:50:37','2019-01-04 08:50:37',NULL,NULL,0,0,1),(51,3,'Greta','Romaguera','GretaRomaguera','GretaRomaguera@gmail.com','http://placehold.it/50.png','$2y$13$huDkTh1xOyLdNqFcdS6o3.IdsRiJu9QBm39NsgIJiT9uc7OmA3FjG','2018-09-04 09:52:55','2019-01-04 08:50:38','2019-01-04 08:50:38',NULL,NULL,0,0,0),(52,4,'Freddy','Moore','FreddyMoore','FreddyMoore@gmail.com','http://placehold.it/50.png','$2y$13$3VB6IK0a6bVbc6yi.fR0gOP70.hruJB1NQEYXa.PVPhydXSJ.07cW','2018-07-24 20:33:23','2019-01-04 08:50:38','2019-01-04 08:50:38',NULL,NULL,1,0,1),(53,3,'Alverta','Grant','AlvertaGrant','AlvertaGrant@gmail.com','http://placehold.it/50.png','$2y$13$TJ.sgfCFtBqPf6oaJ1hx1uN5yQzlE1nuxkUpDX2ckL71wchQ5lyNq','2018-12-07 05:34:01','2019-01-04 08:50:39','2019-01-04 08:50:39',NULL,NULL,1,0,1),(54,4,'Mathilde','Davis','MathildeDavis','MathildeDavis@gmail.com','http://placehold.it/50.png','$2y$13$epSuhZy0bPx15HpAkUm9wuogX3ESF9muuOwhgsfJFeqfdpCHuLJK.','2018-11-02 12:27:41','2019-01-04 08:50:39','2019-01-04 08:50:39',NULL,NULL,0,1,1),(55,3,'Marian','Gutmann','MarianGutmann','MarianGutmann@gmail.com','http://placehold.it/50.png','$2y$13$cEwHikWWmV8Rm/tpcW34s.VbMktnAmGCZUwYcNu1JcfauIu55H7/u','2018-10-04 05:37:03','2019-01-04 08:50:40','2019-01-04 08:50:40',NULL,NULL,0,1,1),(56,5,'Asia','Kilback','AsiaKilback','AsiaKilback@gmail.com','http://placehold.it/50.png','$2y$13$DmFqiqxoHZ5l8.FHQBtQTuDR9nxIgV0dHIKj7Eqf9/QtB61d1S4tK','2018-08-24 13:39:49','2019-01-04 08:50:40','2019-01-04 08:50:40',NULL,NULL,0,0,0),(57,3,'Wilhelm','Weimann','WilhelmWeimann','WilhelmWeimann@gmail.com','http://placehold.it/50.png','$2y$13$nzl2OZJsWQu7Z0tee8Sgn.586yUorLkCqRc8/cVHiRca.dyTbRUy.','2018-12-10 12:27:52','2019-01-04 08:50:41','2019-01-04 08:50:41',NULL,NULL,0,0,0),(58,3,'Trevion','Reinger','TrevionReinger','TrevionReinger@gmail.com','http://placehold.it/50.png','$2y$13$mi93c4BTqARod93kI5VK8eic4cz8sDFv87TkCYaqRavF8avj9tmbe','2018-10-26 23:42:24','2019-01-04 08:50:42','2019-01-04 08:50:42',NULL,NULL,0,0,1),(59,5,'Orin','Roob','OrinRoob','OrinRoob@gmail.com','http://placehold.it/50.png','$2y$13$MYIZM.WrJBKmXqJZag6dy.X2/x5N6YMr4oE17RpU7HESUhFSp0m6G','2018-12-06 22:16:49','2019-01-04 08:50:42','2019-01-04 08:50:42',NULL,NULL,1,0,0),(60,4,'Deontae','Muller','DeontaeMuller','DeontaeMuller@gmail.com','http://placehold.it/50.png','$2y$13$kmHgQfBsK2H5oMg4bU4YcuEH/xZdqfUJ6w.bbOQkQeJZcnjoX5xMq','2019-01-04 01:09:23','2019-01-04 08:50:43','2019-01-04 08:50:43',NULL,NULL,1,0,0),(61,2,'John','Doe','jdoe','JohnDoe@gmail.com','userDefault.png','$2y$13$xheWsMdpgYC45oVx5Dz0LOFnIV1x8ZFfsNOqR0Du8HrMIvmMyJ.q6','2019-01-04 08:50:43','2019-01-04 08:50:43',NULL,NULL,NULL,0,0,1),(62,5,'Admin','Admin','admin','AdminAdmin@gmail.com','userDefault.png','$2y$13$WIymsKBcYHWvD94rJxUd0eTFONy381/P76wSeG7S/6WUCSP8hzPU.','2019-01-04 08:50:44','2019-01-04 08:50:44','2019-01-04 09:52:13',NULL,NULL,0,0,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_registration`
--

DROP TABLE IF EXISTS `user_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_registration`
--

LOCK TABLES `user_registration` WRITE;
/*!40000 ALTER TABLE `user_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'ROLE_GUEST',NULL),(2,'ROLE_MEMBER',NULL),(3,'ROLE_MODERATOR',NULL),(4,'ROLE_COURSE_MANAGER',NULL),(5,'ROLE_ADMIN',NULL);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-04 11:29:22

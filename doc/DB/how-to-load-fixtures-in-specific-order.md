###Priority loading
* implements ```DependentFixtureInterface``` on Fixture Classes that depend on other tables data
* in ```getDependencies()```
```
    // loaded in order of use, not based on array order
    return [
        TaxRateFixtures::class,
        CategoryFixtures::class,
        TagFixtures::class,
        CoursePromotionFixtures::class,
        LanguageFixtures::class 
    ];
```
* in ```services.yaml```, under key ```services``` => https://blog.joeymasip.com/tag/symfony/
```
    App\DataFixtures\:
        resource: '../src/DataFixtures'
        tags: ['doctrine.fixture.orm']
```
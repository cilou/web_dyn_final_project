### Reload DB with DataFixtures
1. ```php .\bin\console doctrine:database:drop --force```
1. ```php .\bin\console doctrine:database:create```
1. *optional if migration files and db already exist*  
    a. delete all migration files  
    b. ```php .\bin\console make:migration```
1. ```php .\bin\console doctrine:migrations:migrate```
1. ```php .\bin\console doctrine:fixtures:load```
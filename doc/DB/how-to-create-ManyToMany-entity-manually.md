###Example

1. Create **Course** Entity
1. Create **User** Entity
1. Create **CourseReview** Entity
```php
php .\bin\console make:entity CourseReview
```
- **course**, relation ManyToOne Course, not null
- **user**, relation ManyToOne User, not null
- **rating**, smallint, not null
- **review**, text, not null

* Primary Key(course, user) => modify migration file after ```make:migrate``` but before ```doctrine:migrations:migrate```  
* Added ```@ORM\Id()``` above ```$course``` and ```$user``` in Entity  
* Removed ```$id``` and ```getId()```